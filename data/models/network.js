/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const network = sequelize.define('network', {
		network_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'network_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		data: {
			type: DataTypes.TEXT,
			allowNull: false,
			field: 'data'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'network'
	});

	network.associate = (models) => {
		models.network.belongsTo(models.account, { foreignKey: 'account_id' });
	};

	return network;
};
