/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const room_device = sequelize.define('room_device', {
		room_device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'room_device_id'
		},
		device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'device',
				key: 'device_id'
			},
			field: 'device_id'
		},
		room_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'room',
				key: 'room_id'
			},
			field: 'room_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'room_device'
	});

	room_device.associate = (models) => {
		models.room_device.belongsTo(models.device, { foreignKey: 'device_id' });
		models.room_device.belongsTo(models.room, { foreignKey: 'room_id' });
	};

	return room_device;
};
