/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const room = sequelize.define('room', {
		room_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'room_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		image: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'image'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'room'
	});

	room.associate = (models) => {
		models.room.belongsTo(models.account, { foreignKey: 'account_id' });
		models.room.hasMany(models.room_device, { foreignKey: 'room_id' });
	};

	return room;
};
