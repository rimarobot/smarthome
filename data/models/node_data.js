/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const node_data = sequelize.define('node_data', {
		node_data_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'node_data_id'
		},
		node_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'node',
				key: 'node_id'
			},
			field: 'node_id'
		},
		data: {
			type: DataTypes.STRING(100),
			allowNull: true,
			field: 'data'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'node_data'
	});

	node_data.associate = (models) => {
		models.node_data.belongsTo(models.node, { foreignKey: 'node_id' });
	};

	return node_data;
};
