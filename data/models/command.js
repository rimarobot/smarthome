/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const command = sequelize.define('command', {
		command_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'command_id'
		},
		device_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'device_type',
				key: 'device_type_id'
			},
			field: 'device_type_id'
		},
		template: {
			type: DataTypes.STRING(60),
			allowNull: false,
			field: 'template'
		},
		decription: {
			type: DataTypes.STRING(100),
			allowNull: false,
			field: 'decription'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'command'
	});
	command.associate = (models) => {
		 models.command.belongsTo(models.device_type, { foreignKey: 'device_type_id' });
	};

	return command;
};
