/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
const user = sequelize.define('user', {
		user_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'user_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		surname: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'surname'
		},
		firstname: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'firstname'
		},
		email: {
			type: DataTypes.STRING(100),
			allowNull: false,
			field: 'email'
		},
		phone: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'phone'
		},
		username: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'username'
		},
		password: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'password'
		},
		pin: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'pin'
		},
		card: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'card'
		},
		finger_id: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'finger_id'
		},
		card_captured: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'card_captured'
		},
		pin_captured: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'pin_captured'
		},
		finger_captured: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'finger_captured'
		},
		card_added_date: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'card_added_date'
		},
		card_updated_date: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'card_updated_date'
		},
		finger_added_date: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'finger_added_date'
		},
		finger_updated_date: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'finger_updated_date'
		},
		pin_added_date: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'pin_added_date'
		},
		pin_updated_date: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'pin_updated_date'
		},
		card_enabled: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'card_enabled'
		},
		pin_enabled: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'pin_enabled'
		},
		finger_enabled: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'finger_enabled'
		},
		status: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			field: 'status'
		},
		profile_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'profile',
				key: 'profile_id'
			},
			field: 'profile_id'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'user'
	});

	user.associate = (models) => {
		models.user.belongsTo(models.account, { foreignKey: 'account_id' });
		models.user.belongsTo(models.profile, { foreignKey: 'profile_id' });
	};

	return user;
};
