/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const device_type = sequelize.define('device_type', {
		device_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'device_type_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		decription: {
			type: DataTypes.STRING(50),
			allowNull: true,
			field: 'decription'
		},
		image_url: {
			type: DataTypes.STRING(100),
			allowNull: true,
			field: 'image_url'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'device_type'
	});

	device_type.associate = (models) => {
		models.device_type.hasMany(models.command, { foreignKey: 'device_type_id' });
		models.device_type.hasMany(models.device, { foreignKey: 'device_type_id' }); 
	};

	return device_type;
};
