/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const device = sequelize.define('device', {
		device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'device_id'
		},
		node_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'node',
				key: 'node_id'
			},
			field: 'node_id'
		},
		device_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'device_type',
				key: 'device_type_id'
			},
			field: 'device_type_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		status: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'status',
			defaultValue: 'N/A'
		},
		image: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'image',
			defaultValue: 'N/A'
		},
		address: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'address',
			defaultValue: 'N/A'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'device'
	});

	device.associate = (models) => {
		
		models.device.belongsTo(models.device_type, { foreignKey: 'device_type_id' });
		models.device.belongsTo(models.node, { foreignKey: 'node_id' });
		models.device.hasMany(models.room_device, { foreignKey: 'room_device_id' });
		models.device.hasMany(models.control_device, { foreignKey: 'control_device_id' });
		models.device.hasMany(models.scene_device, { foreignKey: 'scene_device_id' });
	};

	return device;
};
