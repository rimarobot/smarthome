/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
 const control = sequelize.define('control', {
	control_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'control_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		startTime: {
			type: DataTypes.TIME,
			allowNull: true,
			field: 'startTime'
		},
		endTime: {
			type: DataTypes.TIME,
			allowNull: true,
			field: 'endTime'
		},
		frequency: {
			type: DataTypes.STRING(200),
			allowNull: true,
			field: 'frequency'
		},
		status: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'status'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'control'
	});

	control.associate = (models) => {
		models.control.belongsTo(models.account, { foreignKey: 'account_id' });
		models.control.hasMany(models.control_device, { foreignKey: 'control_id' });
	};

	return control;
};
