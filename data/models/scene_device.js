/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const scene_device = sequelize.define('scene_device', {
		scene_device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'scene_device_id'
		},
		scene_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'scene',
				key: 'scene_id'
			},
			field: 'scene_id'
		},
		device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'device',
				key: 'device_id'
			},
			field: 'device_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		data: {
			type: DataTypes.STRING(100),
			allowNull: true,
			field: 'data'
		},
		
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'scene_device'
	});

	scene_device.associate = (models) => {
		models.scene_device.belongsTo(models.scene, { foreignKey: 'scene_id' });
		models.scene_device.belongsTo(models.device, { foreignKey: 'device_id' });
	};

	return scene_device;
};
