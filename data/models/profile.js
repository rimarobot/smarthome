/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
const profile = sequelize.define('profile', {
		profile_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'profile_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		description: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'description'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'profile'
	});

	
	profile.associate = (models) => {
		models.profile.hasMany(models.user, { foreignKey: 'profile_id' });
	};

	return profile;
};
