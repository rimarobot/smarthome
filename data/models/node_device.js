/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const node_device = sequelize.define('node_device', {
		node_device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'node_device_id'
		},
		node_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'node_type',
				key: 'node_type_id'
			},
			field: 'node_type_id'
		},
		device_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'device_type',
				key: 'device_type_id'
			},
			field: 'device_type_id'
		},
		value: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'value'
		}
	}, {
		tableName: 'node_device'
	});

	node_device.associate = (models) => {
		//models.node_device.hasMany(models.node_type, { foreignKey: 'node_type_id' });
                //models.node_device.hasMany(models.device_type, { foreignKey: 'device_type_id' });
	};

	return node_device;
};
