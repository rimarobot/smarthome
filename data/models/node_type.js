/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const node_type = sequelize.define('node_type', {
		node_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'node_type_id'
		},
		value: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'value'
		},
		tittle: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'tittle'
		},
		description: {
			type: DataTypes.STRING(100),
			allowNull: false,
			field: 'description'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'node_type'
	});

	node_type.associate = (models) => {
		models.node_type.hasMany(models.node, { foreignKey: 'node_type_id' });
	};

	return node_type;
};
