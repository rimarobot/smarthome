/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const control_device = sequelize.define('control_device', {
		control_device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'control_device_id'
		},
		control_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'control',
				key: 'control_id'
			},
			field: 'control_id'
		},
		device_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'device',
				key: 'device_id'
			},
			field: 'device_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		data: {
			type: DataTypes.STRING(100),
			allowNull: true,
			field: 'data'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'control_device'
	});

	control_device.associate = (models) => {
		models.control_device.belongsTo(models.control, { foreignKey: 'control_id' });
		models.control_device.belongsTo(models.device, { foreignKey: 'device_id' });
	};

	return control_device;
};
