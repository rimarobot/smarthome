/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const scene = sequelize.define('scene', {
		scene_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'scene_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: true,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'name'
		},
		status: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			field: 'status',
			defaultValue: 0
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'scene'
	});

	scene.associate = (models) => {
		models.scene.belongsTo(models.account, { foreignKey: 'account_id' });
		models.scene.hasMany(models.scene_device, { foreignKey: 'scene_id' });
	};

	return scene;
};
