/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const account = sequelize.define('account', {
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'account_id'
		},
		surname: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'surname'
		},
		firstname: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'firstname'
		},
		email: {
			type: DataTypes.STRING(100),
			allowNull: false,
			unique:true,
			field: 'email'
		},
		phone: {
			type: DataTypes.STRING(15),
			allowNull: false,
			unique:true,
			field: 'phone'
		},
		country: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'country'
		},
		address: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'address'
		},
		town: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'town'
		},
		district: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'district'
		},
		username: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'username'
		},
		password: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'password'
		},
		gender: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'gender'
		},
		dob: {
			type: DataTypes.DATE,
			allowNull: false,
			field: 'dob'
		},
		status: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'status'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'account'
	});

	account.associate = (models) => {
		models.account.hasOne(models.account_settings, {  foreignKey: 'account_id' });
		models.account.hasMany(models.user, { foreignKey: 'account_id' });
		models.account.hasMany(models.node, { foreignKey: 'account_id' });
		models.account.hasMany(models.room, { foreignKey: 'account_id' });
		models.account.hasMany(models.scene, { foreignKey: 'account_id' });
		models.account.hasMany(models.control, { foreignKey: 'account_id' });
		models.account.hasOne(models.network, { foreignKey: 'account_id' });
	};

	return account;

};
