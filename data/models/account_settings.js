/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const account_settings = sequelize.define('account_settings', {
		setting_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'setting_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		account_code: {
			type: DataTypes.STRING(45),
			allowNull: false,
			unique:true,
			field: 'account_code'
		},
		account_hash: {
			type: DataTypes.STRING(200),
			allowNull: true,
			field: 'account_hash'
		},
		root_address: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'root_address'
		},
		mesh_ssid: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'mesh_ssid'
		},
		mesh_password: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'mesh_password'
		},
		mesh_port: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'mesh_port'
		},
		mesh_channel: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'mesh_channel'
		},
		mesh_hidden: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'mesh_hidden'
		},
		mesh_connections: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'mesh_connections'
		},
		gateway_ssid: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'gateway_ssid'
		},
		gateway_password: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'gateway_password'
		},
		host_address: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'host_address'
		},
		host_port: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			field: 'host_port'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'account_settings'
	});

	account_settings.associate = (models) => {
		models.account_settings.belongsTo(models.account, { foreignKey: 'account_id' });
	};

	return account_settings;
};
