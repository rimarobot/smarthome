/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const node = sequelize.define('node', {
		node_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'node_id'
		},
		account_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'account',
				key: 'account_id'
			},
			field: 'account_id'
		},
		node_type_id: {
			type: DataTypes.INTEGER(18),
			allowNull: false,
			references: {
				model: 'node_type',
				key: 'node_type_id'
			},
			field: 'node_type_id'
		},
		node_address: {
			type: DataTypes.STRING(45),
			allowNull: false,
			field: 'node_address'
		},
		status: {
			type: DataTypes.STRING(45),
			allowNull: false,
			defaultValue: '0',
			field: 'status'
		},
		last_active: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'last_active'
		},
		voltage: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'voltage'
		},
		memory: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'memory'
		},
		rom: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'rom'
		},
		software_version: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'software_version'
		},
		hardware_version: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'hardware_version'
		},
		processor: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'processor'
		},
		temperature: {
			type: DataTypes.STRING(45),
			allowNull: true,
			field: 'temperature'
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'createdAt'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('current_timestamp'),
			field: 'updatedAt'
		}
	}, {
		tableName: 'node'
	});

	node.associate = (models) => {
		models.node.belongsTo(models.account, { foreignKey: 'account_id' });
		models.node.belongsTo(models.node_type, { foreignKey: 'node_type_id' });
		models.node.hasMany(models.device, { foreignKey: 'node_id' });
	};

	return node;
};
