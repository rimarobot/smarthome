-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2020 at 10:54 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smarthome`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_network` (IN `p_room` VARCHAR(20), IN `p_network` TEXT)  BEGIN
declare v_account_id int(18) default 0;
declare v_network_id int(18) default 0;

select `account_id` into v_account_id from `account_settings` where `account_code`= p_room ;

if (v_account_id > 0) then
	    select  `network_id` into v_network_id from `network` where `account_id` = v_account_id;
         if (v_network_id > 0) then
           update `network` SET `data`= p_network  where `network_id`= v_network_id;
         else
           insert into `network`(`account_id`, `data`, `createdAt`) values (v_account_id,p_network,current_timestamp);
         end if;  
  end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_status` (IN `p_room` VARCHAR(20), IN `p_nodeid` VARCHAR(20), IN `p_voltage` VARCHAR(20), IN `p_ram` VARCHAR(20), IN `p_rom` VARCHAR(20), IN `p_software_version` VARCHAR(20), IN `p_hardware_version` VARCHAR(20), IN `p_processor` VARCHAR(20), IN `p_temperature` VARCHAR(20), IN `p_sensor_data` VARCHAR(20), IN `p_node_type` VARCHAR(20))  BEGIN
declare v_node_id int(18) default 0;  
 
select `node_id` into v_node_id  from `node` where `node_address`=p_nodeid and `account_id` = (select `account_id` from `account_settings` where `account_code`= p_room );
if (v_node_id > 0) then
	 
		update `node`
		set `status` = 1, `last_active` = current_timestamp, `voltage` = p_voltage,
			`memory` =  p_ram, `rom` =  p_rom, `software_version` =  p_software_version,
			`hardware_version` =  p_hardware_version, `processor` =  p_processor, 
            `temperature` = p_temperature, `updatedAt` =  current_timestamp
		where `node_id`= v_node_id;
		insert into `node_data`(`node_id`, `data`, `createdAt`) values  (v_node_id, p_sensor_data, current_timestamp);
        
	 
else 
	 
		insert into `node`(`account_id`, `node_type_id`, `node_address`, `status`, `last_active`, `voltage`, `memory`, `rom`, `software_version`, `hardware_version`, `processor`, `temperature`, `createdAt`) 
		values ((select `account_id` from `account_settings` where `account_code`= p_room ), p_node_type,
		p_nodeid, 1, current_timestamp, p_voltage, p_ram, p_rom, p_software_version, p_hardware_version,p_processor,p_temperature,current_timestamp);
		
		select `node_id` into v_node_id  from `node` where `node_address`=p_nodeid and `account_id` = (select `account_id` from `account_settings` where `account_code`= p_room );
		if (v_node_id > 0) then
            insert into `node_data`(`node_id`, `data`, `createdAt`) values  (v_node_id, p_sensor_data, current_timestamp);
		    insert into `device`(`node_id`,`device_type_id`,`name`,`status`,`address`,`createdAt`)
			select `node`.`node_id`, `node_device`.`device_type_id`,
			CONCAT( `device_type`.`name`, ' # ',`node_device`.`value`) 'name',
			1, `node_device`.`value`, current_timestamp
			 from `node_device` inner join `node_type` on `node_device`.`node_type_id` = `node_type`.`node_type_id`
			 inner join `device_type` on `node_device`.`device_type_id` = `device_type`.`device_type_id` 
			 inner join `node` on `node`.`node_type_id` = `node_device`.`node_type_id`
			 where `node`.`node_address` = p_nodeid;          
           
		end if;

  end if;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(18) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `country` varchar(45) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `gender` varchar(45) NOT NULL,
  `dob` datetime NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `surname`, `firstname`, `email`, `phone`, `country`, `address`, `town`, `district`, `username`, `password`, `gender`, `dob`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'kalibatta', 'richard', 'kalibattarichard@gmail.com', '07513435334', 'Uganda', 'zzana', 'kampala', 'kampala', 'user1', 'pass', 'MALE', '2020-06-15 21:00:00', '1', '2020-06-16 19:54:28', '2020-06-16 19:54:28');

-- --------------------------------------------------------

--
-- Table structure for table `account_settings`
--

CREATE TABLE `account_settings` (
  `setting_id` int(18) NOT NULL,
  `account_id` int(18) NOT NULL,
  `account_code` varchar(45) NOT NULL,
  `account_hash` varchar(200) DEFAULT NULL,
  `root_address` varchar(45) DEFAULT NULL,
  `mesh_ssid` varchar(45) NOT NULL,
  `mesh_password` varchar(45) NOT NULL,
  `mesh_port` int(11) NOT NULL,
  `mesh_channel` int(11) NOT NULL,
  `mesh_hidden` int(11) NOT NULL,
  `mesh_connections` int(11) NOT NULL,
  `gateway_ssid` varchar(45) NOT NULL,
  `gateway_password` varchar(45) NOT NULL,
  `host_address` varchar(45) NOT NULL,
  `host_port` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account_settings`
--

INSERT INTO `account_settings` (`setting_id`, `account_id`, `account_code`, `account_hash`, `root_address`, `mesh_ssid`, `mesh_password`, `mesh_port`, `mesh_channel`, `mesh_hidden`, `mesh_connections`, `gateway_ssid`, `gateway_password`, `host_address`, `host_port`, `createdAt`, `updatedAt`) VALUES
(1, 1, '0000001', 'dbKoI9G2JUDZ6ZBKAAAC', '572052203', 'SHMab67', '2e80ab67', 5555, 1, 1, 3, 'KRB-AIR1', 'benedict24', '192.168.1.100', 3000, '2020-06-16 19:54:46', '2020-06-16 19:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `command`
--

CREATE TABLE `command` (
  `command_id` int(18) NOT NULL,
  `device_type_id` int(18) NOT NULL,
  `template` varchar(60) NOT NULL,
  `decription` varchar(100) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `control`
--

CREATE TABLE `control` (
  `control_id` int(18) NOT NULL,
  `account_id` int(18) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `frequency` varchar(200) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `control`
--

INSERT INTO `control` (`control_id`, `account_id`, `name`, `startTime`, `endTime`, `frequency`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Morning glory', '00:00:00', '00:00:00', '2131296413,2131296419,2131296418,2131296412,2131296417|Mon,Tue,Thu,Fri,Sun', NULL, '2020-06-18 08:54:05', '2020-06-18 08:54:05'),
(2, 1, 'Back Home', '00:00:00', '00:00:00', '2131296419,2131296418|Mon,Fri', NULL, '2020-06-18 20:05:04', '2020-06-20 11:14:16'),
(3, 1, 'Garden Pump', '00:00:00', '00:00:00', '2131296419,2131296425,2131296426,2131296424,2131296418,2131296422|Mon,Tue,Wed,Thu,Fri,Sat', NULL, '2020-06-19 04:24:31', '2020-06-23 13:31:39'),
(4, 1, 'Security Lights', '00:00:00', '00:00:00', '2131296413,2131296419,2131296420,2131296418,2131296412,2131296416,2131296417|Mon,Tue,Wed,Thu,Fri,Sat,Sun', NULL, '2020-06-19 04:28:40', '2020-06-19 04:28:40'),
(5, 1, 'test1', '00:00:00', '00:00:00', '2131296419,2131296425,2131296426,2131296424,2131296418|Mon,Tue,Wed,Thu,Fri', NULL, '2020-06-20 11:28:20', '2020-06-20 11:28:21');

-- --------------------------------------------------------

--
-- Table structure for table `control_device`
--

CREATE TABLE `control_device` (
  `control_device_id` int(18) NOT NULL,
  `control_id` int(18) DEFAULT NULL,
  `device_id` int(18) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `control_device`
--

INSERT INTO `control_device` (`control_device_id`, `control_id`, `device_id`, `name`, `data`, `createdAt`, `updatedAt`) VALUES
(9, 1, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(10, 2, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(11, 3, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(12, 4, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(13, 5, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(14, 1, 2, 'Power Switch # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(15, 2, 2, 'Power Switch # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(16, 3, 2, 'Power Switch # 0', '0', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(17, 4, 2, 'Power Switch # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(18, 5, 2, 'Power Switch # 0', '0', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(19, 1, 3, 'Power Switch # 1', '1', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(20, 2, 3, 'Power Switch # 1', '1', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(21, 3, 3, 'Power Switch # 1', '1', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(22, 4, 3, 'Power Switch # 1', '1', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(23, 5, 3, 'Power Switch # 1', '1', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(24, 1, 4, 'Power Switch # 2', '2', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(25, 2, 4, 'Power Switch # 2', '2', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(26, 3, 4, 'Power Switch # 2', '2', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(27, 4, 4, 'Power Switch # 2', '2', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(28, 5, 4, 'Power Switch # 2', '2', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(29, 1, 5, 'Power Switch # 3', '3', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(30, 2, 5, 'Power Switch # 3', '3', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(31, 3, 5, 'Power Switch # 3', '3', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(32, 4, 5, 'Power Switch # 3', '3', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(33, 5, 5, 'Power Switch # 3', '3', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(34, 1, 6, 'Power Switch # 4', '4', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(35, 2, 6, 'Power Switch # 4', '4', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(36, 3, 6, 'Power Switch # 4', '4', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(37, 4, 6, 'Power Switch # 4', '4', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(38, 5, 6, 'Power Switch # 4', '4', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(39, 1, 7, 'Power Switch # 5', '5', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(40, 2, 7, 'Power Switch # 5', '5', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(41, 3, 7, 'Power Switch # 5', '5', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(42, 4, 7, 'Power Switch # 5', '5', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(43, 5, 7, 'Power Switch # 5', '5', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(44, 1, 8, 'Power Switch # 6', '6', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(45, 2, 8, 'Power Switch # 6', '6', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(46, 3, 8, 'Power Switch # 6', '6', '2020-06-21 21:51:14', '2020-06-23 13:31:38'),
(47, 4, 8, 'Power Switch # 6', '6', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(48, 5, 8, 'Power Switch # 6', '6', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(49, 1, 9, 'Power Switch # 7', '7', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(50, 2, 9, 'Power Switch # 7', '7', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(51, 3, 9, 'Power Switch # 7', '7', '2020-06-21 21:51:14', '2020-06-23 13:31:39'),
(52, 4, 9, 'Power Switch # 7', '7', '2020-06-21 21:51:14', '2020-06-21 21:51:14'),
(53, 5, 9, 'Power Switch # 7', '7', '2020-06-21 21:51:14', '2020-06-21 21:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `device_id` int(18) NOT NULL,
  `node_id` int(18) NOT NULL,
  `device_type_id` int(18) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'N/A',
  `image` varchar(45) NOT NULL DEFAULT 'N/A',
  `address` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`device_id`, `node_id`, `device_type_id`, `name`, `status`, `image`, `address`, `createdAt`, `updatedAt`) VALUES
(1, 1, 3, 'Weather Sensor # 0', '1', 'N/A', '0', '2020-06-21 21:29:22', '2020-06-21 21:29:22'),
(2, 2, 1, 'Power Switch # 0', '1', 'N/A', '0', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(3, 2, 1, 'Power Switch # 1', '1', 'N/A', '1', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(4, 2, 1, 'Power Switch # 2', '1', 'N/A', '2', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(5, 2, 1, 'Power Switch # 3', '1', 'N/A', '3', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(6, 2, 1, 'Power Switch # 4', '1', 'N/A', '4', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(7, 2, 1, 'Power Switch # 5', '1', 'N/A', '5', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(8, 2, 1, 'Power Switch # 6', '1', 'N/A', '6', '2020-06-21 21:30:10', '2020-06-21 21:30:10'),
(9, 2, 1, 'Power Switch # 7', '1', 'N/A', '7', '2020-06-21 21:30:10', '2020-06-21 21:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `device_type`
--

CREATE TABLE `device_type` (
  `device_type_id` int(18) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `decription` varchar(50) DEFAULT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `device_type`
--

INSERT INTO `device_type` (`device_type_id`, `name`, `decription`, `image_url`, `createdAt`, `updatedAt`) VALUES
(1, 'Power Switch', 'Power switch ', NULL, '2020-06-11 20:30:02', '2020-06-11 20:31:05'),
(2, 'Motion Sensor', 'Motion Sensor', NULL, '2020-06-11 20:31:23', '2020-06-11 20:31:47'),
(3, 'Weather Sensor', 'Weather Sensor', NULL, '2020-06-11 20:32:50', '2020-06-11 20:33:05'),
(4, 'Light Sensor', 'Light Sensor', NULL, '2020-06-11 20:33:41', '2020-06-11 20:33:58'),
(5, 'Servo Motor', 'Servo Motor', NULL, '2020-06-11 20:34:11', '2020-06-11 20:34:27'),
(6, 'Gas Sensor', 'Gas Sensor', NULL, '2020-06-11 20:35:08', '2020-06-11 20:35:26'),
(7, 'Access Control', 'Access Control', NULL, '2020-06-11 20:35:44', '2020-06-11 20:36:05'),
(8, 'Fan Control', 'Fan Control', NULL, '2020-06-11 20:36:38', '2020-06-11 20:36:57'),
(9, 'Motion Switch', 'Motion Switch', NULL, '2020-06-11 20:37:12', '2020-06-11 20:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `network`
--

CREATE TABLE `network` (
  `network_id` int(18) NOT NULL,
  `account_id` int(18) NOT NULL,
  `data` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `network`
--

INSERT INTO `network` (`network_id`, `account_id`, `data`, `createdAt`, `updatedAt`) VALUES
(1, 1, '{\"nodeId\":572052203,\"root\":true,\"subs\":[{\"nodeId\":572105673}]}', '2020-06-21 12:56:47', '2020-06-21 12:56:47');

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

CREATE TABLE `node` (
  `node_id` int(18) NOT NULL,
  `account_id` int(18) NOT NULL,
  `node_type_id` int(18) NOT NULL,
  `node_address` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT '0',
  `last_active` datetime DEFAULT NULL,
  `voltage` varchar(45) DEFAULT NULL,
  `memory` varchar(45) DEFAULT NULL,
  `rom` varchar(45) DEFAULT NULL,
  `software_version` varchar(45) DEFAULT NULL,
  `hardware_version` varchar(45) DEFAULT NULL,
  `processor` varchar(45) DEFAULT NULL,
  `temperature` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `node`
--

INSERT INTO `node` (`node_id`, `account_id`, `node_type_id`, `node_address`, `status`, `last_active`, `voltage`, `memory`, `rom`, `software_version`, `hardware_version`, `processor`, `temperature`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, '572052203', '1', '2020-06-21 21:29:22', '65535', '22800', '388576', 'V0.4', 'V0.2', 'ESP8266', '25.71', '2020-06-21 21:29:22', '2020-06-21 21:29:22'),
(2, 1, 9, '572105673', '1', '2020-06-21 21:30:10', '65535', '28600', '361008', 'V0.4', 'V0.2', 'ESP8266', '25.71', '2020-06-21 21:30:10', '2020-06-21 21:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `node_data`
--

CREATE TABLE `node_data` (
  `node_data_id` int(18) NOT NULL,
  `node_id` int(18) NOT NULL,
  `data` varchar(100) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `node_data`
--

INSERT INTO `node_data` (`node_data_id`, `node_id`, `data`, `createdAt`, `updatedAt`) VALUES
(1, 1, '', '2020-06-21 21:29:22', '2020-06-21 21:29:22'),
(2, 2, '11111110', '2020-06-21 21:30:10', '2020-06-21 21:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `node_device`
--

CREATE TABLE `node_device` (
  `node_device_id` int(18) NOT NULL,
  `node_type_id` int(18) NOT NULL,
  `device_type_id` int(18) NOT NULL,
  `value` varchar(45) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `node_device`
--

INSERT INTO `node_device` (`node_device_id`, `node_type_id`, `device_type_id`, `value`, `createdAt`, `updatedAt`) VALUES
(1, 2, 1, '0', '2020-06-20 21:36:42', '2020-06-20 21:36:42'),
(2, 3, 8, '0', '2020-06-20 21:40:45', '2020-06-20 21:40:45'),
(3, 4, 4, '0', '2020-06-20 21:40:45', '2020-06-20 21:40:45'),
(4, 5, 3, '0', '2020-06-20 21:40:45', '2020-06-20 21:40:45'),
(6, 6, 1, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, 1, '0', '2020-06-21 18:08:55', '2020-06-21 18:08:55'),
(8, 7, 1, '1', '2020-06-21 18:08:55', '2020-06-21 18:08:55'),
(9, 8, 1, '0', '2020-06-21 18:08:55', '2020-06-21 18:08:55'),
(10, 8, 1, '1', '2020-06-21 18:08:55', '2020-06-21 18:08:55'),
(11, 8, 1, '2', '2020-06-21 18:08:55', '2020-06-21 18:08:55'),
(12, 8, 1, '3', '2020-06-21 18:08:55', '2020-06-21 18:08:55'),
(13, 9, 1, '0', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(14, 9, 1, '1', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(15, 9, 1, '2', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(16, 9, 1, '3', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(17, 9, 1, '4', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(18, 9, 1, '5', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(19, 9, 1, '6', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(20, 9, 1, '7', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(21, 10, 5, '0', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(22, 11, 1, '0', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(23, 12, 2, '0', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(24, 13, 9, '0', '2020-06-21 18:11:15', '2020-06-21 18:11:15'),
(25, 1, 3, '0', '2020-06-21 18:11:15', '2020-06-21 18:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `node_logs`
--

CREATE TABLE `node_logs` (
  `node_log_id` int(18) NOT NULL,
  `node_id` int(18) NOT NULL,
  `data` text DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `node_logs`
--

INSERT INTO `node_logs` (`node_log_id`, `node_id`, `data`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Manage your home by powering and connecting devices to Wi-Fi Manage your home by powering and connecting devices to Wi-Fi Manage your home by powering and connecting devices to Wi-Fi', '2020-07-01 22:00:21', '2020-07-05 22:01:52'),
(2, 1, 'Motion detected on front Door ', '2020-07-01 22:00:21', '2020-07-05 22:04:06'),
(3, 2, 'Manage your home by powering and connecting devices to Wi-Fi Manage your home by powering and connecting devices to Wi-Fi Manage your home by powering and connecting devices to Wi-Fi', '2020-07-01 22:00:21', '2020-07-05 22:04:46'),
(4, 2, 'Richard has access back Door', '2020-07-05 22:00:21', '2020-07-05 22:05:13');

-- --------------------------------------------------------

--
-- Table structure for table `node_type`
--

CREATE TABLE `node_type` (
  `node_type_id` int(18) NOT NULL,
  `value` varchar(45) NOT NULL,
  `tittle` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `node_type`
--

INSERT INTO `node_type` (`node_type_id`, `value`, `tittle`, `description`, `createdAt`, `updatedAt`) VALUES
(1, 'CENTRAL_HUB_CONTROLLER', 'WiFi Hub', 'Connects to the WiFi and monitors all other nodes in the network', '2020-06-11 20:13:04', '2020-06-11 14:33:04'),
(2, 'LIGHT_RELAY_CONTROLLER', 'Light Switch', 'Control a single light bulb ON and OFF', '2020-06-11 15:05:10', '2020-06-11 15:06:17'),
(3, 'FAN_RELAY_CONTROLLER', 'Thermostat', 'Control Fan ON and OFF, Automate Fan using Temperature sensor', '2020-06-11 15:06:38', '2020-06-11 15:07:52'),
(4, 'LIGHT_SENSOR_CONTROLLER', 'Light Sensor', 'Light sensor to automate Day and night activities', '2020-06-11 16:01:35', '2020-06-11 16:02:43'),
(5, 'TEMP_HUMIDITY_CONTROLLER', 'Weather Sensor', 'Temperature & Humidity Sensor', '2020-06-11 15:09:26', '2020-06-11 15:28:48'),
(6, 'ONE_RELAY_CONTROLLER', 'One Relay Switch', 'One relay switch', '2020-06-11 15:09:26', '2020-06-11 15:29:59'),
(7, 'TWO_RELAY_CONTROLLER', 'Two Relay Switch', 'Two relay switch set', '2020-06-11 15:30:40', '2020-06-11 15:31:37'),
(8, 'FOUR_RELAY_CONTROLLER', 'Four Relay Switch', 'Four relay switch set', '2020-06-11 15:31:44', '2020-06-11 15:32:20'),
(9, 'EIGHT_RELAY_CONTROLLER', 'Eight Relay Switch', 'Eight relay switch set', '2020-06-11 15:50:31', '2020-06-11 15:50:56'),
(10, 'WINDOW_SERVO_CONTROLLER', 'Window Shutter', 'Servo motor to Close and Open window blinds', '2020-06-11 15:51:03', '2020-06-11 15:52:03'),
(11, 'ACCESS_CONTROL_CONTROLLER', 'Door Access Control', 'Door access control unit with:- Fingerprint & Card', '2020-06-11 15:52:08', '2020-06-11 15:53:15'),
(12, 'MOTION_SENSOR_CONTROLLER', 'Motion Sensor', 'Motion sensor to detect intruders around the home', '2020-06-11 20:09:34', '2020-06-11 20:10:38'),
(13, 'MOTION_RELAY_CONTROLLER', 'Motion Switch', 'Motion controlled relay', '2020-06-11 20:11:20', '2020-06-11 20:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `profile_id` int(18) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`profile_id`, `name`, `description`, `createdAt`, `updatedAt`) VALUES
(1, 'Administrator', 'Administrator', '2020-07-03 21:22:57', '2020-07-03 21:23:28'),
(2, 'User', 'User', '2020-07-03 21:22:57', '2020-07-03 21:23:59'),
(3, 'Guest', 'Guest', '2020-07-03 21:22:57', '2020-07-03 21:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `room_id` int(18) NOT NULL,
  `account_id` int(18) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`room_id`, `account_id`, `name`, `image`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Office', '2131296659|Office', '2020-06-17 13:30:51', '2020-06-17 13:30:51'),
(2, 1, 'Kitchen', '2131296657|Kitchen', '2020-06-18 19:37:40', '2020-06-18 19:37:40'),
(3, 1, 'TV Room', '2131296661|TV Room', '2020-06-19 04:21:47', '2020-06-19 04:21:47'),
(4, 1, 'Kid\'s BathRoom', '2131296653|BathRoom', '2020-06-19 04:27:03', '2020-06-19 04:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `room_device`
--

CREATE TABLE `room_device` (
  `room_device_id` int(18) NOT NULL,
  `device_id` int(18) DEFAULT NULL,
  `room_id` int(18) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_device`
--

INSERT INTO `room_device` (`room_device_id`, `device_id`, `room_id`, `name`, `createdAt`, `updatedAt`) VALUES
(16, 1, 1, 'Weather Sensor # 0', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(17, 1, 2, 'Weather Sensor # 0', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(18, 1, 3, 'Weather Sensor # 0', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(19, 1, 4, 'Weather Sensor # 0', '2020-06-21 21:57:01', '2020-06-23 12:59:04'),
(20, 2, 1, 'Power Switch # 0', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(21, 2, 2, 'Power Switch # 0', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(22, 2, 3, 'Power Switch # 0', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(23, 2, 4, 'Power Switch # 0', '2020-06-21 21:57:01', '2020-06-23 12:59:04'),
(24, 3, 1, 'Power Switch # 1', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(25, 3, 2, 'Power Switch # 1', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(26, 3, 3, 'Power Switch # 1', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(27, 3, 4, 'Power Switch # 1', '2020-06-21 21:57:01', '2020-06-23 12:59:04'),
(28, 4, 1, 'Power Switch # 2', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(29, 4, 2, 'Power Switch # 2', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(30, 4, 3, 'Power Switch # 2', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(31, 4, 4, 'Power Switch # 2', '2020-06-21 21:57:01', '2020-06-23 12:59:05'),
(32, 5, 1, 'Power Switch # 3', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(33, 5, 2, 'Power Switch # 3', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(34, 5, 3, 'Power Switch # 3', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(35, 5, 4, 'Power Switch # 3', '2020-06-21 21:57:01', '2020-06-23 12:59:05'),
(36, 6, 1, 'Power Switch # 4', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(37, 6, 2, 'Power Switch # 4', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(38, 6, 3, 'Power Switch # 4', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(39, 6, 4, 'Power Switch # 4', '2020-06-21 21:57:01', '2020-06-23 12:59:04'),
(40, 7, 1, 'Power Switch # 5', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(41, 7, 2, 'Power Switch # 5', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(42, 7, 3, 'Power Switch # 5', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(43, 7, 4, 'Power Switch # 5', '2020-06-21 21:57:01', '2020-06-23 12:59:05'),
(44, 8, 1, 'Power Switch # 6', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(45, 8, 2, 'Power Switch # 6', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(46, 8, 3, 'Power Switch # 6', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(47, 8, 4, 'Power Switch # 6', '2020-06-21 21:57:01', '2020-06-23 12:59:05'),
(48, 9, 1, 'Power Switch # 7', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(49, 9, 2, 'Power Switch # 7', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(50, 9, 3, 'Power Switch # 7', '2020-06-21 21:57:01', '2020-06-21 21:57:01'),
(51, 9, 4, 'Power Switch # 7', '2020-06-21 21:57:01', '2020-06-23 12:59:04');

-- --------------------------------------------------------

--
-- Table structure for table `scene`
--

CREATE TABLE `scene` (
  `scene_id` int(18) NOT NULL,
  `account_id` int(18) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `status` int(18) NOT NULL DEFAULT 0,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `scene`
--

INSERT INTO `scene` (`scene_id`, `account_id`, `name`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'test', 0, '2020-06-18 20:38:55', '2020-06-18 20:38:55'),
(2, 1, 'Security Lights', 0, '2020-06-19 04:16:41', '2020-06-20 10:57:29'),
(3, 1, 'Night Lights', 0, '2020-06-19 04:22:52', '2020-06-20 10:56:54'),
(4, 1, 'Test 2', 0, '2020-06-19 04:30:38', '2020-06-19 04:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `scene_device`
--

CREATE TABLE `scene_device` (
  `scene_device_id` int(18) NOT NULL,
  `scene_id` int(18) NOT NULL,
  `device_id` int(18) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `scene_device`
--

INSERT INTO `scene_device` (`scene_device_id`, `scene_id`, `device_id`, `name`, `data`, `createdAt`, `updatedAt`) VALUES
(16, 1, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(17, 2, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(18, 3, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(19, 4, 1, 'Weather Sensor # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(20, 1, 2, 'Power Switch # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(21, 2, 2, 'Power Switch # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(22, 3, 2, 'Power Switch # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(23, 4, 2, 'Power Switch # 0', '0', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(24, 1, 3, 'Power Switch # 1', '1', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(25, 2, 3, 'Power Switch # 1', '1', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(26, 3, 3, 'Power Switch # 1', '1', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(27, 4, 3, 'Power Switch # 1', '1', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(28, 1, 4, 'Power Switch # 2', '2', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(29, 2, 4, 'Power Switch # 2', '2', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(30, 3, 4, 'Power Switch # 2', '2', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(31, 4, 4, 'Power Switch # 2', '2', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(32, 1, 5, 'Power Switch # 3', '3', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(33, 2, 5, 'Power Switch # 3', '3', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(34, 3, 5, 'Power Switch # 3', '3', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(35, 4, 5, 'Power Switch # 3', '3', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(36, 1, 6, 'Power Switch # 4', '4', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(37, 2, 6, 'Power Switch # 4', '4', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(38, 3, 6, 'Power Switch # 4', '4', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(39, 4, 6, 'Power Switch # 4', '4', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(40, 1, 7, 'Power Switch # 5', '5', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(41, 2, 7, 'Power Switch # 5', '5', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(42, 3, 7, 'Power Switch # 5', '5', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(43, 4, 7, 'Power Switch # 5', '5', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(44, 1, 8, 'Power Switch # 6', '6', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(45, 2, 8, 'Power Switch # 6', '6', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(46, 3, 8, 'Power Switch # 6', '6', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(47, 4, 8, 'Power Switch # 6', '6', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(48, 1, 9, 'Power Switch # 7', '7', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(49, 2, 9, 'Power Switch # 7', '7', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(50, 3, 9, 'Power Switch # 7', '7', '2020-06-21 21:54:12', '2020-06-21 21:54:12'),
(51, 4, 9, 'Power Switch # 7', '7', '2020-06-21 21:54:12', '2020-06-21 21:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(18) NOT NULL,
  `account_id` int(18) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `pin` varchar(45) DEFAULT NULL,
  `card` varchar(45) DEFAULT NULL,
  `finger_id` varchar(45) DEFAULT NULL,
  `card_captured` int(11) DEFAULT NULL,
  `pin_captured` int(11) DEFAULT NULL,
  `finger_captured` int(11) DEFAULT NULL,
  `card_added_date` datetime DEFAULT NULL,
  `card_updated_date` datetime DEFAULT NULL,
  `finger_added_date` datetime DEFAULT NULL,
  `finger_updated_date` datetime DEFAULT NULL,
  `pin_added_date` datetime DEFAULT NULL,
  `pin_updated_date` datetime DEFAULT NULL,
  `card_enabled` int(11) DEFAULT NULL,
  `pin_enabled` int(11) DEFAULT NULL,
  `finger_enabled` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `profile_id` int(18) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `account_id`, `surname`, `firstname`, `email`, `phone`, `username`, `password`, `pin`, `card`, `finger_id`, `card_captured`, `pin_captured`, `finger_captured`, `card_added_date`, `card_updated_date`, `finger_added_date`, `finger_updated_date`, `pin_added_date`, `pin_updated_date`, `card_enabled`, `pin_enabled`, `finger_enabled`, `status`, `profile_id`, `createdAt`, `updatedAt`) VALUES
(2, 1, 'test', 'name', 'email here', '575765', NULL, NULL, NULL, NULL, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 2, '2020-07-04 02:14:40', '2020-07-04 12:39:19'),
(3, 1, 'Laureline', 'Nampijja', 'laureline.nampijja@gmail.com', '0751345334', NULL, NULL, NULL, NULL, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 2, '2020-07-04 02:25:25', '2020-07-05 14:56:13'),
(4, 1, 'Naome', 'Slyvia', 'naome.sylvia@gmail.com', '0751345334', NULL, NULL, NULL, NULL, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 2, '2020-07-04 17:15:31', '2020-07-04 17:15:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Indexes for table `account_settings`
--
ALTER TABLE `account_settings`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `account_code` (`account_code`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `command`
--
ALTER TABLE `command`
  ADD PRIMARY KEY (`command_id`),
  ADD KEY `device_type_id` (`device_type_id`);

--
-- Indexes for table `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`control_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `control_device`
--
ALTER TABLE `control_device`
  ADD PRIMARY KEY (`control_device_id`),
  ADD KEY `control_id` (`control_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`device_id`),
  ADD KEY `node_id` (`node_id`),
  ADD KEY `device_type_id` (`device_type_id`);

--
-- Indexes for table `device_type`
--
ALTER TABLE `device_type`
  ADD PRIMARY KEY (`device_type_id`);

--
-- Indexes for table `network`
--
ALTER TABLE `network`
  ADD PRIMARY KEY (`network_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `node`
--
ALTER TABLE `node`
  ADD PRIMARY KEY (`node_id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `node_type_id` (`node_type_id`);

--
-- Indexes for table `node_data`
--
ALTER TABLE `node_data`
  ADD PRIMARY KEY (`node_data_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `node_device`
--
ALTER TABLE `node_device`
  ADD PRIMARY KEY (`node_device_id`),
  ADD KEY `node_type_id` (`node_type_id`),
  ADD KEY `device_type_id` (`device_type_id`);

--
-- Indexes for table `node_logs`
--
ALTER TABLE `node_logs`
  ADD PRIMARY KEY (`node_log_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `node_type`
--
ALTER TABLE `node_type`
  ADD PRIMARY KEY (`node_type_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`room_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `room_device`
--
ALTER TABLE `room_device`
  ADD PRIMARY KEY (`room_device_id`),
  ADD KEY `device_id` (`device_id`),
  ADD KEY `room_id` (`room_id`);

--
-- Indexes for table `scene`
--
ALTER TABLE `scene`
  ADD PRIMARY KEY (`scene_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `scene_device`
--
ALTER TABLE `scene_device`
  ADD PRIMARY KEY (`scene_device_id`),
  ADD KEY `scene_id` (`scene_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `profile_id` (`profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `account_settings`
--
ALTER TABLE `account_settings`
  MODIFY `setting_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `command`
--
ALTER TABLE `command`
  MODIFY `command_id` int(18) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `control`
--
ALTER TABLE `control`
  MODIFY `control_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `control_device`
--
ALTER TABLE `control_device`
  MODIFY `control_device_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `device_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `device_type`
--
ALTER TABLE `device_type`
  MODIFY `device_type_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `network`
--
ALTER TABLE `network`
  MODIFY `network_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `node`
--
ALTER TABLE `node`
  MODIFY `node_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `node_data`
--
ALTER TABLE `node_data`
  MODIFY `node_data_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `node_device`
--
ALTER TABLE `node_device`
  MODIFY `node_device_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `node_logs`
--
ALTER TABLE `node_logs`
  MODIFY `node_log_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `node_type`
--
ALTER TABLE `node_type`
  MODIFY `node_type_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `profile_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `room_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `room_device`
--
ALTER TABLE `room_device`
  MODIFY `room_device_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `scene`
--
ALTER TABLE `scene`
  MODIFY `scene_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scene_device`
--
ALTER TABLE `scene_device`
  MODIFY `scene_device_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(18) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_settings`
--
ALTER TABLE `account_settings`
  ADD CONSTRAINT `account_settings_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `command`
--
ALTER TABLE `command`
  ADD CONSTRAINT `command_ibfk_1` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`device_type_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `control`
--
ALTER TABLE `control`
  ADD CONSTRAINT `control_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `control_device`
--
ALTER TABLE `control_device`
  ADD CONSTRAINT `control_device_ibfk_2` FOREIGN KEY (`control_id`) REFERENCES `control` (`control_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `control_device_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `device`
--
ALTER TABLE `device`
  ADD CONSTRAINT `device_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `node` (`node_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `device_ibfk_2` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`device_type_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `network`
--
ALTER TABLE `network`
  ADD CONSTRAINT `network_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `node`
--
ALTER TABLE `node`
  ADD CONSTRAINT `node_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `node_ibfk_2` FOREIGN KEY (`node_type_id`) REFERENCES `node_type` (`node_type_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `node_data`
--
ALTER TABLE `node_data`
  ADD CONSTRAINT `node_data_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `node` (`node_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `node_device`
--
ALTER TABLE `node_device`
  ADD CONSTRAINT `node_device_ibfk_1` FOREIGN KEY (`node_type_id`) REFERENCES `node_type` (`node_type_id`),
  ADD CONSTRAINT `node_device_ibfk_2` FOREIGN KEY (`device_type_id`) REFERENCES `device_type` (`device_type_id`);

--
-- Constraints for table `node_logs`
--
ALTER TABLE `node_logs`
  ADD CONSTRAINT `node_logs_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `node` (`node_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `room_device`
--
ALTER TABLE `room_device`
  ADD CONSTRAINT `room_device_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `room_device_ibfk_3` FOREIGN KEY (`room_id`) REFERENCES `room` (`room_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `scene`
--
ALTER TABLE `scene`
  ADD CONSTRAINT `scene_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `scene_device`
--
ALTER TABLE `scene_device`
  ADD CONSTRAINT `scene_device_ibfk_2` FOREIGN KEY (`scene_id`) REFERENCES `scene` (`scene_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `scene_device_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
