DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_status`(
in p_room varchar(20), in p_nodeid varchar(20), 
in p_voltage varchar(20), in p_ram varchar(20), 
in p_rom  varchar(20), in p_software_version  varchar(20), 
in p_hardware_version varchar(20),in p_processor varchar(20),
in p_temperature varchar(20), in p_sensor_data  varchar(20),
in p_node_type varchar(20)
)
BEGIN
declare v_node_id int(18) default 0;  
 
select `node_id` into v_node_id  from `node` where `node_address`=p_nodeid and `account_id` = (select `account_id` from `account_settings` where `account_code`= p_room );
if (v_node_id > 0) then
	 
		update `node`
		set `status` = 1, `last_active` = current_timestamp, `voltage` = p_voltage,
			`memory` =  p_ram, `rom` =  p_rom, `software_version` =  p_software_version,
			`hardware_version` =  p_hardware_version, `processor` =  p_processor, 
            `temperature` = p_temperature, `updatedAt` =  current_timestamp
		where `node_id`= v_node_id;
		insert into `node_data`(`node_id`, `data`, `createdAt`) values  (v_node_id, p_sensor_data, current_timestamp);
        
	 
else 
	 
		insert into `node`(`account_id`, `node_type_id`, `node_address`, `status`, `last_active`, `voltage`, `memory`, `rom`, `software_version`, `hardware_version`, `processor`, `temperature`, `createdAt`) 
		values ((select `account_id` from `account_settings` where `account_code`= p_room ), p_node_type,
		p_nodeid, 1, current_timestamp, p_voltage, p_ram, p_rom, p_software_version, p_hardware_version,p_processor,p_temperature,current_timestamp);
		
		select `node_id` into v_node_id  from `node` where `node_address`=p_nodeid and `account_id` = (select `account_id` from `account_settings` where `account_code`= p_room );
		if (v_node_id > 0) then
            insert into `node_data`(`node_id`, `data`, `createdAt`) values  (v_node_id, p_sensor_data, current_timestamp);
		    insert into `device`(`node_id`,`device_type_id`,`name`,`status`,`address`,`createdAt`)
			select `node`.`node_id`, `node_device`.`device_type_id`,
			CONCAT( `device_type`.`name`, ' # ',`node_device`.`value`) 'name',
			1, `node_device`.`value`, current_timestamp
			 from `node_device` inner join `node_type` on `node_device`.`node_type_id` = `node_type`.`node_type_id`
			 inner join `device_type` on `node_device`.`device_type_id` = `device_type`.`device_type_id` 
			 inner join `node` on `node`.`node_type_id` = `node_device`.`node_type_id`
			 where `node`.`node_address` = p_nodeid;          
           
		end if;

  end if;
END$$
DELIMITER ;