DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_network`(IN `p_room` VARCHAR(20), IN `p_network` TEXT)
BEGIN
declare v_account_id int(18) default 0;
declare v_network_id int(18) default 0;

select `account_id` into v_account_id from `account_settings` where `account_code`= p_room ;

if (v_account_id > 0) then
	    select  `network_id` into v_network_id from `network` where `account_id` = v_account_id;
         if (v_network_id > 0) then
           update `network` SET `data`= p_network  where `network_id`= v_network_id;
         else
           insert into `network`(`account_id`, `data`, `createdAt`) values (v_account_id,p_network,current_timestamp);
         end if;  
  end if;
END$$
DELIMITER ;