var express = require('express');
var router = express.Router();
var model = require('../data/models/index');
 

router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.control_device.findAll({
        where: { control_id: id },
        include: [{ model: models.device, include: [{ model: models.node, include: [{ model: models.note_type }] }] }]
    })
		.then(result => res.json({
			success: true,
			data: result,
			error:null
		}))
		.catch(error => res.json({
			success: false,
			data: null,
			error: error 
		}))
		;
});

 
router.post('/', function (req, res, next) {
	
	const {control_id, device_id, name, data } = req.body;

	model.control_device
    .create({ control_id: control_id, device_id: device_id, name: name, data: data, createdAt : new Date() })
	.then(result => res.status(200).json({ success: true,data: result, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error }));

});

 
router.put('/:id', function (req, res, next) {

	const id = req.params.id;
	const { control_id , device_id , name , data  } = req.body;

	model.control_device
	.update({control_id: control_id , device_id: device_id , name: name , data: data },
         { where: { control_device_id: id }	})
	.then(result => res.status(200).json({ success: true,data: null, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error	}));
});

 
router.delete('/:id', function (req, res, next) {
	const id = req.params.id;
	model.control_device
	.destroy({ where: { control_device_id: id }	})
	.then(result => res.status(200).json({success: true, data: null, error: null }))
	.catch(error => res.json({success: false, data: null, error: error }));
});

module.exports = router;