var express = require('express');
var router = express.Router();
var models = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;

    models.node.findOne({ where:{ node_id: id}, include:[ { model: models.node_type}] })
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: null,
            error: error
        }))
        ;
});

router.get('/list/:accountid', function (req, res, next) {
    const id = req.params.accountid;

    models.node.findAll({ where:{ account_id: id}, 
        include:[ { model: models.node_type}, { model: models.device, include:[{ model: models.device_type}] }] })
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: null,
            error: error
        }))
        ;
});


router.post('/', function (req, res, next) {

    const {
        account_id,
        node_type_id,
        node_address,
        status,
        last_active,
        voltage,
        memory,
        rom,
        software_version,
        hardware_version,
        processor  
    } = req.body;

    models.node
        .create({ 
            account_id: account_id,
            node_type_id: node_type_id,
            node_address: node_address,
            status: status,
            last_active: last_active,
            voltage: voltage,
            memory: memory,
            rom: rom,
            software_version: software_version,
            hardware_version: hardware_version,
            processor: processor,
            createdAt: new Date() })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const { 
        account_id,
        node_type_id,
        node_address,
        status,
        last_active,
        voltage,
        memory,
        rom,
        software_version,
        hardware_version,
        processor
    } = req.body;

    models.node
        .update({ 
            ccount_id: account_id,
            node_type_id: node_type_id,
            node_address: node_address,
            status: status,
            last_active: last_active,
            voltage: voltage,
            memory: memory,
            rom: rom,
            software_version: software_version,
            hardware_version: hardware_version,
            processor: processor
         }, { where: { node_id: id } })
        .then(result => res.status(200).json({ success: true, data:null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    models.node
        .destroy({ where: { node_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

module.exports = router;