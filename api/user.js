var express = require('express');
const { QueryTypes } = require('sequelize')
var router = express.Router();
var models = require('../data/models/index');
const Op = models.Sequelize.Op;

router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.user.findOne({ where: { account_id: id } })
    .then(result => {
        if(result != null){ res.json({ success: true, data: result, error:null }) }
        else{ res.json({ success: false, data: result, error:'No records found' }) }
        }
    )
    .catch(error => res.json({
            success: false,
            data: null,
            error: error
        }))
        ;
});


router.post('/', function (req, res, next) {

    const {
        account_id, surname, firstname, email,
        phone, username, password, pin,
        card, finger_id, card_captured, pin_captured,
        finger_captured, card_added_date,
        card_updated_date, finger_added_date,
        finger_updated_date, pin_added_date,
        pin_updated_date, card_enabled,
        pin_enabled, finger_enabled,
        status, profile_id
    } = req.body;

    models.user
        .create({
            account_id: account_id, surname: surname,
            firstname: firstname, email: email,
            phone: phone, username: username,
            password: password, pin: pin,
            card: card, finger_id: finger_id,
            card_captured: card_captured,
            pin_captured: pin_captured,
            finger_captured: finger_captured,
            card_added_date: card_added_date,
            card_updated_date: card_updated_date,
            finger_added_date: finger_added_date,
            finger_updated_date: finger_updated_date,
            pin_added_date: pin_added_date,
            pin_updated_date: pin_updated_date,
            card_enabled: card_enabled,
            pin_enabled: pin_enabled,
            finger_enabled: finger_enabled,
            status: status,
            profile_id: profile_id,
            createdAt: new Date()
        })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: [], error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const {
        account_id, surname, firstname, email,
        phone, username, password, pin,
        card, finger_id, card_captured, pin_captured,
        finger_captured, card_added_date,
        card_updated_date, finger_added_date,
        finger_updated_date, pin_added_date,
        pin_updated_date, card_enabled,
        pin_enabled, finger_enabled,
        status, profile_id
    } = req.body;

    models.user
        .update({
            account_id: account_id, surname: surname,
            firstname: firstname, email: email,
            phone: phone, username: username,
            password: password, pin: pin,
            card: card, finger_id: finger_id,
            card_captured: card_captured,
            pin_captured: pin_captured,
            finger_captured: finger_captured,
            card_added_date: card_added_date,
            card_updated_date: card_updated_date,
            finger_added_date: finger_added_date,
            finger_updated_date: finger_updated_date,
            pin_added_date: pin_added_date,
            pin_updated_date: pin_updated_date,
            card_enabled: card_enabled,
            pin_enabled: pin_enabled,
            finger_enabled: finger_enabled,
            status: status,
            profile_id: profile_id
        }, { where: { user_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    models.user
        .destroy({ where: { user_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

router.post('/login', function (req, res, next) {
	
	const { username, password } = req.body;
  
	models.user.findOne({ where:{ password: password,
		[Op.or]: [ {username: username}, {email: username}, {phone : username}] }})
		.then(result => {
			if(result != null){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: null, error:'Invalid login' }) }
			}
		)
		.catch(error => res.json({
			success: false,
			data: null,
			error: error.name 
		}))
		;

});

module.exports = router;