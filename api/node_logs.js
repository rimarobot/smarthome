var express = require('express');
var router = express.Router();
var models = require('../data/models/index');


router.get('/list/:accountid', function (req, res, next) {
    const id = req.params.accountid;
    models.node_logs.findAll({ include:[ {model: models.node, where:{ account_id: id }, include:[ {model: models.node_type}] } ] })
    .then(result => {
        if(result != null && result.length > 0){ res.json({ success: true, data: result, error:null }) }
        else{ res.json({ success: false, data: null, error:'No records found' }) }
    })
    .catch(error => res.json({ success: false, data: null, error: error })) ;
});


module.exports = router;