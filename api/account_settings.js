var express = require('express');
var router = express.Router();
var model = require('../data/models/index'); 
const { v4: uuidv4 } = require('uuid');

/* GET account listing. */
router.get('/:id', function (req, res, next) {
	const id = req.params.id;
	model.account_settings.findOne({ where: { account_id: id }})
		.then(result => res.json({
			success: true,
			data: result,
			error: null
		}))
		.catch(error => res.json({
			success: false,
			data: null,
			error: error
		}));
});


/* POST account. */
router.post('/', function (req, res, next) { 
	const {  account_id,		gateway_ssid,		gateway_password } = req.body;
    var uuid = uuidv4();
	
	var _account_code =   (''+ account_id).padStart(7, '0'),
	_mesh_ssid = 'SHM' + uuid.substring(8, 4),
	_mesh_password = uuid.substring(0, 8),
	_mesh_port = 5555, _mesh_channel = 1, _mesh_hidden=1,_mesh_connections = 3,
	_host_address = '192.168.1.100' , _host_port =3000;
	  

	model.account_settings
	.create({ account_id: account_id,		account_code: _account_code,
		mesh_ssid: _mesh_ssid,		mesh_password: _mesh_password,		mesh_port: _mesh_port,
		mesh_channel: _mesh_channel,		mesh_hidden: _mesh_hidden,		mesh_connections: _mesh_connections,
		gateway_ssid: gateway_ssid,		gateway_password: gateway_password,		host_address: _host_address,
		host_port: _host_port,		createdAt: new Date() })
	.then(result => res.status(200).json({ success: true,data: result, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error }));

});


/* update todo. */
router.put('/:id', function (req, res, next) {

	const id = req.params.id;
	const { 
		account_id,		account_code,		account_hash,		mesh_ssid,
		mesh_password,		mesh_port,		mesh_channel,		mesh_hidden,
		mesh_connections,		gateway_ssid,		gateway_password,
		host_address,		host_port  } = req.body;

	model.account_settings
	.update({
		    account_id: account_id,		account_code: account_code,		account_hash: account_hash,
			mesh_ssid: mesh_ssid,		mesh_password: mesh_password,		mesh_port: mesh_port,
			mesh_channel: mesh_channel,		mesh_hidden: mesh_hidden,		mesh_connections: mesh_connections,
			gateway_ssid: gateway_ssid,		gateway_password: gateway_password,		host_address: host_address,
			host_port: host_port,		createdAt: new Date() 
	    }, { where: { setting_id: id }	})
	.then(result => res.status(200).json({ success: true,data: null, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error	}));
});

/* Delete todo. */
router.delete('/:id', function (req, res, next) {
	const id = req.params.id;
	model.account_settings
	.destroy({ where: { setting_id: id }	})
	.then(result => res.status(200).json({success: true, data: null, error: null }))
	.catch(error => res.json({success: false, data: null, error: error }));
});

module.exports = router;