
module.exports = (app)=>{
   

  app.use('/account', require('./account'));
  app.use('/account-settings', require('./account_settings'));
  app.use('/command', require('./command'));
  app.use('/control', require('./control'));
  app.use('/control-device', require('./control_device'));
  app.use('/device', require('./device'));
  app.use('/device-type', require('./device_type'));
  app.use('/network', require('./network'));
  app.use('/node', require('./node'));
  app.use('/node-type', require('./node_type'));
  app.use('/node-logs', require('./node_logs'));
  app.use('/node-data', require('./node_data'));
  app.use('/profile', require('./profile'));
  app.use('/room', require('./room'));
  app.use('/room-device', require('./room_device'));
  app.use('/scene', require('./scene'));
  app.use('/scene-device', require('./scene_device'));
  app.use('/user', require('./user')); 

  return app;
};




