var express = require('express');
var router = express.Router();
var models = require('../data/models/index');
const Op = models.Sequelize.Op;
/* GET account listing. */
router.get('/:accountid', function (req, res, next) {
	const id = req.params.accountid;
  
	models.account.findOne({ where:{ account_id: id},
		include:[{ model: models.account_settings }, { model: models.user }, { model: models.network }] })
		.then(result => {
			if(result != null){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: result, error:'Invalid account' }) }
			}
		)
		.catch(error => res.json({
			success: false,
			data: null,
			error: error.name 
		}))
		;
});


/* POST account. */
router.post('/', function (req, res, next) {
	
	const { surname, firstname, email, phone, country, address, 
		town, district, username, gender, dob, status, password } = req.body;

		models.account
	.create({ surname: surname, firstname: firstname, email: email,
		 phone: phone, country: country, address: address,
		town: town, district: district, username: username,password: password, 
		gender: gender, dob: dob, status: 1, createdAt: new Date() })
	.then(result => res.status(200).json({ success: true,data: result, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error.name }));

});

/* POST account. */
router.post('/login', function (req, res, next) {
	
	const { username, password } = req.body;
  
	models.account.findOne({ where:{ password: password,
		[Op.or]: [ {username: username}, {email: username}, {phone : username}] },
		include:[{ model: models.account_settings }, { model: models.user }, { model: models.network }] })
		.then(result => {
			if(result != null){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: result, error:'Invalid login' }) }
			}
		)
		.catch(error => res.json({
			success: false,
			data: null,
			error: error.name 
		}))
		;

});


/* update todo. */
router.put('/:id', function (req, res, next) {

	const id = req.params.id;

	const { surname, firstname, email, phone, country, address, 
		town, district, username, gender, dob, status } = req.body;

		models.account
	.update({
		surname: surname, firstname: firstname, email: email,
		 phone: phone, country: country, address: address,
		town: town, district: district, username: username, 
		gender: gender, dob: dob, status: status, updatedAt: new Date() 
	    }, { where: { account_id: id }	})
	.then(result => res.status(200).json({ success: true,data: null, error: null  }))
	.catch(error => res.json({ success: false, data: null, error: error	}));
});

/* Delete todo. */
router.delete('/:id', function (req, res, next) {
	const id = req.params.id;
	models.account
	.destroy({ where: { account_id: id }	})
	.then(result => res.status(200).json({success: true, data: null, error: null  }))
	.catch(error => res.json({success: false, data: null, error: error }));
});

module.exports = router;