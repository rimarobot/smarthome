var express = require('express');
var router = express.Router();
var model = require('../data/models/index');


router.get('/', function (req, res, next) {
    const id = req.params.id;
    model.node_type.findAll({})
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: [],
            error: error
        }))
        ;
});

 

module.exports = router;