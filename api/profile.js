var express = require('express');
var router = express.Router();
var models = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.profile.findAll({ where: { profile_id: id }})
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: [],
            error: error
        }))
        ;
});

 

module.exports = router;