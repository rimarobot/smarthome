var express = require('express');
var router = express.Router();
var model = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;

    model.network.findOne({ where:{ account_id: id} })
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: null,
            error: error
        }))
        ;
});


router.post('/', function (req, res, next) {

    const { account_id, data } = req.body;
    model.network
        .create({ account_id: account_id,  data: data, createdAt: new Date() })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const { account_id, data} = req.body;

    model.network
        .update({ account_id: account_id,  data: data }, { where: { network_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    model.network
        .destroy({ where: { network_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

module.exports = router;