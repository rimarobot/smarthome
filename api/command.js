var express = require('express');
var router = express.Router();
var model = require('../data/models/index');
 

router.get('/', function (req, res, next) {
	model.command.findAll({ })
		.then(result => res.json({
			success: true,
			data: result,
			error:null
		}))
		.catch(error => res.json({
			success: false,
			data: null,
			error: error 
		}))
		;
});

 
router.post('/', function (req, res, next) {
	
	const {device_type_id,
		command_template ,
		command_decription} = req.body;

	model.command
    .create({ device_type_id: device_type_id, command_template: command_template, 
        command_decription: command_decription, createdAt: new Date() })
	.then(result => res.status(200).json({ success: true,data: result, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error }));

});

 
router.put('/:id', function (req, res, next) {

	const id = req.params.id;

	const { device_type_id,
		command_template ,
		command_decription } = req.body;

	model.command
	.update({device_type_id: device_type_id, command_template: command_template, 
        command_decription: command_decription    },
         { where: { command_id: id }	})
	.then(result => res.status(200).json({ success: true,data: null, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error	}));
});

 
router.delete('/:id', function (req, res, next) {
	const id = req.params.id;
	model.command
	.destroy({ where: { command_id: id }	})
	.then(result => res.status(200).json({success: true, data: null, error: null }))
	.catch(error => res.json({success: false, data: null, error: error }));
});

module.exports = router;