var express = require('express');
var router = express.Router();
var models = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.room_device.findAll({ where:{room_id: id},
    include:[ { model: models.device,  include:[ { model:models.node ,  include:[ {model:models.note_type}]}]}] })
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: [],
            error: error
        }))
        ;
});


router.post('/', function (req, res, next) {

    const { 
        device_id,
        room_id,
        name 
    } = req.body;

    models.room_device
        .create({ device_id: device_id, room_id: room_id, name: name, createdAt: new Date() })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: [], error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const { device_id,
        room_id,
        name
    } = req.body;

    models.room_device
        .update({ device_id: device_id, room_id: room_id, name: name }, { where: { room_device_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    models.room_device
        .destroy({ where: { room_device_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

module.exports = router;