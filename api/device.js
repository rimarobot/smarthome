var express = require('express');
var router = express.Router();
var models = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.device.findOne( 
        { where:{ device_id: id }, 
        include:[ {model: models.node, include:[ {model: models.node_type}] },
         {model: models.device_type }] })
         .then(result => {
			if(result != null){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: null, error:'No records found' }) }
			})
		.catch(errors => res.json({
            success: false,
            data: [],
            error: errors
        }));
});

router.get('/list/:accountid', function (req, res, next) {
    const id = req.params.accountid;
    models.device.findAll( 
        { include:[ {model: models.node, where:{ account_id: id }, include:[ {model: models.node_type}] },
         {model: models.device_type }] })
         .then(result => {
			if(result != null && result.length > 0){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: null, error:'No records found' }) }
			})
		.catch(errors => res.json({
            success: false,
            data: null,
            error: errors
        }));
});


router.post('/', function (req, res, next) {

    const { node_id, device_type_id, name } = req.body;
    models.device
        .create({ node_id: node_id, device_type_id: device_type_id, name: name, createdAt: new Date() })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const { node_id, device_type_id, name } = req.body;

    models.device
        .update({ node_id: node_id, device_type_id: device_type_id, name: name }, { where: { device_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    models.device
        .destroy({ where: { device_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

module.exports = router;