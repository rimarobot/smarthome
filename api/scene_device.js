var express = require('express');
var router = express.Router();
var model = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.scene_device.findAll({
        where: { scene_id: id },
        include: [{ model: models.device, include: [{ model: models.node, include: [{ model: models.note_type }] }] }]
    })
        .then(result => res.json({
            success: true,
            data: result,
            error: null
        }))
        .catch(error => res.json({
            success: false,
            data: [],
            error: error
        }))
        ;
});


router.post('/', function (req, res, next) {

    const {
        scene_id,
        device_id,
        name,
        data
    } = req.body;
    model.scene_device
        .create({
            scene_id: scene_id,
            device_id: device_id,
            name: name,
            data: data, createdAt: new Date()
        })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: [], error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const {
        scene_id,
        device_id,
        name,
        data
    } = req.body;

    model.scene_device
        .update({
            scene_id: scene_id,
            device_id: device_id,
            name: name,
            data: data
        }, { where: { network_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    model.scene_device
        .destroy({ where: { network_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

module.exports = router;