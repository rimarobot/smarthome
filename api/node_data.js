var express = require('express');
var router = express.Router();
var model = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    model.node_data.findAll({ where: { node_id: id }})
    .then(result => {
        if(result != null){ res.json({ success: true, data: result, error:null }) }
        else{ res.json({ success: false, data: result, error:'No records found' }) }
        }
    )
    .catch(error => res.json({
        success: false,
        data: null,
        error: error.name 
    }))
    ;
}); 
module.exports = router;