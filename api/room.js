var express = require('express');
var router = express.Router();
var models = require('../data/models/index');


router.get('/:id', function (req, res, next) {
    const id = req.params.id;
    models.room.findOne({  where:{ room_id: id} ,
        include: [ { model: models.room_device, include: [{ model: models.device, include:[{ model: models.node}, {model:models.device_type}]}]}] })
        .then(result => {
			if(result != null){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: result, error:'No records found' }) }
			})
        .catch(error => res.json({
            success: false,
            data: [],
            error: error
        }))
        ;
});

router.get('/list/:accountid', function (req, res, next) {
    const id = req.params.accountid;
    models.room.findAll({  where:{ account_id: id} ,
        include: [ { model: models.room_device, include: [{ model: models.device, include:[{ model: models.node}, {model:models.device_type}]}]}] })
        .then(result => {
			if(result != null && result.length > 0){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: null, error:'No records found' }) }
			})
		.catch(error => res.json({
            success: false,
            data: [],
            error: error
        }))
        ;
});

router.post('/', function (req, res, next) {

    const { account_id, name, image } = req.body;
    models.room
        .create({ account_id: account_id,  name: name, image: image, createdAt: new Date() })
        .then(result => res.status(200).json({ success: true, data: result, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));

});


router.put('/:id', function (req, res, next) {

    const id = req.params.id;
    const { account_id, name} = req.body;

    models.room
        .update({ account_id: account_id,  name: name, image: image}, { where: { room_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});


router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    models.room
        .destroy({ where: { room_id: id } })
        .then(result => res.status(200).json({ success: true, data: null, error: null }))
        .catch(error => res.json({ success: false, data: null, error: error }));
});

module.exports = router;