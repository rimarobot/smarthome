var express = require('express');
var router = express.Router();
var models = require('../data/models/index');
 

router.get('/:id', function (req, res, next) {
    const id = req.params.id;
	models.control.findOne({ where:{ control_id: id }, 
		include: [ { model: models.control_device, include: [{ model: models.device, include:[{ model: models.node}, {model:models.device_type}]}]}] })
		.then(result => {
			if(result != null){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: result, error:'No records found' }) }
			})
		.catch(error => res.json({
			success: false,
			data: null,
			error: error 
		}))
		;
});

router.get('/list/:accountid', function (req, res, next) {
    const id = req.params.accountid;
	models.control.findAll({ where:{ account_id: id }, 
		include: [ { model: models.control_device, include: [{ model: models.device, include:[{ model: models.node}, {model:models.device_type}]}]}] })
		.then(result => {
			if(result != null && result.length > 0){ res.json({ success: true, data: result, error:null }) }
			else{ res.json({ success: false, data: null, error:'No records found' }) }
			})
		.catch(error => res.json({
			success: false,
			data: null,
			error: error 
		}))
		;
});

 
router.post('/', function (req, res, next) {
	
	const {account_id , name, startTime, endTime, frequency} = req.body;
	models.control
    .create({ account_id: account_id , name: name, startTime: startTime, endTime: endTime, frequency: frequency,  createdAt : new Date() })
	.then(result => res.status(200).json({ success: true,data: result, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error }));

});

 
router.put('/:id', function (req, res, next) {

	const id = req.params.id;
	const { account_id , name , startTime, endTime, frequency } = req.body;

	models.control
	.update({account_id: account_id , name: name, startTime: startTime, endTime: endTime, frequency: frequency  }, { where: { control_id: id }	})
	.then(result => res.status(200).json({ success: true,data: null, error: null }))
	.catch(error => res.json({ success: false, data:null, error: error	}));
});

 
router.delete('/:id', function (req, res, next) {
	const id = req.params.id;
	models.control
	.destroy({ where: { control_id: id }	})
	.then(result => res.status(200).json({success: true, data: null, error: null }))
	.catch(error => res.json({success: false, data: null, error: error }));
});

module.exports = router;