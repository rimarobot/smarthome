var express = require('express');
var router = express.Router();
var model = require('../data/models/index');
 

router.get('/', function (req, res, next) {
    
	model.device_type.findAll({})
		.then(result => res.json({
			success: true,
			data: result,
			error:null
		}))
		.catch(error => res.json({
			success: false,
			data: null,
			error: error 
		}))
		;
});

 
router.post('/', function (req, res, next) {
	
	const {name , decription } = req.body;
	model.device_type
    .create({ name: name , decription: decription, createdAt : new Date() })
	.then(result => res.status(200).json({ success: true,data: result, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error }));

});

 
router.put('/:id', function (req, res, next) {

	const id = req.params.id;
	const { name , decription } = req.body;

	model.device_type
	.update({name: name , decription: decription }, { where: { device_type_id: id }	})
	.then(result => res.status(200).json({ success: true,data: null, error: null }))
	.catch(error => res.json({ success: false, data: null, error: error	}));
});

 
router.delete('/:id', function (req, res, next) {
	const id = req.params.id;
	model.device_type
	.destroy({ where: { device_type_id: id }	})
	.then(result => res.status(200).json({success: true, data: null, error: null }))
	.catch(error => res.json({success: false, data: null, error: error }));
});

module.exports = router;