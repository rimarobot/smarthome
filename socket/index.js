
module.exports = (server) => {
    //socket IO server installation
    // const { QueryTypes } = require('sequelize')
    // var models = require('../data/models/index');

    var io = require('socket.io')(server);
     
    io.on('connection', function (socket) {

        console.log(`Socket ${socket.id} connected.`); 
        //login
        //This event bus logs in all users including the hub user and 
        //creates a chatroom  for them to contain thier conversation.
        socket.on('login', function (payload) {
            require('./login')('login', io, socket, payload);
        });

        //registration
        //This event bus captures new or updated biometric details from
        // the hub and save them in the database
        socket.on('register', function (payload) {
            require('./registration')('register', io, socket, payload);
        });

        //routines
        //This event bus checks for co-routines ready to be executed and sends them to the 
        //hub network 
        socket.on('routines', function (payload) {
            require('./routines')('routines', io, socket, payload);
        });
        //status
        //This event bus recieve status data of all nodes in the hub network and saves them 
        // in the database 
        socket.on('status', function (payload) {
            
            require('./status')('status', io, socket, payload);
        });
        //weather
        //This event bus periodically checks for weather updates and sends them to the 
        // hub for action
        socket.on('weather', function (payload) {
            require('./weather')('weather', io, socket, payload);
        });
        
        //command 
        //User sends commands from the client end device on this event bus and 
        //it forwards them to the hub which acts accordingly.
        socket.on('command', function (payload) {
            require('./command')('command', io, socket, payload);
        });

        //network 
        //The hub sends network details to the database
        socket.on('network', function (payload) {
            require('./network')('network', io, socket, payload);
        });

        //verification 
        //The hub send biometric details to the server for verification.
        socket.on('verify', function (payload) {
            require('./verification')('verify', io, socket, payload);
        });



        //logout users to update others about disconnection
        socket.on('disconnect', function () { 
            socket.broadcast.to(socket.room).emit('logout', JSON.stringify({ 'msg': socket.username +' has left' }));
            // models.sequelize.query(
            //     'UPDATE `account_settings` SET `account_hash`=\'\'  WHERE `account_code`=:room AND `root_address`=:address ',
            //     { replacements: { room: data.room, address: data.address }, type: QueryTypes.UPDATE, raw: true });
        });

    });

    return io;

}