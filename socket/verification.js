module.exports = (event, io, socket, payload) => {
    const { QueryTypes } = require('sequelize')
    var models = require('../data/models/index');
    var vdata = payload;



    /*
    Defination
     C => command type                       N => node id                    V => voltage                            X => memory
     Y => rom                                F => software version           H => hardware version                   P => processor
     E => temperature                        D => data {sensor data}         T => node type
     M => command direction
    
    Work flow
    from root node {room:***,node:***,type:**,command:***, data:[card,pin,finger id]}
    user inputs details on the terminal [fingerprint, card, pin] for verification
    the node send http data to hub which now forwards the request to server via socket for approval 
    eg { 'room':value, 'node':source node id,  'data': 'T:node type#M:1#C:command type#I:user id#D:usermane [10 characters]#S:status#', 'dir': direction }
    the server then approves the request  and send aprrovel status to the hub. 


    */

    models.sequelize
        .query('SELECT `user`.* FROM `user` INNER JOIN `account_settings` ON `user`.`account_id` = `account_settings`.`account_id` ' +
            ' WHERE `account_settings`.`account_code` =:room AND ((`user`.`finger_id`=:data AND `user`.`finger_enabled` = 1) OR ' +
            ' (`user`.`pin`=:data  AND `user`.`pin_enabled` =1) OR (`user`.`card`=:data AND `user`.`card_enabled`=1)) AND `user`.`status` = 1',
            { replacements: { room: vdata.room, data: vdata.bio }, model: models.user, mapToModel: true, type: QueryTypes.SELECT }
        )
        .then(result => {
            if (result.length > 0) {

                socket.emit(event, JSON.stringify({'node': vdata.node , 'msg': 'T:' + vdata.type + '#C:' + (vdata.cmd == 9 ? 10 : 12) + '#D:' + result[0].firstname + '#S:1#' }))
            }
            else {
                socket.emit(event, JSON.stringify({'node': vdata.node , 'msg': 'T:' + vdata.type + '#C:' + (vdata.cmd == 9 ? 10 : 12) + '#D:INVALID#S:0#'  }))
            }
        })
        .catch(error => {
            socket.emit(event, JSON.stringify({'node': vdata.node , 'msg': 'T:' + vdata.type + '#C:' + (vdata.cmd == 9 ? 10 : 12) + '#D:INVALID#S:0#' }))
        });

}