module.exports = (event, io, socket, payload) => {
    const { QueryTypes } = require('sequelize')
    var models = require('../data/models/index');
    var _data = payload;

    //save network structure data to database.
    models.sequelize
    .query('CALL `sp_network`(:room, :network)',
     { replacements: {room: _data.room, network: _data.network }, type: QueryTypes.UPDATE } )
    .then(result => { socket.broadcast.to(_data.room).emit(event, JSON.stringify({ 'msg': _data.network })); }) 

}