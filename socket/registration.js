module.exports = (event, io, socket, payload) => {
    const { QueryTypes } = require('sequelize')
    var models = require('../data/models/index');
    var vdata = payload;



    /*
    Defination
     C => command type                       N => node id                    V => voltage                            X => memory
     Y => rom                                F => software version           H => hardware version                   P => processor
     E => temperature                        D => data {sensor data}         T => node type
     M => command direction
    
    Work flow
    user clicks add/update [fingerprint, card, pin] from mobile app
    the app send socket data to server 
    eg { 'room':value, 'address':terget node id,  'data': 'T:node type#C:command type#I:user id#D:usermane [10 characters]#', 'dir': direction }
    the server then forwards the request to the hub  with a callback otherwith notify the user that request has failed
     

    */



    if (vdata.scr == 'mob') { //mobile application request
        models.sequelize
            .query('SELECT `account_hash` FROM `account_settings` WHERE `account_code`=:room',
                { replacements: { room: vdata.room }, model: models.account_settings, mapToModel: true, type: QueryTypes.SELECT }
            )
            .then(result => {
                if (result.length > 0) {

                    socket.broadcast.to(result[0].account_hash).emit(event, JSON.stringify(
                        { 'node': vdata.node, 'msg': vdata.data }
                    ));
                }
                else {
                    socket.emit(event, JSON.stringify({ 'node': vdata.node, 'status': 0, 'msg': 'Process Failed' }))
                }
            })
            .catch(error => {
                socket.emit(event, JSON.stringify({ 'node': vdata.node, 'status': 0, 'msg': 'Process Error' }))
            });

    }
    else if (vdata.scr == 'hub') {

        if (vdata.status == 1) {//ACTION SUCCESSFUL
          
            if (vdata.cmd == 6) {//REGISTER_FINGER_RESPONSE
                models.sequelize.query(
                    'UPDATE `user` SET `finger_id` =:id, `finger_captured` = 1, `finger_added_date` = CURRENT_TIMESTAMP,`finger_updated_date` = CURRENT_TIMESTAMP WHERE `user_id` = :user_id',
                    { replacements: { id: vdata.bioinfo, user_id: vdata.userid  }, type: QueryTypes.UPDATE });
            }
            else if (vdata.cmd == 8) {//REGISTER_CARD_RESPONSE
                models.sequelize.query(
                    'UPDATE `user` SET `card`=:pan, `card_captured`=1, `card_added_date` =CURRENT_TIMESTAMP, `card_updated_date` = CURRENT_TIMESTAMP  WHERE `user_id` = :user_id',
                    { replacements: { pan: vdata.bioinfo, user_id: vdata.userid }, type: QueryTypes.UPDATE });
            }
             
            socket.broadcast.to(vdata.room).emit(event, JSON.stringify({ 'node': vdata.node,  'status': 1, 'msg': 'Process Ok' }))
        }
        else {//ACTION FAILED
            socket.broadcast.to(vdata.room).emit(event, JSON.stringify({ 'node': vdata.node, 'status': 0, 'msg': 'Process Failed' }))
        }
    }


}