module.exports = (event, io, socket, payload) => {
    const { QueryTypes } = require('sequelize')
    var models = require('../data/models/index');
    var data = payload;


    /*
     T => device type
     M => direction **
     C => command type
     N => node id / mac address
     D => data
     K => device address
     I => user id     
     S => user access status **
 
      {"address": 'mac address', "command": 'T:device type#:M:direction#C:command type#'}  
    */
    models.sequelize
        .query('SELECT `account_hash` FROM `account_settings` WHERE `account_code`=:room',
            { replacements: { room: data.room }, nmodel: models.account_settings, nmapToModel: true, type: QueryTypes.SELECT }
        )
        .then(result => {
            if (result.length > 0) {
                socket.broadcast.to(result[0].account_hash).emit(event, JSON.stringify(
                    { 'node': data.node, 'msg': data.cmd }
                ));
            }
            else {
                socket.emit(event, JSON.stringify({ 'node': data.node, 'msg': 'Failed' }))
            }
        })
        .catch(error => {
            socket.emit(event, JSON.stringify({ 'node': data.node, 'msg': 'Error' }))
        });

}