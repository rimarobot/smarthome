module.exports = (event, io, socket, payload) => {


    const { QueryTypes } = require('sequelize')
    var models = require('../data/models/index');

    /*
      payload struct { "room":room, "address":address, "username":username }
    */
    var data = payload; 
    console.log(payload); 
    //socket.emit(event, JSON.stringify({ 'username': data.username, 'status': 1, 'msg': 'Error' }))
    var old = socket.room;

    /*
     Make sure to logout other rooms before joining a new one.
    */
    if (old != null) {
        socket.leave(old);
        models.sequelize.query(
            'UPDATE `account_settings` SET `account_hash`=\'\'  WHERE `account_code`=:room AND `root_address`=:address ',
            { replacements: { room: data.room, address: data.address }, type: QueryTypes.UPDATE, raw: true });
    }


    //check if node is registered.
    models.sequelize
        .query(
            'SELECT `account`.`account_id`, `account`.`surname` from `account` INNER JOIN `account_settings` ON `account`.`account_id`= `account_settings`.`account_id`  WHERE `account_settings`.`account_code` = :room',
            {
                replacements: { room: data.room },
                model: models.account,
                mapToModel: true,
                type: QueryTypes.SELECT
            })
        .then(result => { 
          
            if (result.length > 0) {
                socket.room = data.room;
                socket.username = data.username;
                socket.join(data.room);
                socket.broadcast.to(data.room).emit(event, JSON.stringify({ 'msg': data.username + ' has joined' }));
                socket.emit(event, JSON.stringify({ 'username': data.username, 'status': 1, 'msg': 'Successful' }))
                
                if(data.username == 'ESP8266' || data.username == 'ESP-01' || data.username == 'ESP32'){
                models.sequelize.query(
                    'UPDATE `account_settings` SET `account_hash`=:socket_id ,`root_address`=:address WHERE `account_code`=:room',
                    { replacements: { socket_id: socket.id, address: data.address, room: data.room }, type: QueryTypes.UPDATE, raw: true });
                }
                
            }
            else {
                socket.emit(event, JSON.stringify({ 'username': data.username, 'status': 0, 'msg': 'Invalid' }))
            }
        })
        .catch(error => {
            socket.emit(event, JSON.stringify({ 'username': data.username, 'status': 0, 'msg': 'Error' }))
        });

}