module.exports = (event, io, socket, payload) => {
    const { QueryTypes } = require('sequelize')
    var models = require('../data/models/index');
    var data = payload;


     
    /*
     
     Defination
     C => command type                       N => node id                    V => voltage                            X => memory
     Y => rom                                F => software version           H => hardware version                   P => processor
     E => temperature                        D => data {sensor data}         T => node type
     
     Communication
     1. To hub from slave nodes
      T:''#C:''#N:''#V:''#X:''#Y:''#F:''#H:''#P:''#E:''#D:''#
     2. To server from hub
      {   room:'room', T:'node type',
          N:'node id', V:'voltage', X:'memory', 
          Y:'rom', F:'software version', H:'hardware version',
          P:'processor', E:'temperature', D:'data'
      }  

    */ 
    models.sequelize
        .query('CALL `sp_status`(:room, :nodeid, :voltage, :ram, :rom, :sversion, :hversion, :processor, :temperature, :sdata, :ntype)',
            {
                replacements: {
                    room: data.room, nodeid: data.N, voltage: data.V, ram: data.X, rom: data.Y, sversion: data.F,
                    hversion: data.H, processor: data.P, temperature: data.E, sdata: data.D, ntype: data.T
                }, type: QueryTypes.UPDATE
            }
        )
        .then(result => { socket.broadcast.to(data.room).emit(event, JSON.stringify(data)); });
    // .catch(error => {
    //     socket.emit(event, JSON.stringify({ 'node': data.address, 'status': 0, 'msg': 'Error' }))
    // });

}