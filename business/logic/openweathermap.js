const request = require('request');
var getWeather = (location, callback) => {
    request(
        {
            url: 'https://api.openweathermap.org/data/2.5/weather?q=' + location + '&appid=1a31b0f3c0df5a5ec975a072253f4f07',
            json: true
        }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                callback('success', body);
            }
            else {
                callback('error', null);
            }
        });
};
module.exports.getWeather = getWeather;