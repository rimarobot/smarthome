package com.example.smarthome.ui.home;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.smarthome.R;
import com.example.smarthome.ui.home.devices.DeviceListFragment;
import com.example.smarthome.ui.home.rooms.RoomsListFragment;
import com.example.smarthome.ui.home.routines.RoutinesListFragment;
import com.example.smarthome.ui.home.sceens.ScenesListFragment;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_home_rooms, R.string.tab_home_scenes, R.string.tab_home_routines, R.string.tab_home_devices};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 1:
                return ScenesListFragment.newInstance(2);
            case 2:
                return RoutinesListFragment.newInstance(1);
            case 3:
                return DeviceListFragment.newInstance(1);
            default:
                return RoomsListFragment.newInstance(2);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }


    @Override
    public int getCount() {
        return 4;
    }
}