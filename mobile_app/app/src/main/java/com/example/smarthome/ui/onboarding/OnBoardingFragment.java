package com.example.smarthome.ui.onboarding;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.smarthome.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnBoardingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnBoardingFragment extends Fragment {

    SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    Button mSkipBtn, mNextBtn, mFinishBtn;
    ImageView zero, one, two;
    ImageView[] indicators;
    int lastLeftValue = 0;
    int page = 0;   //  to track page position

    public OnBoardingFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static OnBoardingFragment newInstance() {
        return new OnBoardingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_on_boarding, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), this.getChildFragmentManager());
        mViewPager = root.findViewById(R.id.view_pager_onboarding);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mNextBtn = (Button) root.findViewById(R.id.intro_btn_next);
        mSkipBtn = (Button) root.findViewById(R.id.intro_btn_skip);
        mFinishBtn = (Button) root.findViewById(R.id.intro_btn_finish);
        zero = (ImageView) root.findViewById(R.id.intro_indicator_0);
        one = (ImageView) root.findViewById(R.id.intro_indicator_1);
        two = (ImageView) root.findViewById(R.id.intro_indicator_2);
        indicators = new ImageView[]{zero, one, two};

        mViewPager.setCurrentItem(page);
        updateIndicators(page);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                page = position;

                updateIndicators(page);
                mNextBtn.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
                mFinishBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                mViewPager.setCurrentItem(page, true);
            }
        });

        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                page = 0;
                //mViewPager.setCurrentItem(page, true);
                Navigation.findNavController(v).navigate(R.id.action_onBoardingFragment_to_loginFragment);
            }
        });

        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_onBoardingFragment_to_loginFragment);
            }
        });

        return root;
    }


    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }
}
