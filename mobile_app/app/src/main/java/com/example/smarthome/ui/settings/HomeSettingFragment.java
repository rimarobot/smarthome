package com.example.smarthome.ui.settings;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentHomeSettingBinding;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class HomeSettingFragment extends Fragment {

    private HomeViewModel model;
    private com.example.smarthome.models.account account;
    private FragmentHomeSettingBinding binding;
    private ApiInterface api;
    private Disposable disposable;
    private boolean isSuccessful = false;

    public HomeSettingFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HomeSettingFragment newInstance() {
        return new HomeSettingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getAccoutDetails().observe(this, data -> {
            account = data;
            loadUI();
        });
    }

    void loadUI(){
        if(account != null) {
            binding.settingFullname.setText(account.surname + " " + account.firstname);
            binding.settingEmail.setText(account.email);
            binding.settingPhone.setText(account.phone);
            binding.settingAddress.setText(account.address);
            binding.settingGatewaySsid.setText(account.account_setting.gateway_ssid);
            binding.settingGatewayPassword.setText(account.account_setting.gateway_password);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        View root = inflater.inflate(R.layout.fragment_home_setting, container, false);
        binding = FragmentHomeSettingBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_save:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void save() {
        String Errors = "";
        if((TextUtils.isEmpty(binding.settingFullname.getText().toString()))){
            Errors = "name required";
            binding.settingFullname.setError(Errors);
            return;
        }
        if((TextUtils.isEmpty(binding.settingEmail.getText().toString()))){
            Errors = "email required";
            binding.settingEmail.setError(Errors);
            return;
        }
        if((TextUtils.isEmpty(binding.settingPhone.getText().toString()))){
            Errors = "phone required";
            binding.settingPhone.setError(Errors);
            return;
        }

        if((TextUtils.isEmpty(binding.settingAddress.getText().toString()))){
            Errors = "address required";
            binding.settingAddress.setError(Errors);
            return;
        }
        if((TextUtils.isEmpty(binding.settingGatewaySsid.getText().toString()))){
            Errors = "gateway ssid required";
            binding.settingGatewaySsid.setError(Errors);
            return;
        }
        if((TextUtils.isEmpty(binding.settingGatewayPassword.getText().toString()))){
            Errors = "gateway password required";
            binding.settingGatewayPassword.setError(Errors);
            return;
        }

        String[] name = binding.settingFullname.getText().toString().split(" ");
        String surname = "",firstname ="";
        if (name.length >0)
            surname = name[0];
        if(name.length >1)
            firstname = name[1];
         account.surname =  surname;
         account.firstname = firstname;
        account.email =  binding.settingEmail.getText().toString();
        account.phone = binding.settingPhone.getText().toString();
        account.address =  binding.settingAddress.getText().toString();

        account.account_setting.gateway_ssid =  binding.settingGatewaySsid.getText().toString();
        account.account_setting.gateway_password =  binding.settingGatewayPassword.getText().toString();
        model.setAccoutDetails(account);
        updateAccount();

    }

    void updateAccount(){

        api.account_put(account.account_id, account)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._account>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._account _account) {
                       if(_account.success){
                           updateSettings();
                       }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void updateSettings(){

        api.account_settings_put(account.account_setting.setting_id, account.account_setting)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._account_settings>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._account_settings _settings) {
                        if(_settings.success)
                         Navigation.findNavController(HomeSettingFragment.this.getView()).popBackStack();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
