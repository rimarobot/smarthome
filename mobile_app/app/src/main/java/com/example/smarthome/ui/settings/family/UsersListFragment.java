package com.example.smarthome.ui.settings.family;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.databinding.FragmentUsersListListBinding;
import com.example.smarthome.models.user;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.disposables.Disposable;


public class UsersListFragment extends Fragment implements UsersRecyclerViewAdapter.OnUserInteractionListener {

    // TODO: Customize parameters
    private int mColumnCount = 2;
    public  boolean level3 = false;
    private HomeViewModel model;
    private com.example.smarthome.models.user user = new com.example.smarthome.models.user();
    private FragmentUsersListListBinding binding;
    private ApiInterface api;
    private Disposable disposable;
    private List<user> list = new ArrayList<>();

    public UsersListFragment() {
    }

    public static UsersListFragment newInstance() {
        return new UsersListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getSelectedUserList().observe(this, data ->{
            list.clear();
            if(data != null) {
                list.addAll(data);
            }
            binding.list.getAdapter().notifyDataSetChanged();
            binding.list.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    binding.list.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.user_image).performClick();
                    binding.list.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                }
            });
            loadUI();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View root = inflater.inflate(R.layout.fragment_users_list_list, container, false);
        binding = FragmentUsersListListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        if (mColumnCount <= 1) {
            binding.list.setLayoutManager(new LinearLayoutManager(this.getContext()));
        } else {
            binding.list.setLayoutManager(new GridLayoutManager(this.getContext(), mColumnCount));
        }
        binding.list.setAdapter(new UsersRecyclerViewAdapter(list,this.getContext(), this));
        binding.selectedUserEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(UsersListFragment.this.getView()).navigate(R.id.action_usersListFragment_to_userDetailsFragment);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadUI();
    }



    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        loaduserUi();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(!level3) {
            ((BottomNavigationView) getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
        }
    }
    void loadUI(){
        if(list != null) {
//            binding.selectedUserName.setVisibility(View.GONE);
//            binding.selectedUserImage.setVisibility(View.GONE);
            int count = 0;
            try{ count =   list.size(); }catch (Exception e){ }
            binding.userCount.setText( count + " Members ");
        }
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_add:
                level3 = true;
                model.setSelectedUser(new user());
                Navigation.findNavController(UsersListFragment.this.getView()).navigate(R.id.action_usersListFragment_to_userDetailsFragment);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onUserStatusClicked(user item) {
        user = item;
    }

    @Override
    public void onUserDetailsClicked(user item) {
            model.setSelectedUser(item);
            user = item;
            loaduserUi();
    }

    void loaduserUi(){
        try {
            binding.selectedUserName.setText(user.surname.toUpperCase() + " " + user.firstname.toUpperCase());
            //binding.selectedUserImage.
            model.setSelectedUser(user);
        }catch (Exception e){}
    }
}
