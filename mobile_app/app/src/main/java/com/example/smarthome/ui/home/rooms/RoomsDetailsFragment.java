package com.example.smarthome.ui.home.rooms;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentRoomDetailsBinding;
import com.example.smarthome.models.*;
import com.example.smarthome.ui.home.HomeViewModel;
import com.example.smarthome.ui.home.rooms.device.AccessControlView;
import com.example.smarthome.ui.home.rooms.device.GarageView;
import com.example.smarthome.ui.home.rooms.device.LightSensorView;
import com.example.smarthome.ui.home.rooms.device.MotionSwitchView;
import com.example.smarthome.ui.home.rooms.device.MotionSensorView;
import com.example.smarthome.ui.home.rooms.device.PowerSwitchView;
import com.example.smarthome.ui.home.rooms.device.RoomDevicesRecyclerViewAdapter;
import com.example.smarthome.ui.home.rooms.device.ThermostatView;
import com.example.smarthome.ui.home.rooms.device.WeatherSensorView;
import com.example.smarthome.ui.home.rooms.device.ServoMotorView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class RoomsDetailsFragment extends Fragment implements RoomDevicesRecyclerViewAdapter.OnRoomDeviceInteractionListener {

    private com.example.smarthome.models.room room;
    private List<room_device> list = new ArrayList<>();
    private HomeViewModel model;
    private FragmentRoomDetailsBinding binding;
    public RoomsDetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RoomsDetailsFragment newInstance() {
        return new RoomsDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getRoomSelected().observe(this, data ->{
            room = data;
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(data.name);
            list.clear();
            if(data.room_devices != null) {
                list.addAll(data.room_devices);
            }
            binding.roomDeviceList.getAdapter().notifyDataSetChanged();
            binding.roomDeviceList.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    binding.roomDeviceList.findViewHolderForAdapterPosition(0).itemView.performClick();
                    binding.roomDeviceList.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                }
            });
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View root = inflater.inflate(R.layout.fragment_room_details, container, false);
        binding = FragmentRoomDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        binding.roomDeviceList.setLayoutManager(new LinearLayoutManager(this.getActivity(),
                LinearLayoutManager.HORIZONTAL,false));
        binding.roomDeviceList.setHasFixedSize(true);
        binding.roomDeviceList.setAdapter(new RoomDevicesRecyclerViewAdapter(list,this.getContext(), this));

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }

//    @Override
//    public void onPause() {
//        super.onPause();
//        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
//    }


    @Override
    public void onRoomDeviceClicked(room_device item) {
        model.setDeviceSelected(item.device);
        Fragment f = null;
        switch (item.device.device_type.name){
            case "Power Switch":
                f =  PowerSwitchView.newInstance();
                break;
            case "Weather Sensor":
                f =  WeatherSensorView.newInstance();
                break;
            case "Motion Sensor":
                f =  MotionSensorView.newInstance();
                break;
            case "Motion Switch":
                f =  MotionSwitchView.newInstance();
                break;
            case "Light Sensor":
                f =  LightSensorView.newInstance();
                break;
            case "Servo Motor":
                f =  ServoMotorView.newInstance();
                break;
            case "Fan Control":
                f =  ThermostatView.newInstance();
                break;
            case "Access Control":
                f =  AccessControlView.newInstance();
                break;
            default:
                f =  GarageView.newInstance();
                break;
        }
        FragmentTransaction transaction = this.getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.room_device_detail, f);
        transaction.disallowAddToBackStack();
        transaction.commit();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_edit:
                Navigation.findNavController(RoomsDetailsFragment.this.getView()).navigate(R.id.action_roomsDetailsFragment_to_roomsAddFragment);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
