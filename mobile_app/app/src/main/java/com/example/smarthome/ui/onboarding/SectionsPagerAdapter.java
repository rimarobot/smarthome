package com.example.smarthome.ui.onboarding;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;



/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 1:
                return OnBoardingSecondFragment.newInstance();
            case 2:
                return OnBoardingThirdFragment.newInstance();
            default:
                return OnBoardingFirstFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}