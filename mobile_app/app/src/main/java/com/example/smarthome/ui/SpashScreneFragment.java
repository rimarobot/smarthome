package com.example.smarthome.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.helpers.Account;
import com.example.smarthome.helpers.Config;
import com.example.smarthome.ui.home.HomeViewModel;
import com.example.smarthome.ui.onboarding.OnBoardingFragmentDirections;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpashScreneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpashScreneFragment extends Fragment {

    private HomeViewModel model;
    private ApiInterface api;
    private Disposable disposable;
    private Account pref;
    public SpashScreneFragment() {
        // Required empty public constructor
    }

    public static SpashScreneFragment newInstance() {
        return new SpashScreneFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_spash_screne, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Config.account = new Account(this.getContext()).getAccount();
        if(Config.account.account_id == 0  || TextUtils.isEmpty(Config.account.account_setting.account_code)){
            //go to login here
            Navigation.findNavController(this.getView()).navigate(R.id.action_spashScreneFragment_to_onBoardingFragment);
//            Navigation.findNavController(this.getView()).navigate(SpashScreneFragmentDirection);
        }
        else{
            //go to home screen
            loadData();
            Navigation.findNavController(this.getView()).navigate(R.id.action_spashScreneFragment_to_navigation_home);
        }
    }


    private void loadData(){
        //update account id
        int account_id = Config.account.account_id;

        api.room_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._roomlist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._roomlist roomlist) {
                        if(roomlist.success)
                            model.setRoomList(roomlist.data);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

        api.scene_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._scenelist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._scenelist scenelist) {
                        if(scenelist.success)
                            model.setSceneList(scenelist.data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        api.control_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._controllist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._controllist controllist) {
                        if(controllist.success)
                            model.setRoutineList(controllist.data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        api.node_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._nodelist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._nodelist nodelist) {
                        if(nodelist.success)
                            model.setNodeList(nodelist.data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });


        api.account_get(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._account>(){
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._account account) {
                        if(account.success == true) {

                            model.setAccoutDetails(account.data);
                            if(account.data.account_setting != null)
                                model.setSelectedSetting(account.data.account_setting);
                            if(account.data.network != null)
                                model.setSelectedNetwork(account.data.network);
                            if(account.data.users != null)
                                model.setSelectedUserList(account.data.users);


                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
