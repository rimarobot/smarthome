package com.example.smarthome.ui.home.routines;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.models.control;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.List;


public class RoutinesRecyclerViewAdapter extends RecyclerView.Adapter<RoutinesRecyclerViewAdapter.ViewHolder> {

    private final List<control> mValues;
    private final OnRoutineInteractionListener mListener;
    private final Context context;


    public RoutinesRecyclerViewAdapter(List<control> items, Context cont, OnRoutineInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = cont;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_routine_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        int count = 0;
        try{
            count =   mValues.get(position).control_devices.size();
        }catch (Exception e){
        }
        holder.mDevicesView.setText("x"+String.valueOf(count)+ " Devices" );
        holder.mTittleView.setText(mValues.get(position).name);
        holder.mTimeView.setText(mValues.get(position).startTime + (TextUtils.isEmpty(mValues.get(position).endTime) ? "" : " to " + mValues.get(position).endTime));
        String frequency[] = mValues.get(position).frequency.split("\\|");
        String dates =  frequency[1];
        String[] test = dates.split(",");
        if(test.length >= 7)
            holder.mDateView.setText("Every Day");
        else
            holder.mDateView.setText(dates);

        holder.mStatusView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (null != mListener) {
                    mListener.onStatusClicked(holder.mItem, isChecked);
                }
            }
        });

        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onDetailsClicked(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDevicesView, mTittleView, mTimeView,mDateView;
        public final SwitchMaterial mStatusView;
        public final ImageView mImageView;
        public control mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDevicesView = (TextView) view.findViewById(R.id.devices);
            mTittleView = (TextView) view.findViewById(R.id.title);
            mTimeView = (TextView) view.findViewById(R.id.txt_time);
            mDateView = (TextView) view.findViewById(R.id.txt_date_range);
            mStatusView = (SwitchMaterial) view.findViewById(R.id.status);
            mImageView = (ImageView) view.findViewById(R.id.details);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTittleView.getText() + "'";
        }
    }

    public interface OnRoutineInteractionListener {
        // TODO: Update argument type and name
        void onStatusClicked(control item, boolean isChecked);
        void onDetailsClicked(control item);

    }
}
