package com.example.smarthome.models;

public class user
{
	public int user_id;
	public int account_id;
	public account account;
	public String surname;
	public String firstname;
	public String email;
	public String phone;
	public String username;
	public String password;
	public String pin;
	public String card;
	public int finger_id;
	public int card_captured;
	public int pin_captured;
	public int finger_captured;
	public String card_added_date;
	public String card_updated_date;
	public String finger_added_date;
	public String finger_updated_date;
	public String pin_added_date;
	public String pin_updated_date;
	public int card_enabled;
	public int pin_enabled;
	public int finger_enabled;
	public int status;
	public int profile_id;
	public String createdAt;
	//public String updatedAt;

}
