package com.example.smarthome.models;

import java.util.List;

public class scene
{
	public int scene_id;
	public int account_id;
	public account account;
	public String name;
	public int status;
	public String createdAt;
	//public String updatedAt;
	public List<scene_device> scene_devices;

	public scene() {
	}

	public scene(int scene_id, int account_id, com.example.smarthome.models.account account, String name, String createdAt, String updatedAt) {
		this.scene_id = scene_id;
		this.account_id = account_id;
		this.account = account;
		this.name = name;
		this.createdAt = createdAt;
		//this.updatedAt = updatedAt;
	}

}
