package com.example.smarthome.ui.access.registration;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.databinding.FragmentRegistrationSecondBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationSecondFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationSecondFragment extends Fragment implements Step {
    private FragmentRegistrationSecondBinding binding;
    private RegistrationViewModel model;
    private com.example.smarthome.models.account account;
    String Errors = "";
    public RegistrationSecondFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RegistrationSecondFragment newInstance( ) {
        return new RegistrationSecondFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this.getActivity()).get(RegistrationViewModel.class);
        model.getAccount().observe(this, account1 -> {
            account = account1;
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_registration_second, container, false);
        binding = FragmentRegistrationSecondBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {

        if((TextUtils.isEmpty(binding.username.getText().toString()))){
            Errors = "username required";
            binding.username.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.password.getText().toString()))){
            Errors = "password required";
            binding.password.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.confirmPassword.getText().toString()))){
            Errors = "confirm password";
            binding.confirmPassword.setError(Errors);
            return new VerificationError(Errors);
        }
        if(!binding.password.getText().toString().contentEquals(binding.confirmPassword.getText().toString())){
            Errors = "confirm password";
            binding.confirmPassword.setError(Errors);
            return new VerificationError(Errors);
        }

        account.username = binding.username.getText().toString();
        account.password = binding.password.getText().toString();
        model.setAccount(account);

        return new VerificationError(Errors);
    }
}
