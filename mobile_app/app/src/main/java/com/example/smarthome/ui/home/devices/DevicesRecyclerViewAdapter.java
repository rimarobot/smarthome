package com.example.smarthome.ui.home.devices;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.models.device;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;


public class DevicesRecyclerViewAdapter extends RecyclerView.Adapter<DevicesRecyclerViewAdapter.ViewHolder> {

    private final List<device> mValues;
    private final OnDeviceInteractionListener mListener;
    private final Context context;

    public DevicesRecyclerViewAdapter(List<device> items, Context cont, OnDeviceInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = cont;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_device_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mStatusView.setText(mValues.get(position).status);
        holder.mTitleView.setText(mValues.get(position).name);
//        int resID = context.getResources().getIdentifier("@drawable/"+ mValues.get(position).image , "drawable", context.getPackageName());
//        holder.mImageView.setImageResource(resID);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onDeviceClicked(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mStatusView;
        public final TextView mTitleView;
        public final ImageView mImageView;
        public device mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mStatusView = (TextView) view.findViewById(R.id.status);
            mTitleView = (TextView) view.findViewById(R.id.title);
            mImageView = (ImageView) view.findViewById(R.id.image);
        }

    }

    public interface OnDeviceInteractionListener {
        // TODO: Update argument type and name
        void onDeviceClicked(device item);
    }
}
