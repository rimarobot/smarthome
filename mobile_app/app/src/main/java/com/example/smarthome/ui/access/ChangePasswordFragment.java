package com.example.smarthome.ui.access;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentChangePasswordBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends Fragment {

    FragmentChangePasswordBinding binding;
    private ApiInterface api;
    private Disposable disposable;
    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance(String param1, String param2) {
       return new ChangePasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        //View root = inflater.inflate(R.layout.fragment_change_password, container, false);
        binding = FragmentChangePasswordBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_save:

                if((TextUtils.isEmpty(binding.password.getText().toString()))){
                    binding.password.setError("*");
                    return false;
                }
                if((TextUtils.isEmpty(binding.confirmPassword.getText().toString()))){
                    binding.confirmPassword.setError("*");
                    return false;
                }
                if(binding.confirmPassword.getText().toString().compareTo(binding.password.getText().toString()) != 0 ){
                    binding.confirmPassword.setError("Not matching");
                    return false;
                }

                api.reset(binding.password.getText().toString(), 1)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe( new Observer<response._account>(){

                            @Override
                            public void onSubscribe(Disposable d) {
                                disposable = d;
                            }

                            @Override
                            public void onNext(response._account account) {
                                if(account.success == true){
                                    Navigation.findNavController(ChangePasswordFragment.this.getView()).navigate(R.id.action_changePasswordFragment_to_navigation_settings);
                                }
                                else{
                                    binding.password.setError(account.error);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
