package com.example.smarthome.ui.access;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.request;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentLoginBinding;
import com.example.smarthome.helpers.Account;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {

    private FragmentLoginBinding binding;
    private HomeViewModel  model;
    private ApiInterface api;
    private Disposable disposable;
    private Account pref;

    public LoginFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        pref = new Account(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //hide nav bar and app bar
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        // Inflate the layout for this fragment
        //View root = inflater.inflate(R.layout.fragment_login, container, false);
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_registerFragment);
            }
        });

        binding.btnLostPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_lostPasswordFragment);
            }
        });

        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((TextUtils.isEmpty(binding.txtUsername.getText().toString()))){
                    binding.txtUsername.setError("*");
                    return;
                }
                if((TextUtils.isEmpty(binding.txtPassword.getText().toString()))){
                    binding.txtPassword.setError("*");
                    return;
                }
                login(v);


            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Navigation.findNavController(view).popBackStack(R.id.loginFragment, false);
    }

    private void loadData(){
        //update account id
        int account_id = pref.getAccount().account_id;

        api.room_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._roomlist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._roomlist roomlist) {
                        if(roomlist.success)
                            model.setRoomList(roomlist.data);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

        api.scene_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._scenelist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._scenelist scenelist) {
                        if(scenelist.success)
                            model.setSceneList(scenelist.data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        api.control_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._controllist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._controllist controllist) {
                        if(controllist.success)
                            model.setRoutineList(controllist.data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        api.node_get_list(account_id)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe( new Observer<response._nodelist>(){

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(response._nodelist nodelist) {
                        if(nodelist.success)
                            model.setNodeList(nodelist.data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    void login(View v){
        // TRY USERS
        api.login_user(new request(binding.txtUsername.getText().toString(), binding.txtPassword.getText().toString()))
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._user>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._user user) {
                        if(user.success == true) {

                            api.account_get(user.data.account_id)
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe( new Observer<response._account>(){
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                            disposable = d;
                                        }

                                        @Override
                                        public void onNext(response._account account) {
                                            if(account.success == true) {

                                                model.setAccoutDetails(account.data);
                                                if(account.data.account_setting != null)
                                                    model.setSelectedSetting(account.data.account_setting);
                                                if(account.data.network != null)
                                                    model.setSelectedNetwork(account.data.network);
                                                if(account.data.users != null)
                                                    model.setSelectedUserList(account.data.users);
                                                pref.Save(account.data);

                                                loadData();
                                                Navigation.findNavController(v).navigate(R.id.navigation_home);
                                            }
                                            else
                                                binding.txtPassword.setError(account.error);
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            binding.txtPassword.setError(e.getMessage());
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });

                        }
                        else {
                            api.login_account(new request(binding.txtUsername.getText().toString(), binding.txtPassword.getText().toString()))
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe( new Observer<response._account>(){
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                            disposable = d;
                                        }

                                        @Override
                                        public void onNext(response._account account) {
                                            if(account.success == true) {

                                                model.setAccoutDetails(account.data);
                                                if(account.data.account_setting != null)
                                                    model.setSelectedSetting(account.data.account_setting);
                                                if(account.data.network != null)
                                                    model.setSelectedNetwork(account.data.network);
                                                if(account.data.users != null)
                                                    model.setSelectedUserList(account.data.users);
                                                pref.Save(account.data);

                                                loadData();
                                                Navigation.findNavController(v).navigate(R.id.navigation_home);
                                            }
                                            else
                                                binding.txtPassword.setError(account.error);
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            binding.txtPassword.setError(e.getMessage());
                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                            }
                    }

                    @Override
                    public void onError(Throwable e) {
                        binding.txtPassword.setError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
