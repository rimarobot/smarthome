package com.example.smarthome.ui.access.registration;

public class VerificationError {

    /**
     * A message explaining the cause of the error.
     */
    private final String mErrorMessage;

    public VerificationError(String errorMessage) {
        this.mErrorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

}
