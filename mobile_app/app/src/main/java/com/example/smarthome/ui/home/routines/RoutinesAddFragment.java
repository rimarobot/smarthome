package com.example.smarthome.ui.home.routines;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentRoutinesAddBinding;
import com.example.smarthome.models.control_device;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public class RoutinesAddFragment extends Fragment {

    private FragmentRoutinesAddBinding binding;
    private com.example.smarthome.models.control control = new com.example.smarthome.models.control();
    private List<control_device> list = new ArrayList<>();
    private HomeViewModel model;
    private static int mColumnCount = 2;
    private ApiInterface api;
    private Disposable disposable;

    public RoutinesAddFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RoutinesAddFragment newInstance() {
        return new RoutinesAddFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getRoutineSelected().observe(this, data ->{
            control = data;
            binding.routineName.setText(control.name);
            binding.routineStartTime.setText(control.startTime);
            binding.routineEndTime.setText(control.endTime);
            if(!TextUtils.isEmpty(control.frequency)) {
                String[] frequency = control.frequency.split("\\|");
                String[] days = frequency[0].split(",");
                for (int i = 0; i < binding.dateRange.getChildCount(); i++) {
                    Chip chip = (Chip) binding.dateRange.getChildAt(i);
                    for(int j = 0; j< days.length; j++) {
                        if (chip.getId() == Integer.parseInt(days[j]))
                            chip.setChecked(true);
                    }
                }
            }

                list.clear();
                 if(data.control_devices != null)
                list.addAll(data.control_devices);
                binding.deviceList.getAdapter().notifyDataSetChanged();

        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.fragment_routines_add, container, false);
        binding = FragmentRoutinesAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        if (mColumnCount <= 1) {
            binding.deviceList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        } else {
            binding.deviceList.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        }
        binding.deviceList.setAdapter(new DeviceAdapter(list, this.getContext(), new OnItemInteractionListener(){

            @Override
            public void onRemoveClicked(control_device item) {

            }
        }));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView status;
        final View mView;
        final ImageView icon;
        control_device item;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.device_list_item_view, parent, false));
            mView = itemView;
            name = (TextView)itemView.findViewById(R.id.device_name);
            status = (TextView)itemView.findViewById(R.id.device_status);
            icon = (ImageView)itemView.findViewById(R.id.device_icon);
        }
    }

    private class DeviceAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<control_device> list;
        private final Context context;
        private final OnItemInteractionListener listener;

        DeviceAdapter(List<control_device> data, Context cxt, OnItemInteractionListener listener1) {
            list = data;
            context = cxt;
            listener = listener1;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.item =  list.get(position);
            holder.name.setText(holder.item.name);
            holder.status.setText(holder.item.device.status);
//            int resID = context.getResources().getIdentifier("@drawable/"+ holder.item.device.image , "drawable", context.getPackageName());
//            holder.icon.setImageResource(resID);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener) {
                        listener.onRemoveClicked(holder.item);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    private interface OnItemInteractionListener {
        void onRemoveClicked(control_device item);
    }

    private void save(){
        String room_icon = "";
        if(TextUtils.isEmpty(binding.routineName.getText().toString())){
            binding.routineName.setError("*");
            return;
        }
        if(TextUtils.isEmpty(binding.routineStartTime.getText().toString())){
            binding.routineStartTime.setError("*");
            return;
        }
        if(TextUtils.isEmpty(binding.routineEndTime.getText().toString())){
            binding.routineEndTime.setError("*");
            return;
        }

        List<Integer> days = binding.dateRange.getCheckedChipIds();
        if(days.size() > 0) {
            room_icon = String.valueOf(days.get(0));
            if(days.size() > 1) {
                for (int i = 1; i < days.size() ; i++)
                    room_icon += "," + String.valueOf(days.get(i));
            }

            room_icon += "|";

            for (int i = 0; i < binding.dateRange.getChildCount(); i++) {
                Chip chip = (Chip) binding.dateRange.getChildAt(i);
                if (chip.isChecked())
                    room_icon += chip.getText().toString() + ",";
            }

            room_icon =  room_icon.substring(0,room_icon.length() -1);
        }

        if(TextUtils.isEmpty(room_icon)){
            binding.routineName.setError("Days not set");
            return;
        }

        control.account_id = 1;
        control.name = binding.routineName.getText().toString();
        control.frequency = room_icon;
        control.startTime = binding.routineStartTime.getText().toString();
        control.endTime = binding.routineEndTime.getText().toString();

        if(control.control_id == 0)
            create_routine();
        else
            update_routine();

        if(control.control_devices != null)
            if(control.control_devices.size()> 0)
                for(control_device device : control.control_devices) {
                    if(device.control_device_id == 0)
                        create_routine_device(device);
                    else
                        update_routine_device(device);
                }
    }

    void create_routine(){
        api.control_post(control)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._control>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._control data) {
                        if(data.success == true)
                            control.control_id = data.data.control_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void update_routine(){
        api.control_put(control.control_id, control)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._control>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._control data) {
                        if(data.success == true)
                            control.control_id = data.data.control_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void create_routine_device(control_device device){

        api.control_device_post(device)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<response._control_device>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._control_device data) {
//                            if (data.success == true)
//                                room.room_id = data.data.room_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void update_routine_device(control_device device){
        api.control_device_put(device.control_device_id, device)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<response._control_device>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._control_device data) {
//                            if (data.success == true)
//                                room.room_id = data.data.room_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
