package com.example.smarthome.ui.settings;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.smarthome.models.node_logs;

import java.util.List;

public class MessagesViewModel extends ViewModel {
    private final MutableLiveData<List<node_logs>> logs = new MutableLiveData<List<node_logs>>();
    public void set(List<node_logs> items) {
        logs.postValue(items);
    }
    public LiveData<List<node_logs>> get() {
        return logs;
    }
    public void add(node_logs value) {
        List<node_logs> items = logs.getValue();
        items.add(0,value);
        logs.postValue(items);
    }
}
