package com.example.smarthome.ui.home.sceens;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentSceneDetailBinding;
import com.example.smarthome.models.scene_device;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ScenesDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScenesDetailsFragment extends Fragment {
    private com.example.smarthome.models.scene scene;
    private List<scene_device> list = new ArrayList<>();
    private HomeViewModel model;
    private FragmentSceneDetailBinding binding;
    private static int mColumnCount = 2;
    public ScenesDetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ScenesDetailsFragment newInstance() {
        return new ScenesDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getSceneSelected().observe(this, data ->{
            scene = data;
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(data.name);
            binding.title.setText(scene.name);
            binding.devices.setText("x0 Devices");
            binding.status.setSelected(scene.status==1);
            list.clear();
            if(data.scene_devices != null) {
                list.addAll(data.scene_devices);
                binding.devices.setText("x"+ String.valueOf(data.scene_devices.size())+" Devices");
            }
            binding.deviceList.getAdapter().notifyDataSetChanged();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_scene_detail, container, false);
        binding = FragmentSceneDetailBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        if (mColumnCount <= 1) {
            binding.deviceList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        } else {
            binding.deviceList.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        }
        binding.deviceList.setAdapter(new DeviceAdapter(list, this.getContext(), new OnItemInteractionListener(){

            @Override
            public void onClicked(scene_device item) {

            }
        }));
        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_edit:
                Navigation.findNavController(this.getView()).navigate(R.id.action_sceensDetailsFragment_to_sceensAddFragment);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView status;
        final View mView;
        final ImageView icon;
        scene_device item;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.device_list_item_view, parent, false));
            mView = itemView;
            name = (TextView)itemView.findViewById(R.id.device_name);
            status = (TextView)itemView.findViewById(R.id.device_status);
            icon = (ImageView)itemView.findViewById(R.id.device_icon);
        }
    }

    private class DeviceAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<scene_device> list;
        private final Context context;
        private final OnItemInteractionListener listener;

        DeviceAdapter(List<scene_device> data, Context cxt, OnItemInteractionListener listener1) {
            list = data;
            context = cxt;
            listener = listener1;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.item =  list.get(position);
            holder.name.setText(holder.item.name);
            holder.status.setText(holder.item.device.status);
//            int resID = context.getResources().getIdentifier("@drawable/"+ holder.item.device.image , "drawable", context.getPackageName());
//            holder.icon.setImageResource(resID);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener) {
                        listener.onClicked(holder.item);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    private interface OnItemInteractionListener {
        void onClicked(scene_device item);
    }
}
