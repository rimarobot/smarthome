package com.example.smarthome.models;

public class node_data
{
	public int node_data_id;
	public int node_id;
	public node node;
	public String data;
	public String createdAt;
	public String updatedAt;

	public node_data() {
	}

	public node_data(int node_data_id, int node_id, com.example.smarthome.models.node node, String data, String createdAt, String updatedAt) {
		this.node_data_id = node_data_id;
		this.node_id = node_id;
		this.node = node;
		this.data = data;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}


}
