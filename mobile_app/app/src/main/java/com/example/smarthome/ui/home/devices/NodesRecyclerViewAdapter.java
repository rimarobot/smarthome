package com.example.smarthome.ui.home.devices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smarthome.R;
import com.example.smarthome.models.device;
import com.example.smarthome.models.node;

import java.util.List;


public class NodesRecyclerViewAdapter extends RecyclerView.Adapter<NodesRecyclerViewAdapter.ViewHolder> {

    private final List<node> mValues;
    private final Context context;
    private DevicesRecyclerViewAdapter deviceAdapter;
    private RecyclerView.RecycledViewPool recycledViewPool;
    private DevicesRecyclerViewAdapter.OnDeviceInteractionListener mListener;

    public NodesRecyclerViewAdapter(List<node> items, Context cont, DevicesRecyclerViewAdapter.OnDeviceInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = cont;
        recycledViewPool = new RecyclerView.RecycledViewPool();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_node_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mStatusView.setText(holder.mItem.status==1?"ON":"OFF");
        holder.mTitleView.setText(holder.mItem.node_type.tittle);
        deviceAdapter = new DevicesRecyclerViewAdapter(holder.mItem.devices, context, mListener);
        holder.mRecyclerView.setAdapter(deviceAdapter);
        holder.mRecyclerView.setRecycledViewPool(recycledViewPool);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mStatusView;
        public final TextView mTitleView;
        private final RecyclerView mRecyclerView;
        public node mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mStatusView = (TextView) view.findViewById(R.id.node_status);
            mTitleView = (TextView) view.findViewById(R.id.node_tittle);
            mRecyclerView = (RecyclerView)view.findViewById(R.id.node_device_list);
            //mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setNestedScrollingEnabled(false);
            mRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }

    }

}
