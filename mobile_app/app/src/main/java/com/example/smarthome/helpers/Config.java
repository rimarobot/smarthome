package com.example.smarthome.helpers;

public class Config
{

    //account data
    public static final String account_id = "account_id";
    public static final String account_name = "name";
    public static final String account_email = "email";
    public static final String account_phone = "phone";
    public static final String account_address = "address";

    //account setting data
    public static final String setting_id = "setting_id";
    public static final String account_code = "account_code";
    public static final String root_address = "root_address";
    public static final String mesh_ssid = "mesh_ssid";
    public static final String mesh_password = "mesh_password";
    public static final String mesh_port = "mesh_port";
    public static final String mesh_channel = "mesh_channel";
    public static final String mesh_hidden = "mesh_hidden";
    public static final String mesh_connections = "mesh_connections";
    public static final String gateway_ssid = "gateway_ssid";
    public static final String gateway_password = "gateway_password";
    public static final String host_address = "host_address";
    public static final String host_port = "host_port";

    public static final int FILTER_DIALOG = 400;

    public static com.example.smarthome.models.account account;
    public static int username;
    public static int profile;
    public  static final String socket_server = "";

}