package com.example.smarthome.ui.home.rooms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.smarthome.R;
import com.example.smarthome.models.room;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;


public class RoomsRecyclerViewAdapter extends RecyclerView.Adapter<RoomsRecyclerViewAdapter.ViewHolder> {

    private final List<room> mValues;
    private final OnRoomInteractionListener mListener;
    private Context context;
    private static View viewOld=null;

    public RoomsRecyclerViewAdapter(List<room> items, Context cont, OnRoomInteractionListener _mListener) {
        mValues = items;
        context = cont;
        mListener = _mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_rooms_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        int count = 0;
        try{
            count =   mValues.get(position).room_devices.size();
        }catch (Exception e){
        }
        holder.mIdView.setText("x" + count +" Devices ");
        holder.mContentView.setText(mValues.get(position).name);
        String[]  image = mValues.get(position).image.split("\\|");
        holder.mImageView.setImageDrawable(context.getResources().getDrawable(getImage(image[1])));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onRoomClicked(holder.mItem);
                    holder.mView.setBackgroundColor(context.getResources().getColor(R.color.grey));
                    if(viewOld!=null)
                        viewOld.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                    viewOld=holder.mView;
                }
            }
        });
    }

    private Integer getImage(String in){
        switch(in){
            case "Kitchen":
                return R.drawable.ic_kitchen;
            case "Bed Room":
                return R.drawable.ic_bedroom;
            case "BathRoom":
                return R.drawable.ic_bathroom;
            case "Office":
                return R.drawable.ic_office;
            case "TV Room":
                return R.drawable.ic_tvroom;
            case "Garage":
                return R.drawable.ic_garage;
            case "Living Room":
                return R.drawable.ic_livingroom;
            case "Toilet":
                return R.drawable.ic_toilet;
            case "Kid Room":
                return R.drawable.ic_kidroom;
            default:
                return R.drawable.ic_bedroom;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ShapeableImageView mImageView;
        public room mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            mImageView = (ShapeableImageView) view.findViewById(R.id.image);

        }

    }

    public interface OnRoomInteractionListener {
        // TODO: Update argument type and name
        void onRoomClicked(room item);
    }
}
