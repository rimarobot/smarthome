package com.example.smarthome.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.smarthome.models.*;

import java.util.List;

public class HomeViewModel extends ViewModel {
    private final MutableLiveData<List<room>> roomList = new MutableLiveData<List<room>>();
    private final MutableLiveData<room> selectedRoom = new MutableLiveData<room>();
    private final MutableLiveData<List<node>> nodeList = new MutableLiveData<List<node>>();
    private final MutableLiveData<device> selectedDevice = new MutableLiveData<device>();
    private final MutableLiveData<List<control>> routineList = new MutableLiveData<List<control>>();
    private final MutableLiveData<control> selectedRoutine = new MutableLiveData<control>();
    private final MutableLiveData<List<scene>> sceneList = new MutableLiveData<List<scene>>();
    private final MutableLiveData<scene> selectedScene = new MutableLiveData<scene>();
    private final MutableLiveData<account> account = new MutableLiveData<account>();
    private final MutableLiveData<account_setting> settings = new MutableLiveData<account_setting>();
    private final MutableLiveData<network> network = new MutableLiveData<network>();
    private final MutableLiveData<List<user>> userList = new MutableLiveData<List<user>>();
    private final MutableLiveData<user> user = new MutableLiveData<user>();

    public void setRoomList(List<room> items) {
        roomList.postValue(items);
    }
    public LiveData<List<room>> getRoomList() {
        return roomList;
    }
    public void setRoomSelected(room value) {
        selectedRoom.setValue(value);
    }
    public LiveData<room> getRoomSelected() {
        return selectedRoom;
    }

    public void setSceneList(List<scene> items) {
        sceneList.postValue(items);
    }
    public LiveData<List<scene>> getSceneList() {
        return sceneList;
    }
    public void setSceneSelected(scene v) {
        selectedScene.setValue(v);
    }
    public LiveData<scene> getSceneSelected() {
        return selectedScene;
    }

    public void setNodeList(List<node> items) {
        nodeList.postValue(items);
    }
    public LiveData<List<node>> getNodeList() {
        return nodeList;
    }
    public void setDeviceSelected(device v) {
        selectedDevice.setValue(v);
    }
    public LiveData<device> getDeviceSelected() {
        return selectedDevice;
    }

    public void setRoutineList(List<control> items) {
        routineList.postValue(items);
    }
    public LiveData<List<control>> getRoutineList() {
        return routineList;
    }
    public void setRoutineSelected(control v) {
        selectedRoutine.setValue(v);
    }
    public LiveData<control> getRoutineSelected() {
        return selectedRoutine;
    }

    public void setAccoutDetails(account items) {
        account.postValue(items);
    }
    public LiveData<account> getAccoutDetails() {
        return account;
    }
    public void setSelectedSetting(account_setting items) {
        settings.postValue(items);
    }
    public LiveData<account_setting> getSelectedSetting() {
        return settings;
    }

    public void setSelectedNetwork(network items) {
        network.postValue(items);
    }
    public LiveData<network> getSelectedNetwork() {
        return network;
    }
    public void setSelectedUserList(List<user> items) {
        userList.postValue(items);
    }
    public LiveData<List<user>> getSelectedUserList() {
        return userList;
    }
    public void setSelectedUser(user items) {
        user.postValue(items);
    }
    public LiveData<user> getSelectedUser() {
        return user;
    }

}
