package com.example.smarthome.ui.home.routines;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentRoutinesListListBinding;
import com.example.smarthome.models.control;
import com.example.smarthome.ui.home.HomeViewModel;

import java.util.ArrayList;
import java.util.List;


public class RoutinesListFragment extends Fragment implements RoutinesRecyclerViewAdapter.OnRoutineInteractionListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private List<control> list = new ArrayList<>();
    private HomeViewModel model;
    private FragmentRoutinesListListBinding binding;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RoutinesListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static RoutinesListFragment newInstance(int columnCount) {
        RoutinesListFragment fragment = new RoutinesListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getRoutineList().observe(this, data -> {
            list.clear();
            list.addAll(data);
            binding.list.getAdapter().notifyDataSetChanged();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.fragment_routines_list_list, container, false);
        binding = FragmentRoutinesListListBinding.inflate(inflater, container, false);
        // Set the adapter
        View view = binding.getRoot();
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new RoutinesRecyclerViewAdapter(list,context, RoutinesListFragment.this));
        }
        return view;
    }

    @Override
    public void onStatusClicked(control item, boolean isChecked) {

    }

    @Override
    public void onDetailsClicked(control item) {
        model.setRoutineSelected(item);
        Navigation.findNavController(this.getView()).navigate(R.id.action_navigation_home_to_routinesDetailsFragment);
    }
}
