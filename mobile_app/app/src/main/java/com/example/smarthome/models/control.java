package com.example.smarthome.models;

import java.util.List;

public class control
{
	public int control_id;
	public int account_id;
	public account account;
	public String name;
	public String startTime;
	public String endTime;
	public String frequency;
	public String createdAt;
	//public String updatedAt;
    public List<control_device> control_devices;

	public control() {
	}

    public control(int control_id, int account_id, com.example.smarthome.models.account account, String name, String startTime, String endTime, String frequency, String createdAt) {
        this.control_id = control_id;
        this.account_id = account_id;
        this.account = account;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.frequency = frequency;
        this.createdAt = createdAt;
        //this.updatedAt = updatedAt;
    }

}
