package com.example.smarthome.models;

public class control_device
{
	public int control_device_id;
	public int control_id;
	public int device_id;
	public device device;
	public String name;
	public String data;
	public String createdAt;
	public String updatedAt;

	public control_device() {
	}

	public control_device(int control_device_id, int control_id, int device_id, com.example.smarthome.models.device device, String name, String data, String createdAt, String updatedAt) {
		this.control_device_id = control_device_id;
		this.control_id = control_id;
		this.device_id = device_id;
		this.device = device;
		this.name = name;
		this.data = data;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}
}
