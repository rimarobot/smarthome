package com.example.smarthome.models;

public class room_device
{
	public int room_device_id;
	public int device_id;
	public device device;
	public int room_id;
	public String name;
	public String createdAt;
	//public String updatedAt;

    public room_device() {
    }

    public room_device(int room_device_id, int device_id, com.example.smarthome.models.device device, int room_id, String name, String createdAt) {
        this.room_device_id = room_device_id;
        this.device_id = device_id;
        this.device = device;
        this.room_id = room_id;
        this.name = name;
        this.createdAt = createdAt;
    }
};
