package com.example.smarthome.models;

import java.util.List;

public class node
{
	public int node_id;
	public int account_id;
	public int node_type_id;
	public node_type node_type;
	public String node_address;
	public int status;
	public String last_active;
	public String voltage;
	public String memory;
	public String rom;
	public String software_version;
	public String hardware_version;
	public String processor;
	public String temperature;
	public String createdAt;
	public String updatedAt;
	public List<device> devices;

}
