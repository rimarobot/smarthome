package com.example.smarthome.ui.access;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentLostPasswordBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LostPasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LostPasswordFragment extends Fragment {

    FragmentLostPasswordBinding binding;
    private ApiInterface api;
    private Disposable disposable;

    public LostPasswordFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static LostPasswordFragment newInstance() {
       return new LostPasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        // Inflate the layout for this fragment
        binding = FragmentLostPasswordBinding.inflate(inflater, container, false);
        //View root = inflater.inflate(R.layout.fragment_lost_password, container, false);
        View root = binding.getRoot();
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((TextUtils.isEmpty(binding.email.getText().toString()))){
                    binding.email.setError("*");
                    return;
                }
                api.reset(binding.email.getText().toString(), 1)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe( new Observer<response._account>(){

                            @Override
                            public void onSubscribe(Disposable d) {
                                disposable = d;
                            }

                            @Override
                            public void onNext(response._account account) {
                              if(account.success == true){
                                  Navigation.findNavController(v).navigate(R.id.action_lostPasswordFragment_to_loginFragment);
                              }
                              else{
                                  binding.email.setError(account.error);
                              }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            }
        });

        return root;
    }
}
