package com.example.smarthome.models;

import java.util.List;

public class room
{
	public int room_id;
	public int account_id;
	public account account;
	public String name;
    public String image;
	public String createdAt;
	//public String updatedAt;
	public List<room_device> room_devices;

    public room() {
    }

    public room(int room_id, int account_id, com.example.smarthome.models.account account, String name, String createdAt, String updatedAt) {
        this.room_id = room_id;
        this.account_id = account_id;
        this.account = account;
        this.name = name;
        this.createdAt = createdAt;
        //this.updatedAt = updatedAt;
    }
}
