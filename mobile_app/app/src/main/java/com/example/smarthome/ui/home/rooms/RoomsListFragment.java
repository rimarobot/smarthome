package com.example.smarthome.ui.home.rooms;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentRoomsListListBinding;
import com.example.smarthome.models.room;
import com.example.smarthome.ui.home.HomeFragmentDirections;
import com.example.smarthome.ui.home.HomeViewModel;

import java.util.ArrayList;
import java.util.List;


public class RoomsListFragment extends Fragment implements RoomsRecyclerViewAdapter.OnRoomInteractionListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private List<room> list = new ArrayList<>();
    private HomeViewModel model;
    private FragmentRoomsListListBinding binding;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RoomsListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static RoomsListFragment newInstance(int columnCount) {
        RoomsListFragment fragment = new RoomsListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getRoomList().observe(this, data ->{
            list.clear();
            list.addAll(data);
            binding.list.getAdapter().notifyDataSetChanged();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.fragment_rooms_list_list, container, false);
        binding = FragmentRoomsListListBinding.inflate(inflater, container, false);
        // Set the adapter
        View view = binding.getRoot();
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new RoomsRecyclerViewAdapter(list, this.getContext(), RoomsListFragment.this));
        }
        return view;
    }


    @Override
    public void onRoomClicked(room item) {
        model.setRoomSelected(item);
        Navigation.findNavController(this.getView()).navigate(R.id.action_navigation_home_to_roomsDetailsFragment);

    }


}
