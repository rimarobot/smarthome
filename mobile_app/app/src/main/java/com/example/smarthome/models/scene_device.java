package com.example.smarthome.models;

public class scene_device
{
	public int scene_device_id;
	public int scene_id;
	public scene scene;
	public int device_id;
	public device device;
	public String name;
	public String data;
	public String createdAt;
	//public String updatedAt;

    public scene_device() {
    }

	public scene_device(int scene_device_id, int scene_id, com.example.smarthome.models.scene scene, int device_id, com.example.smarthome.models.device device, String name, String data, String createdAt, String updatedAt) {
		this.scene_device_id = scene_device_id;
		this.scene_id = scene_id;
		this.scene = scene;
		this.device_id = device_id;
		this.device = device;
		this.name = name;
		this.data = data;
		this.createdAt = createdAt;
		//this.updatedAt = updatedAt;
	}
}
