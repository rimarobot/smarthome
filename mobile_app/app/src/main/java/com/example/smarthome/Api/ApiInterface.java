package com.example.smarthome.api;


import com.example.smarthome.models.*;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.*;


public interface ApiInterface {

    /*
       Methods for interacting with the sales table
     */
    //account
    @POST("account")
    Observable<response._account> account_post(@Body account account);
    @GET("account/{id}")
    Observable<response._account> account_get(@Path("id") int id);
    @PUT("account/{id}")
    Observable<response._account> account_put(@Path("id") int id, @Body account account);
    @DELETE("account/{id}")
    Observable<response._account> account_delete(@Path("id") int id);
    @POST("account/login")
    Observable<response._account> login_account(@Body request request);
    @POST("account/reset-password")
    Observable<response._account> reset(@Body String key, @Body int type);
    @POST("account/change-password")
    Observable<response._account> change(@Body String key, @Body int type);

    //account-settings
    @POST("account-settings")
    Observable<response._account_settings> account_settings_post(@Body account_setting account_settings);
    @GET("account-settings")
    Observable<response._account_settings> account_settings_get(@Path("id") int id);
    @PUT("account-settings/{id}")
    Observable<response._account_settings> account_settings_put(@Path("id") int id, @Body account_setting account_settings);
    @DELETE("account-settings/{id}")
    Observable<response._account_settings> account_settings_delete(@Path("id") int id);

    //command
    @POST("command")
    Observable<response._command> command_post(@Body command command);
    @GET("command")
    Observable<response._command> command_get(@Path("id") int id);
    @PUT("command/{id}")
    Observable<response._command> command_put(@Path("id") int id, @Body command command);
    @DELETE("command/{id}")
    Observable<response._command> command_delete(@Path("id") int id);

    //control
    @POST("control")
    Observable<response._control> control_post(@Body control control);
    @GET("control/{id}")
    Observable<response._control> control_get(@Path("id") int id);
    @GET("control/list/{accountid}")
    Observable<response._controllist> control_get_list(@Path("accountid") int account);
    @PUT("control/{id}")
    Observable<response._control> control_put(@Path("id") int id, @Body control control);
    @DELETE("control/{id}")
    Observable<response._control> control_delete(@Path("id") int id);

    //control-device
    @POST("control-device")
    Observable<response._control_device> control_device_post(@Body control_device control_device);
    @GET("control-device/{id}")
    Observable<response._control_device> control_device_get(@Path("id") int id);
    @GET("control-device/list/{controlid}")
    Observable<response._control_devicelist> control_device_get_list(@Path("controlid") int controlid);
    @PUT("control-device/{id}")
    Observable<response._control_device> control_device_put(@Path("id") int id, @Body control_device control_device);
    @DELETE("control-device/{id}")
    Observable<response._control_device> control_device_delete(@Path("id") int id);

    //device
    @POST("device")
    Observable<response._device> device_post(@Body device device);
    @GET("device/{id}")
    Observable<response._device> device_get(@Path("id") int id);
    @GET("device/list/{accountid}")
    Observable<response._devicelist> device_get_list(@Path("accountid") int accountid);
    @PUT("device/{id}")
    Observable<response._device> device_put(@Path("id") int id, @Body device device);
    @DELETE("device/{id}")
    Observable<response._device> device_delete(@Path("id") int id);

    //device-type
    @POST("device-type")
    Observable<response._device_type> device_type_post(@Body device_type device_type);
    @GET("device-type/{id}")
    Observable<response._device_type> device_type_get(@Path("id") int id);
    @GET("device-type/list")
    Observable<response._device_typelist> device_type_get_list();
    @PUT("device-type/{id}")
    Observable<response._device_type> device_type_put(@Path("id") int id, @Body device_type device_type);
    @DELETE("device-type/{id}")
    Observable<response._device_type> device_type_delete(@Path("id") int id);

    //network
    @POST("network")
    Observable<response._network> network_post(@Body network network);
    @GET("network/{accountid}")
    Observable<response._network> network_get(@Path("accountid") int accountid);
    @PUT("network/{id}")
    Observable<response._network> network_put(@Path("id") int id, @Body network network);
    @DELETE("network/{id}")
    Observable<response._network> network_delete(@Path("id") int id);

    //node
    @POST("node")
    Observable<response._node> node_post(@Body node node);
    @GET("node/{id}")
    Observable<response._node> node_get(@Path("id") int id);
    @GET("node/list/{accountid}")
    Observable<response._nodelist> node_get_list(@Path("accountid") int accountid);
    @PUT("node/{id}")
    Observable<response._node> node_put(@Path("id") int id, @Body node node);
    @DELETE("node/{id}")
    Observable<response._node> node_delete(@Path("id") int id);

    //node-data
    @POST("node-data")
    Observable<response._node_data> node_data_post(@Body node_data node_data);
    @GET("node-data/{id}")
    Observable<response._node_data> node_data_get(@Path("id") int id);
    @GET("node-data/list/{accountid}")
    Observable<response._node_datalist> node_data_get_list(@Path("accountid") int accountid);
    @PUT("node-data/{id}")
    Observable<response._node_data> node_data_put(@Path("id") int id, @Body node_data node_data);
    @DELETE("node-data/{id}")
    Observable<response._node_data> node_data_delete(@Path("id") int id);

    //node-logs
    @POST("node-logs")
    Observable<response._node_logs> node_logs_post(@Body node_logs node_logs);
    @GET("node-logs/{id}")
    Observable<response._node_logs> node_logs_get(@Path("id") int id);
    @GET("node-logs/list/{accountid}")
    Observable<response._node_logslist> node_logs_get_list(@Path("accountid") int accountid);
    @PUT("node-logs/{id}")
    Observable<response._node_logs> node_logs_put(@Path("id") int id, @Body node_logs node_logs);
    @DELETE("node-logs/{id}")
    Observable<response._node_logs> node_logs_delete(@Path("id") int id);

    //node-type
    @POST("node-type")
    Observable<response._node_type> node_type_post(@Body node_type node_type);
    @GET("node-type/{id}")
    Observable<response._node_type> node_type_get(@Path("id") int id);
    @GET("node-type/list")
    Observable<response._node_typelist> node_type_get_list();
    @PUT("node-type/{id}")
    Observable<response._node_type> node_type_put(@Path("id") int id, @Body node_type node_type);
    @DELETE("node-type/{id}")
    Observable<response._node_type> node_type_delete(@Path("id") int id);

    //profile
    @POST("profile")
    Observable<response._profile> profile_post(@Body profile profile);
    @GET("profile/{id}")
    Observable<response._profile> profile_get(@Path("id") int id);
    @GET("profile/list")
    Observable<response._profilelist> profile_get_list();
    @PUT("profile/{id}")
    Observable<response._profile> profile_put(@Path("id") int id, @Body profile profile);
    @DELETE("profile/{id}")
    Observable<response._profile> profile_delete(@Path("id") int id);

    //room-device
    @POST("room")
    Observable<response._room> room_post(@Body room room);
    @GET("room/{id}")
    Observable<response._room> room_get(@Path("id") int id);
    @GET("room/list/{accountid}")
    Observable<response._roomlist> room_get_list(@Path("accountid") int accountid);
    @PUT("room/{id}")
    Observable<response._room> room_put(@Path("id") int id, @Body room room);
    @DELETE("room/{id}")
    Observable<response._room> room_delete(@Path("id") int id);

    //room-device
    @POST("room-device")
    Observable<response._room_device> room_device_post(@Body room_device room_device);
    @GET("room-device/{id}")
    Observable<response._room_device> room_device_get(@Path("id") int id);
    @GET("room-device/list/{roomid}")
    Observable<response._room_devicelist> room_device_get_list(@Path("roomid") int roomid);
    @PUT("room-device/{id}")
    Observable<response._room_device> room_device_put(@Path("id") int id, @Body room_device room_device);
    @DELETE("room-device/{id}")
    Observable<response._room_device> room_device_delete(@Path("id") int id);


    //scene
    @POST("scene")
    Observable<response._scene> scene_post(@Body scene scene);
    @GET("scene/{id}")
    Observable<response._scene> scene_get(@Path("id") int id);
    @GET("scene/list/{accountid}")
    Observable<response._scenelist> scene_get_list(@Path("accountid") int accountid);
    @PUT("scene/{id}")
    Observable<response._scene> scene_put(@Path("id") int id, @Body scene scene);
    @DELETE("scene/{id}")
    Observable<response._scene> scene_delete(@Path("id") int id);

    //scene-device
    @POST("scene-device")
    Observable<response._scene_device> scene_device_post(@Body scene_device scene_device);
    @GET("scene-device/{id}")
    Observable<response._scene_device> scene_device_get(@Path("id") int id);
    @GET("scene-device/list/{sceneid}")
    Observable<response._scene_devicelist> scene_device_get_list(@Path("sceneid") int sceneid);
    @PUT("scene-device/{id}")
    Observable<response._scene_device> scene_device_put(@Path("id") int id, @Body scene_device scene_device);
    @DELETE("scene-device/{id}")
    Observable<response._scene_device> scene_device_delete(@Path("id") int id);

    //user
    @POST("user")
    Observable<response._user> user_post(@Body user user);
    @GET("user/{id}")
    Observable<response._user> user_get(@Path("id") int id);
    @GET("user/list/{accountid}")
    Observable<response._userlist> user_get_list(@Path("accountid") int accountid);
    @PUT("user/{id}")
    Observable<response._user> user_put(@Path("id") int id, @Body user user);
    @DELETE("user/{id}")
    Observable<response._user> user_delete(@Path("id") int id);
    @POST("user/login")
    Observable<response._user> login_user(@Body request request);

}
