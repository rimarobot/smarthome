package com.example.smarthome.ui.chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.graphics.Rect;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.smarthome.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Collections;

public class ChatFragment extends Fragment {
    private  final Integer[] IMAGES = { R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
            R.mipmap.ic_launcher_round,R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,
            R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round ,
            R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round,R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round};
    private ArrayList<Integer> arrayList = new ArrayList<Integer>();
//    private ChatViewModel mViewModel;
    RecyclerView activeUserRecyclerView;


    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
//        mViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int ellipse = (int)( (displayMetrics.widthPixels -50) / (((int)displayMetrics.density) * 50));

        View root = inflater.inflate(R.layout.chat_fragment, container, false);
        activeUserRecyclerView= root.findViewById(R.id.active_user_list);
        activeUserRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(),
                LinearLayoutManager.HORIZONTAL,false));
        activeUserRecyclerView.addItemDecoration(new OverlapDecoration());
        activeUserRecyclerView.setHasFixedSize(true);
        Collections.addAll(arrayList, IMAGES);
        activeUserRecyclerView.setAdapter(new ActiveUsersRecyclerViewAdapter(arrayList, 8));
        return root;
    }




    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -20;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            outRect.set(0, 0, vertOverlap, 0);


        }
    }

}
