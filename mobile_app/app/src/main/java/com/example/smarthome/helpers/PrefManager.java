package com.example.smarthome.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;

import java.math.BigDecimal;

/**
 * Created by Ravi on 01/06/15.
 */
public class PrefManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared pref file name
    private static final String PREF_NAME = "smart-home";

    // Constructor
    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void put(String key, String Value) {
        editor.putString(key, Value);
        editor.commit();
    }

    public void put(String key, byte[] Value) {
        String encoded = Base64.encodeToString(Value, Base64.DEFAULT);
        editor.putString(key, encoded);
        editor.commit();
    }

    public void put(String key, boolean Value) {
        editor.putBoolean(key, Value);
        editor.commit();
    }

    public void put(String key, int Value) {
        editor.putInt(key, Value);
        editor.commit();
    }

    public void put(String key, BigDecimal Value) {
        editor.putLong(key, Value.longValue());
        editor.commit();
    }

    public int getInt(String Key) {
        if(pref.contains(Key))
            return pref.getInt(Key, 0);
        else
            return 0;
    }

//    public BigDecimal getBigDecimal(String Key) {
//        if(pref.contains(Key))
//            return new BigDecimal(pref.getLong(Key, 0));
//        else
//            return new BigDecimal(0);
//    }
    public BigDecimal getBigDecimal(String Key) {
        BigDecimal val =  new BigDecimal(0);

        try {
            if (pref.contains(Key))
                val =  new BigDecimal(pref.getLong(Key, 0));
            else
                val =  new BigDecimal(0);
        }catch (Exception ex){
            val =  new BigDecimal(0);
        }

        return  val;
    }


    public String getString(String Key) {

        if(pref.contains(Key))
            return pref.getString(Key, null);
        else
            return null;
    }

    public byte[] getByte(String Key) {

        if(pref.contains(Key)) {
            String encoded = pref.getString(Key, null);
            byte[] decoded = Base64.decode(encoded, Base64.DEFAULT);
            return  decoded;
        }
        else
            return null;
    }



    public boolean getBoolean(String Key) {
        if(pref.contains(Key))
            return pref.getBoolean(Key, false);
        else
            return false;
    }

    public void Clear() {
        editor.clear();
        editor.commit();
    }
}