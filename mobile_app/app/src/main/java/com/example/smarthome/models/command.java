package com.example.smarthome.models;

public class command  {
	public int command_id;
	public int device_type_id;
	public device_type device_type;
	public String template;
	public String decription;
	public String createdAt;
	public String updatedAt;

	public command() {
	}

	public command(int command_id, int device_type_id, device_type device_type, String template, String decription, String createdAt, String updatedAt) {
		this.command_id = command_id;
		this.device_type_id = device_type_id;
		this.device_type = device_type;
		this.template = template;
		this.decription = decription;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getCommand_id() {
		return command_id;
	}

	public void setCommand_id(int command_id) {
		this.command_id = command_id;
	}

	public int getDevice_type_id() {
		return device_type_id;
	}

	public void setDevice_type_id(int device_type_id) {
		this.device_type_id = device_type_id;
	}

	public com.example.smarthome.models.device_type getDevice_type() {
		return device_type;
	}

	public void setDevice_type(com.example.smarthome.models.device_type device_type) {
		this.device_type = device_type;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getDecription() {
		return decription;
	}

	public void setDecription(String decription) {
		this.decription = decription;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
