package com.example.smarthome.api;


import com.example.smarthome.models.account;
import com.example.smarthome.models.account_setting;
import com.example.smarthome.models.command;
import com.example.smarthome.models.control;
import com.example.smarthome.models.control_device;
import com.example.smarthome.models.device;
import com.example.smarthome.models.device_type;
import com.example.smarthome.models.network;
import com.example.smarthome.models.node;
import com.example.smarthome.models.node_data;
import com.example.smarthome.models.node_logs;
import com.example.smarthome.models.node_type;
import com.example.smarthome.models.profile;
import com.example.smarthome.models.room;
import com.example.smarthome.models.room_device;
import com.example.smarthome.models.scene;
import com.example.smarthome.models.scene_device;
import com.example.smarthome.models.user;

import java.util.List;

public class request {
  public String username;
  public String password;

    public request(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
