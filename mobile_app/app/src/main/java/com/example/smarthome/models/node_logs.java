package com.example.smarthome.models;

public class node_logs
{
	public int node_log_id;
	public int node_id;
	public node node;
	public String data;
	public String createdAt;
	public String updatedAt;

	public node_logs() {
	}

	public node_logs(int node_log_id, int node_id, com.example.smarthome.models.node node, String data, String createdAt, String updatedAt) {
		this.node_log_id = node_log_id;
		this.node_id = node_id;
		this.node = node;
		this.data = data;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getNode_log_id() {
		return node_log_id;
	}

	public void setNode_log_id(int node_log_id) {
		this.node_log_id = node_log_id;
	}

	public int getNode_id() {
		return node_id;
	}

	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}

	public com.example.smarthome.models.node getNode() {
		return node;
	}

	public void setNode(com.example.smarthome.models.node node) {
		this.node = node;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
