package com.example.smarthome.ui.onboarding;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnBoardingFirstFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnBoardingFirstFragment extends Fragment {

    public OnBoardingFirstFragment() {
        // Required empty public constructor
    }


    public static OnBoardingFirstFragment newInstance() {
        OnBoardingFirstFragment fragment = new OnBoardingFirstFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_on_boarding_first, container, false);
    }
}
