package com.example.smarthome.ui.chat;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.smarthome.R;

import java.util.ArrayList;


public class ActiveUsersRecyclerViewAdapter extends RecyclerView.Adapter<ActiveUsersRecyclerViewAdapter.ViewHolder> {

    //private final List<DummyItem> mValues;
//    private final OnListFragmentInteractionListener mListener;
    private ArrayList<Integer> mValues = new ArrayList<Integer>();
    private int elipsce;

    //public ActiveUsersRecyclerViewAdapter(List<DummyItem> items, OnListFragmentInteractionListener listener) {
    public ActiveUsersRecyclerViewAdapter(ArrayList<Integer> imagesArray, int _elipsce) {
        mValues = imagesArray;
        elipsce = _elipsce;
//        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_active_user_view, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.imageView.setImageResource(mValues.get(position));

        if (position == elipsce) {
            holder.relativeLayout.setVisibility(View.VISIBLE);
            holder.tvCount.setText("+" + String.valueOf(mValues.size()-(elipsce + 1 )));
        }else {
            holder.relativeLayout.setVisibility(View.GONE);
        }

//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (null != mListener) {
//                    // Notify the active callbacks interface (the activity, if the
//                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(holder.mItem);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relativeLayout;
        TextView tvCount;
        ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            relativeLayout = v.findViewById(R.id.relative);
            tvCount = v.findViewById(R.id.tvCount);
            imageView = v.findViewById(R.id.profile_image);
        }
    }
}
