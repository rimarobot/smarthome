package com.example.smarthome.ui.access.registration;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.models.account_setting;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    Button mCancelBtn, mNextBtn, mFinishBtn;
    ImageView zero, one, two;
    ImageView[] indicators;
    int page = 0;
    private ApiInterface api;
    private Disposable disposable;
    private RegistrationViewModel model;
    private com.example.smarthome.models.account account;
    private com.example.smarthome.models.account_setting settings;

    public RegisterFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(RegistrationViewModel.class);
        model.getAccount().observe(this, account1 -> {
            account = account1;
        });
        model.getSetting().observe(this, account_settings -> {
            settings = account_settings;
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), this.getChildFragmentManager());
        mViewPager = root.findViewById(R.id.view_pager_registration);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mNextBtn = (Button) root.findViewById(R.id.reg_btn_next);
        mCancelBtn = (Button) root.findViewById(R.id.reg_btn_cancel);
        mFinishBtn = (Button) root.findViewById(R.id.reg_btn_finish);
        zero = (ImageView) root.findViewById(R.id.reg_indicator_0);
        one = (ImageView) root.findViewById(R.id.reg_indicator_1);
        two = (ImageView) root.findViewById(R.id.reg_indicator_2);
        indicators = new ImageView[]{zero, one, two};

        mViewPager.setCurrentItem(page);
        updateIndicators(page);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                page = position;

                updateIndicators(page);
                mNextBtn.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
                mFinishBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment frg = RegisterFragment.this.getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.view_pager_registration + ":" + mViewPager.getCurrentItem());
                if(frg != null && frg instanceof Step){
                    VerificationError error =  ((Step) frg).verifyStep();
                    if(TextUtils.isEmpty(error.getErrorMessage())){
                       if(page == 0){
                           page += 1;
                           mViewPager.setCurrentItem(page, true);
                       }
                       else if(page == 1){
                            saveAccount();
                        }
                    }
                }
            }
        });

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(page >0){
                    page = 0;
                    mViewPager.setCurrentItem(page, true);
                }
                else {
                    Navigation.findNavController(v).navigate(R.id.action_registerFragment_to_loginFragment);
                }
            }
        });

        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment frg = RegisterFragment.this.getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.view_pager_registration + ":" + mViewPager.getCurrentItem());
                if(frg != null && frg instanceof Step){
                    VerificationError error =  ((Step) frg).verifyStep();
                    if(TextUtils.isEmpty(error.getErrorMessage())){
                        if(page == 2){
                            saveSettings();
                        }
                    }
                }
            }
        });

        return root;
    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }

    void saveAccount(){

        api.account_post(account)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._account>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._account _account) {
                        if(_account.success){
                            account = _account.data;
                            model.setAccount(account);
                            model.setSetting(new account_setting( _account.data.account_id ));
                            page += 1;
                            mViewPager.setCurrentItem(page, true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void saveSettings(){

        api.account_settings_post(settings)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._account_settings>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._account_settings _settings) {
                        if(_settings.success){
                            settings = _settings.data;
                            model.setSetting(settings);
                            Navigation.findNavController(RegisterFragment.this.getView()).navigate(R.id.action_registerFragment_to_loginFragment);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
