package com.example.smarthome.api;


import com.example.smarthome.models.*;
import java.util.List;

public class response {

    public class _user
    {
        public boolean success;
        public user data;
        public String error;
    }
    public class _userlist
    {
        public boolean success;
        public List<user> data;
        public String error;
    }
    public class _scene_device
    {
        public boolean success;
        public scene_device data;
        public String error;
    }
    public class _scene_devicelist
    {
        public boolean success;
        public List<scene_device> data;
        public String error;
    }


    public class _scene
    {
        public boolean success;
        public scene data;
        public String error;
    }
    public class _scenelist
    {
        public boolean success;
        public List<scene> data;
        public String error;
    }

    public class _room_device
    {
        public boolean success;
        public room_device data;
        public String error;
    }
    public class _room_devicelist
    {
        public boolean success;
        public List<room_device> data;
        public String error;
    }

    public class _profile
    {
        public boolean success;
        public profile data;
        public String error;
    }
    public class _profilelist
    {
        public boolean success;
        public List<profile> data;
        public String error;
    }

    public class _room
    {
        public boolean success;
        public room data;
        public String error;
    }
    public class _roomlist
    {
        public boolean success;
        public List<room> data;
        public String error;
    }

    public class _node_type
    {
        public boolean success;
        public node_type data;
        public String error;
    }
    public class _node_typelist
    {
        public boolean success;
        public List<node_type> data;
        public String error;
    }

    public class _node
    {
        public boolean success;
        public node data;
        public String error;
    }
    public class _nodelist
    {
        public boolean success;
        public List<node> data;
        public String error;
    }

    public class _node_logs
    {
        public boolean success;
        public node_logs data;
        public String error;
    }
    public class _node_logslist
    {
        public boolean success;
        public List<node_logs> data;
        public String error;
    }


    public class _node_data
    {
        public boolean success;
        public node_data data;
        public String error;
    }
    public class _node_datalist
    {
        public boolean success;
        public List<node_data> data;
        public String error;
    }

    public class _device
    {
        public boolean success;
        public device data;
        public String error;
    }
    public class _devicelist
    {
        public boolean success;
        public List<device> data;
        public String error;
    }

    public class _network
    {
        public boolean success;
        public network data;
        public String error;
    }
    public class _networklist
    {
        public boolean success;
        public List<network> data;
        public String error;
    }

    public class _device_type
    {
        public boolean success;
        public device_type data;
        public String error;
    }
    public class _device_typelist
    {
        public boolean success;
        public List<device> data;
        public String error;
    }


    public class _control
    {
        public boolean success;
        public control data;
        public String error;
    }
    public class _controllist
    {
        public boolean success;
        public List<control> data;
        public String error;
    }

    public class _control_device
    {
        public boolean success;
        public control_device data;
        public String error;
    }
    public class _control_devicelist
    {
        public boolean success;
        public List<control_device> data;
        public String error;
    }

    public class _account
    {
        public boolean success;
        public account data;
        public String error;
    }
    public class _accountlist
    {
        public boolean success;
        public List<account> data;
        public String error;
    }

    public class _command
    {
        public boolean success;
        public command data;
        public String error;
    }
    public class _commandlist
    {
        public boolean success;
        public List<command> data;
        public String error;
    }

    public class _account_settings
    {
        public boolean success;
        public account_setting data;
        public String error;
    }
    public class _account_settingslist
    {
        public boolean success;
        public List<account_setting> data;
        public String error;
    }

}
