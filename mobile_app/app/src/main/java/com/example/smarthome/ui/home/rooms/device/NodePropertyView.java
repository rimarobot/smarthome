package com.example.smarthome.ui.home.rooms.device;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.databinding.FragmentNodePropertyViewBinding;
import com.example.smarthome.ui.home.HomeViewModel;

import org.json.JSONException;
import org.json.JSONObject;

public class NodePropertyView extends Fragment {
    private HomeViewModel model;
    private FragmentNodePropertyViewBinding binding;
    private com.example.smarthome.models.device mDevice;
    private OnStatusReceiver onStatusReceiver = new OnStatusReceiver();
    public class OnStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            /*
             {    room:room, T: node type,
                  N:node id, V:voltage, X:memory,
                  Y:rom, F:software version, H:hardware version,
                  P:processor, E:temperature, D:data
              }
             */
            String data = intent.getExtras().getString("data");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject object = new JSONObject(data);
                        binding.nodeVoltage.setText(object.getString("V"));
                        binding.nodeTemp.setText(object.getString("E"));
                        binding.nodeRam.setText(object.getString("X"));
                        binding.nodeStatus.setText("ON");
                        binding.nodeId.setText(object.getString("N"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    public NodePropertyView() {
        // Required empty public constructor
    }

    public static NodePropertyView newInstance() {
        return new NodePropertyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getDeviceSelected().observe(this, data ->{
            mDevice = data;
            binding.nodeId.setText(mDevice.node.node_address);
            binding.nodeStatus.setText( (mDevice.node.status==1? "ON" : "OFF"));
            binding.nodeRam.setText(mDevice.node.memory);
            binding.nodeTemp.setText(mDevice.node.temperature);
            binding.nodeVoltage.setText(mDevice.node.voltage);
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_node_property_view, container, false);
        binding = FragmentNodePropertyViewBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(onStatusReceiver, new IntentFilter("onStatus"));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(onStatusReceiver);
    }

}
