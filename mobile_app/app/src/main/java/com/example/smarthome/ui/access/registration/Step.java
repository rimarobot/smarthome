package com.example.smarthome.ui.access.registration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface Step {

    @Nullable
    VerificationError verifyStep();

}

