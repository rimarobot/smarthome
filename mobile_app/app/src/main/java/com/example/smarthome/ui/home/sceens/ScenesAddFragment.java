package com.example.smarthome.ui.home.sceens;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentSceneAddBinding;
import com.example.smarthome.models.scene_device;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ScenesAddFragment extends Fragment {

    private FragmentSceneAddBinding binding;
    private com.example.smarthome.models.scene scene = new com.example.smarthome.models.scene();
    private List<scene_device> list = new ArrayList<>();
    private HomeViewModel model;
    private static int mColumnCount = 2;
    private ApiInterface api;
    private Disposable disposable;
    public ScenesAddFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ScenesAddFragment newInstance( ) {
        return new ScenesAddFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getSceneSelected().observe(this, data ->{
            scene = data;
            binding.sceneName.setText(scene.name);
            list.clear();
            if(data.scene_devices != null)
                list.addAll(data.scene_devices);
            binding.deviceList.getAdapter().notifyDataSetChanged();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // return inflater.inflate(R.layout.fragment_scene_add, container, false);
        binding = FragmentSceneAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        if (mColumnCount <= 1) {
            binding.deviceList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        } else {
            binding.deviceList.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        }
        binding.deviceList.setAdapter(new DeviceAdapter(list, this.getContext(), new OnItemInteractionListener(){

            @Override
            public void onRemoveClicked(scene_device item) {

            }
        }));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView status;
        final View mView;
        final ImageView icon;
        scene_device item;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.device_list_item_view, parent, false));
            mView = itemView;
            name = (TextView)itemView.findViewById(R.id.device_name);
            status = (TextView)itemView.findViewById(R.id.device_status);
            icon = (ImageView)itemView.findViewById(R.id.device_icon);
        }
    }

    private class DeviceAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<scene_device> list;
        private final Context context;
        private final OnItemInteractionListener listener;

        DeviceAdapter(List<scene_device> data, Context cxt, OnItemInteractionListener listener1) {
            list = data;
            context = cxt;
            listener = listener1;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.item =  list.get(position);
            holder.name.setText(holder.item.name);
            holder.status.setText(holder.item.device.status);
//            int resID = context.getResources().getIdentifier("@drawable/"+ holder.item.device.image , "drawable", context.getPackageName());
//            holder.icon.setImageResource(resID);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener) {
                        listener.onRemoveClicked(holder.item);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    private interface OnItemInteractionListener {
        void onRemoveClicked(scene_device item);
    }

    private void save(){
        String room_icon = "";
        if(TextUtils.isEmpty(binding.sceneName.getText().toString())){
            binding.sceneName.setError("*");
            return;
        }

        scene.account_id = 1;
        scene.name = binding.sceneName.getText().toString();
        if(scene.scene_id == 0)
            create_scene();
        else
            update_scene();

        if(scene.scene_devices != null)
            if(scene.scene_devices.size()> 0)
                for(scene_device device : scene.scene_devices) {
                    if(device.scene_device_id == 0)
                        create_scene_device(device);
                    else
                        update_scene_device(device);
                }
    }

    void create_scene(){
        api.scene_post(scene)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._scene>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._scene data) {
                        if(data.success == true)
                            scene.scene_id = data.data.scene_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void update_scene(){
        api.scene_put(scene.scene_id, scene)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._scene>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._scene data) {
                        if(data.success == true)
                            scene.scene_id = data.data.scene_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void create_scene_device(scene_device device){

        api.scene_device_post(device)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<response._scene_device>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._scene_device data) {
//                            if (data.success == true)
//                                room.room_id = data.data.room_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void update_scene_device(scene_device device){
        api.scene_device_put(device.scene_device_id, device)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<response._scene_device>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._scene_device data) {
//                            if (data.success == true)
//                                room.room_id = data.data.room_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
