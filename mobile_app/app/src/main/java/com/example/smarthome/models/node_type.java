package com.example.smarthome.models;

public class node_type
{
	public int node_type_id;
	public String value;
	public String tittle;
	public String description;
	public String createdAt;
	public String updatedAt;

	public node_type() {
	}

	public node_type(int node_type_id, String value, String tittle, String description, String createdAt, String updatedAt) {
		this.node_type_id = node_type_id;
		this.value = value;
		this.tittle = tittle;
		this.description = description;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getNode_type_id() {
		return node_type_id;
	}

	public void setNode_type_id(int node_type_id) {
		this.node_type_id = node_type_id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
