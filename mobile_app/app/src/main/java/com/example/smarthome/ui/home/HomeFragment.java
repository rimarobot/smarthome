package com.example.smarthome.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;

import com.example.smarthome.R;
import com.example.smarthome.SmartHomeApplication;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.helpers.Config;
import com.example.smarthome.models.user;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.disposables.Disposable;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class HomeFragment extends Fragment{

    private static final String TAG = "Smart Home App";
    private ViewPager viewPager;
    private HomeViewModel  model;
    private ApiInterface api;
    private Disposable disposable;
    private Socket mSocket;
    private Boolean isConnected = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        SmartHomeApplication app = (SmartHomeApplication) getActivity().getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("login", onLogin);
        mSocket.on("register", onRegister);
        mSocket.on("routines", onRoutine);
        mSocket.on("status", onStatus);
        mSocket.on("weather", onWeather);
        mSocket.on("command", onCommand);
        mSocket.on("network", onNetwork);
        mSocket.on("verify", onVerify);
        mSocket.on("update", onUpdate);
        mSocket.on("config", onConfig);
        mSocket.connect();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();

        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("login", onLogin);
        mSocket.off("register", onRegister);
        mSocket.off("routines", onRoutine);
        mSocket.off("status", onStatus);
        mSocket.off("weather", onWeather);
        mSocket.off("command", onCommand);
        mSocket.off("network", onNetwork);
        mSocket.off("verify", onVerify);
        mSocket.off("update", onUpdate);
        mSocket.off("config", onConfig);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), this.getChildFragmentManager());
        viewPager = root.findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabs = root.findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Navigation.findNavController(view).popBackStack(R.id.navigation_home, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_add, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_add:
                switch (viewPager.getCurrentItem()){
                    case 1:
                        NavHostFragment.findNavController(HomeFragment.this).navigate(R.id.action_navigation_home_to_sceensAddFragment);
                        break;
                    case 2:
                        NavHostFragment.findNavController(HomeFragment.this).navigate(R.id.action_navigation_home_to_routinesAddFragment);
                        break;
                    case 3:
                        NavHostFragment.findNavController(HomeFragment.this).navigate(R.id.action_navigation_home_to_deviceAddFragment);
                        break;
                    default:
                        NavHostFragment.findNavController(HomeFragment.this).navigate(R.id.action_navigation_home_to_roomsAddFragment);
                        break;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "connected");
                    JSONObject object = new JSONObject();
                    try {
                        object.put("room", Config.account.account_setting.account_code);
                        object.put("address", Config.account.account_id);
                        object.put("username", Config.account.surname);
                        mSocket.emit("login", object);
                    } catch (JSONException e) { 
                        e.printStackTrace();
                    } 
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting"); 
                }
            });
        }
    };

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String status;
                    String msg;
                    try {
                        username = data.getString("username");
                        status = data.getString("status");
                        msg = data.getString("msg");
                        if(username.equals(Config.account.username)){
                           // you are logged in
                            for ( int i =0; i < Config.account.users.size(); i++ ) {
                               user u = Config.account.users.get(i);
                                if(u.username.equals(username)) {
                                    u.status = 1;
                                   Config.account.users.set(i,u);
                               }
                            }
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }

                }
            });
        }
    };

    private Emitter.Listener onRegister = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onRegister");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onRoutine = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onRoutine");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onStatus = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onStatus");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onWeather = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onWeather");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };


    private Emitter.Listener onCommand = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onCommand");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onNetwork = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onNetwork");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onVerify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onVerify");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onUpdate = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onUpdate");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };

    private Emitter.Listener onConfig = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Intent intent = new Intent();
                    intent.setAction("onConfig");
                    intent.putExtra("data", data.toString());
                    HomeFragment.this.getContext().sendBroadcast(intent);
                }
            });
        }
    };
}
