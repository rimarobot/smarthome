package com.example.smarthome.ui.access.registration;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentRegistrationFirstBinding;
import com.example.smarthome.models.account;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationFirstFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationFirstFragment extends Fragment implements Step {
    private FragmentRegistrationFirstBinding binding;
    private RegistrationViewModel model;
    public RegistrationFirstFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RegistrationFirstFragment newInstance() {
        return new RegistrationFirstFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        model = new ViewModelProvider(getActivity()).get(RegistrationViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View root = inflater.inflate(R.layout.fragment_registration_first, container, false);
        binding = FragmentRegistrationFirstBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(),android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.countries));
        binding.country.setAdapter(adapter);
        binding.dob.setInputType(InputType.TYPE_NULL);
        binding.dob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    MaterialDatePicker picker = MaterialDatePicker.Builder.datePicker() .build();
                    picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
                        @Override
                        public void onPositiveButtonClick(Object selection) {
                            TimeZone timeZoneUTC = TimeZone.getDefault();
                            int offsetFromUTC = timeZoneUTC.getOffset(new Date().getTime()) * -1;
                            SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy/MMM/dd", Locale.US);
                            Date date = new Date((Long)selection + offsetFromUTC);
                            binding.dob.setText(simpleFormat.format(date) )  ;
                        }
                    });
                    picker.show(RegistrationFirstFragment.this.getChildFragmentManager(), picker.toString());
                }
            }
        });

        binding.dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDatePicker picker = MaterialDatePicker.Builder.datePicker().build();
                picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        TimeZone timeZoneUTC = TimeZone.getDefault();
                        int offsetFromUTC = timeZoneUTC.getOffset(new Date().getTime()) * -1;
                        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy/MMM/dd", Locale.US);
                        Date date = new Date((Long)selection + offsetFromUTC);
                        binding.dob.setText(simpleFormat.format(date) )  ;
                    }
                });
                picker.show(RegistrationFirstFragment.this.getChildFragmentManager(), picker.toString());
            }
        });
        return root;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        String Errors = "";
        if((TextUtils.isEmpty(binding.fullname.getText().toString()))){
            Errors = "name required";
            binding.fullname.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.email.getText().toString()))){
            Errors = "email required";
            binding.email.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.phone.getText().toString()))){
            Errors = "phone required";
            binding.phone.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.country.getText().toString()))){
            Errors = "country required";
            binding.country.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.district.getText().toString()))){
            Errors = "district required";
            binding.district.setError(Errors);
            return new VerificationError(Errors);
        }

        if((TextUtils.isEmpty(binding.town.getText().toString()))){
            Errors = "town required";
            binding.town.setError(Errors);
            return new VerificationError(Errors);
        }

        if((TextUtils.isEmpty(binding.address.getText().toString()))){
            Errors = "address required";
            binding.address.setError(Errors);
            return new VerificationError(Errors);
        }
        int radioButtonID = binding.gender.getCheckedRadioButtonId();

        if(radioButtonID == 0){
            Errors = "gender required";
            //binding.gender.setError(Errors);
            return new VerificationError(Errors);
        }
        View radioButton = binding.gender.findViewById(radioButtonID);
        int idx = binding.gender.indexOfChild(radioButton);
        RadioButton r = (RadioButton) binding.gender.getChildAt(idx);
        String gender = r.getText().toString();
        if((TextUtils.isEmpty(gender))){
            Errors = "gender required";
            //binding.gender.(Errors);
            return new VerificationError(Errors);
        }

        if((TextUtils.isEmpty(binding.dob.getText().toString()))){
            Errors = "date of birth required";
            binding.dob.setError(Errors);
            return new VerificationError(Errors);
        }

        String[] name = binding.fullname.getText().toString().split(" ");
        String surname = "",firstname ="";
        if (name.length >0)
            surname = name[0];
        if(name.length >1)
            firstname = name[1];

        model.setAccount(new account( surname, firstname ,  binding.email.getText().toString(),  binding.phone.getText().toString(),
                binding.country.getText().toString(),  binding.address.getText().toString(),  binding.town.getText().toString(),
                binding.district.getText().toString(),  gender,  binding.dob.getText().toString()  ));
        return new VerificationError(Errors);
    }
}
