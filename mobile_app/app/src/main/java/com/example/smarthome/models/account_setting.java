package com.example.smarthome.models;

public class account_setting
 {
    public int setting_id;
	public int account_id;
	public String account_code;
	public String account_hash;
	public String root_address;
	public String mesh_ssid;
	public String mesh_password;
	public int mesh_port;
	public int mesh_channel;
	public int mesh_hidden;
	public int mesh_connections;
	public String gateway_ssid;
	public String gateway_password;
	public String host_address;
	public int host_port;
	public String createdAt;
	//public String updatedAt;


	 public account_setting() {
	 }

	 public account_setting(int account_id) {
		 this.account_id = account_id;
	 }
 }
