package com.example.smarthome.models;

public class device_type
{
	public int device_type_id;
	public String name;
	public String decription;
	public String createdAt;
	public String updatedAt;

	public device_type() {
	}

	public device_type(int device_type_id, String name, String decription, String createdAt, String updatedAt) {
		this.device_type_id = device_type_id;
		this.name = name;
		this.decription = decription;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getDevice_type_id() {
		return device_type_id;
	}

	public void setDevice_type_id(int device_type_id) {
		this.device_type_id = device_type_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDecription() {
		return decription;
	}

	public void setDecription(String decription) {
		this.decription = decription;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
