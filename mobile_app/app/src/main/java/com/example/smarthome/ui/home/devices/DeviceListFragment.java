package com.example.smarthome.ui.home.devices;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentDeviceListListBinding;
import com.example.smarthome.models.device;
import com.example.smarthome.models.node;
import com.example.smarthome.ui.home.HomeViewModel;

import java.util.ArrayList;
import java.util.List;


public class DeviceListFragment extends Fragment implements DevicesRecyclerViewAdapter.OnDeviceInteractionListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private List<node> list = new ArrayList<>();;
    private HomeViewModel model;
    private FragmentDeviceListListBinding binding;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DeviceListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DeviceListFragment newInstance(int columnCount) {
        DeviceListFragment fragment = new DeviceListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getNodeList().observe(this, data ->{
            list.clear();
            list.addAll(data);
            binding.list.getAdapter().notifyDataSetChanged();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.fragment_device_list_list, container, false);
        binding = FragmentDeviceListListBinding.inflate(inflater, container, false);
        // Set the adapter
        View view = binding.getRoot();
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new NodesRecyclerViewAdapter(list, context, this));
        }
        return view;
    }

    @Override
    public void onDeviceClicked(device item) {
        model.setDeviceSelected(item);
        Navigation.findNavController(this.getView()).navigate(R.id.action_navigation_home_to_deviceDetailsFragment);
    }


}
