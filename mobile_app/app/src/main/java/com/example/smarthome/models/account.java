package com.example.smarthome.models;

import com.google.gson.annotations.Expose;

import java.util.List;

public class account {
	public int	account_id;
	public String surname;
	public String firstname;
	public String email;
	public String phone;
	public String country;
	public String address;
	public String town;
	public String district;
	public String username;
	public String password;
	public String gender;
	public String dob;
	public int status;
	public String createdAt;
    public account_setting account_setting;
	public network network;
	public List<user> users;
	public account() {
	}

	public account(String surname, String firstname, String email, String phone, String country, String address, String town, String district, String gender, String dob) {
		this.surname = surname;
		this.firstname = firstname;
		this.email = email;
		this.phone = phone;
		this.country = country;
		this.address = address;
		this.town = town;
		this.district = district;
		this.gender = gender;
		this.dob = dob;
	}
}
