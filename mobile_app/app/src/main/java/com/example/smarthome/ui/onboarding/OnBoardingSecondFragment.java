package com.example.smarthome.ui.onboarding;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnBoardingSecondFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnBoardingSecondFragment extends Fragment {

    public OnBoardingSecondFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static OnBoardingSecondFragment newInstance() {
        OnBoardingSecondFragment fragment = new OnBoardingSecondFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_on_boarding_second, container, false);
    }
}
