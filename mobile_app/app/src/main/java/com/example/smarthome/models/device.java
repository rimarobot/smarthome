package com.example.smarthome.models;

public class device
{
	public int device_id;
	public int node_id;
	public node node;
	public int device_type_id;
	public device_type device_type;
	public String name;
	public String status;
	public String image;
	public String createdAt;
	//public String updatedAt;

	public device() {
	}

	public device(int device_id, int node_id, com.example.smarthome.models.node node, int device_type_id, com.example.smarthome.models.device_type device_type, String name, String createdAt, String updatedAt) {
		this.device_id = device_id;
		this.node_id = node_id;
		this.node = node;
		this.device_type_id = device_type_id;
		this.device_type = device_type;
		this.name = name;
		this.createdAt = createdAt;
		//this.updatedAt = updatedAt;
	}

}
