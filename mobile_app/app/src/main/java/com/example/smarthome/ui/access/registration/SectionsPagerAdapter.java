package com.example.smarthome.ui.access.registration;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.smarthome.ui.onboarding.OnBoardingFirstFragment;
import com.example.smarthome.ui.onboarding.OnBoardingSecondFragment;
import com.example.smarthome.ui.onboarding.OnBoardingThirdFragment;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 1:
                return RegistrationSecondFragment.newInstance();
            case 2:
                return RegistrationThirdFragment.newInstance();
            default:
                return RegistrationFirstFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}