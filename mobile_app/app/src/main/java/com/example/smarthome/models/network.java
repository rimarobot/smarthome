package com.example.smarthome.models;

public class network
{
	public int network_id;
	public int account_id;
	public account account;
	public String data;
	public String createdAt;
	public String updatedAt;

	public network() {
	}

	public network(int network_id, int account_id, com.example.smarthome.models.account account, String data, String createdAt, String updatedAt) {
		this.network_id = network_id;
		this.account_id = account_id;
		this.account = account;
		this.data = data;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public int getNetwork_id() {
		return network_id;
	}

	public void setNetwork_id(int network_id) {
		this.network_id = network_id;
	}

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public com.example.smarthome.models.account getAccount() {
		return account;
	}

	public void setAccount(com.example.smarthome.models.account account) {
		this.account = account;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
