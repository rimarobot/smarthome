package com.example.smarthome.ui.home.sceens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.smarthome.R;
import com.example.smarthome.models.scene;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.List;


public class ScenesRecyclerViewAdapter extends RecyclerView.Adapter<ScenesRecyclerViewAdapter.ViewHolder> {

    private final List<scene> mValues;
    private final OnSceneInteractionListener mListener;

    public ScenesRecyclerViewAdapter(List<scene> items, Context context, OnSceneInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_scenes_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        int count = 0;
        try{
            count =   mValues.get(position).scene_devices.size();
        }catch (Exception e){
        }
        holder.mIdView.setText("x" + count +" Devices ");
        holder.mContentView.setText(mValues.get(position).name);
        holder.mStatusView.setChecked( (mValues.get(position).status == 1) ? true : false);
        holder.mStatusView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (null != mListener) {
                    mListener.onStatusClicked(holder.mItem, isChecked);
                }
            }
        });

        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onDetailsClicked(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final SwitchMaterial mStatusView;
        public final ImageView mImageView;
        public scene mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            mStatusView = (SwitchMaterial) view.findViewById(R.id.status);
            mImageView = (ImageView) view.findViewById(R.id.details);
        }

    }

    public interface OnSceneInteractionListener {
        // TODO: Update argument type and name
        void onStatusClicked(scene item, boolean isChecked);
        void onDetailsClicked(scene item);

    }
}
