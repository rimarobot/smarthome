package com.example.smarthome.ui.settings.family;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentUserDetailsBinding;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public class UserDetailsFragment extends Fragment {
    private HomeViewModel model;
    private com.example.smarthome.models.user user = new com.example.smarthome.models.user();
    private FragmentUserDetailsBinding binding;
    private ApiInterface api;
    private Disposable disposable;
    private boolean isSuccessful = false;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static UserDetailsFragment newInstance() {
        return new UserDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getSelectedUser().observe(this, data -> {
            user = data;
            loadUI();
        });
    }

    void loadUI(){
        if(user != null && user.user_id != 0) {
            binding.fullname.setText(user.surname + " " + user.firstname);
            binding.email.setText(user.email);
            binding.phone.setText(user.phone);
            Chip chip = (Chip) binding.profile.getChildAt(user.profile_id -1);
            chip.setChecked(true);
//            binding.settingAddress.setText(account.address);
//            binding.settingGatewaySsid.setText(account.account_setting.gateway_ssid);
//            binding.settingGatewayPassword.setText(account.account_setting.gateway_password);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        //return inflater.inflate(R.layout.fragment_user_details, container, false);
        binding = FragmentUserDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void save() {
        String Errors = "";
        if((TextUtils.isEmpty(binding.fullname.getText().toString()))){
            Errors = "name required";
            binding.fullname.setError(Errors);
            return;
        }
        if((TextUtils.isEmpty(binding.email.getText().toString()))){
            Errors = "email required";
            binding.email.setError(Errors);
            return;
        }
        if((TextUtils.isEmpty(binding.phone.getText().toString()))){
            Errors = "phone required";
            binding.phone.setError(Errors);
            return;
        }
        int profile = 0;
        for (int i = 0; i < binding.profile.getChildCount(); i++) {
            Chip chip = (Chip) binding.profile.getChildAt(i);
            if(chip.isChecked()) {
                profile = i + 1;
                break;
            }
        }

        if(profile ==0){
            Errors = "profile required";
            binding.phone.setError(Errors);
            return;
        }

        String[] name = binding.fullname.getText().toString().split(" ");
        String surname = "",firstname ="";
        if (name.length >0)
            surname = name[0];
        if(name.length >1)
            firstname = name[1];
         user.firstname  = firstname;
         user.surname = surname;
         user.email =  binding.email.getText().toString();
         user.phone = binding.phone.getText().toString();
         user.account_id =  1;
         user.profile_id = profile;

        if(user.user_id == 0)
            create_user();
        else
            update_user();
    }

    void create_user(){
        api.user_post(user)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._user>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._user data) {
                        isSuccessful = data.success;
                        if(data.success == true) {
                            user.user_id = data.data.user_id;
                            Navigation.findNavController(UserDetailsFragment.this.getView()).popBackStack();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void update_user(){
        api.user_put(user.user_id, user)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._user>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._user data) {
                        if(data.success)
                             Navigation.findNavController(UserDetailsFragment.this.getView()).popBackStack();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
