package com.example.smarthome.ui.settings.family;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.models.user;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.List;


public class UsersRecyclerViewAdapter extends RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder> {

    private final List<user> mValues;
    private final OnUserInteractionListener mListener;
    private  final  Context context;
    public UsersRecyclerViewAdapter(List<user> items, Context ctx, OnUserInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_users_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNameView.setText(holder.mItem.surname + " " + holder.mItem.firstname);
        holder.mStatusView.setSelected( (holder.mItem.status == 1) ? true : false);
        //String image = holder.mItem.email;
        //holder.mImageView.setImageDrawable(context.getResources().getDrawable(getImage(image)));
        holder.mStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onUserStatusClicked(holder.mItem);
                }
            }
        });
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onUserDetailsClicked(holder.mItem);
                }
            }
        });
        holder.mNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onUserDetailsClicked(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final SwitchMaterial mStatusView;
        public final ShapeableImageView mImageView;
        public user mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.user_name);
            mStatusView = (SwitchMaterial) view.findViewById(R.id.user_status);
            mImageView = (ShapeableImageView) view.findViewById(R.id.user_image);
        }

    }

    public interface OnUserInteractionListener {
        void onUserStatusClicked(user item);
        void onUserDetailsClicked(user item);
    }
}
