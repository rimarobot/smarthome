package com.example.smarthome.ui.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.smarthome.R;
import com.example.smarthome.databinding.FragmentSettingsBinding;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SettingsFragment extends Fragment {

    private HomeViewModel model;
    private com.example.smarthome.models.account account;
    private FragmentSettingsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);

        //View root = inflater.inflate(R.layout.fragment_settings, container, false);
        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        ((LinearLayout)root.findViewById(R.id.button_home_setting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_navigation_settings_to_homeSettingFragment);
            }
        });
        ((LinearLayout)root.findViewById(R.id.button_messages)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_navigation_settings_to_messagesFragment);
            }
        });
        ((LinearLayout)root.findViewById(R.id.button_family_access)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_navigation_settings_to_usersListFragment);
            }
        });
        ((LinearLayout)root.findViewById(R.id.button_support)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_navigation_settings_to_supportFragment);
            }
        });
        ((LinearLayout)root.findViewById(R.id.button_change_password)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_navigation_settings_to_changePasswordFragment);
            }
        });
        ((LinearLayout)root.findViewById(R.id.button_sign_out)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getAccoutDetails().observe(this, data -> {
            account = data;
            loadUI();
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadUI();
    }

    void loadUI(){
        if(account != null) {
            binding.settingFullname.setText(account.surname.toUpperCase() + " " + account.firstname.toUpperCase());
            binding.settingEmail.setText(account.email);
            binding.settingPhone.setText(account.phone);
            binding.settingAddress.setText(account.address);
        }
    }

    //    @Override
//    public void onResume() {
//        super.onResume();
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
//        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
//    }


}
