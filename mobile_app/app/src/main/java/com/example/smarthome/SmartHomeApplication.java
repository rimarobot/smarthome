package com.example.smarthome;

import android.app.Application;

import com.example.smarthome.helpers.Config;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class SmartHomeApplication  extends Application {
    private Socket mSocket;
    {
        try {

            mSocket = IO.socket(Config.socket_server);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}
