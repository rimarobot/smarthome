package com.example.smarthome.ui.access.registration;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.databinding.FragmentRegistrationThirdBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationThirdFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationThirdFragment extends Fragment implements Step{
    private FragmentRegistrationThirdBinding binding;
    private com.example.smarthome.models.account_setting settings;
    private RegistrationViewModel model;
    public RegistrationThirdFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RegistrationThirdFragment newInstance( ) {
        return new RegistrationThirdFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this.getActivity()).get(RegistrationViewModel.class);
        model.getSetting().observe(this, data -> {
            settings = data;
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_registration_third, container, false);
        binding = FragmentRegistrationThirdBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        String Errors = "";
        if((TextUtils.isEmpty(binding.gatewaySsid.getText().toString()))){
            Errors = "gateway ssid required";
            binding.gatewaySsid.setError(Errors);
            return new VerificationError(Errors);
        }
        if((TextUtils.isEmpty(binding.gatewayPassword.getText().toString()))){
            Errors = "gateway password required";
            binding.gatewayPassword.setError(Errors);
            return new VerificationError(Errors);
        }

        settings.gateway_ssid = binding.gatewaySsid.getText().toString();
        settings.gateway_password = binding.gatewayPassword.getText().toString();
        model.setSetting(settings);
        return new VerificationError(Errors);
    }
}
