package com.example.smarthome.ui.settings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.MessagesFragmentBinding;
import com.example.smarthome.models.node_logs;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MessagesFragment extends Fragment {

    private MessagesViewModel model;
    private List<formated_logs> mlist = new ArrayList<>();
    private MessagesFragmentBinding binding;
    private ApiInterface api;
    private Disposable disposable;
    public static MessagesFragment newInstance() {
        return new MessagesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
       // return inflater.inflate(R.layout.messages_fragment, container, false);
        binding = MessagesFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.list.setLayoutManager(new LinearLayoutManager(this.getContext()));
        binding.list.setAdapter(new GroupAdapter(mlist));
        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(MessagesViewModel.class);
        model.get().observe(this, data -> {
            mlist.clear();
            mlist.addAll(consolidate(group(data)));
            binding.list.getAdapter().notifyDataSetChanged();
        });
        getlogs();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.VISIBLE);
    }

    private HashMap<String, List<node_logs>> group(List<node_logs> list) {

        HashMap<String, List<node_logs>> groupedHashMap = new HashMap<>();
        for(node_logs item : list) {
            String hashMapKey = format(item.createdAt,"E, dd MMM yyyy");
            if(groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey).add(item);
            } else {
                List<node_logs> temp = new ArrayList<>();
                temp.add(item);
                groupedHashMap.put(hashMapKey, temp);
            }
        }
        return groupedHashMap;
    }

    private String format(String in, String format){
        String out =" Today";
        try {
            SimpleDateFormat in_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
            SimpleDateFormat out_format = new SimpleDateFormat(format);
            Date date = in_format.parse(in);
            out = out_format.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return out;
    }

    private class formated_logs{
        String date;
        List<node_logs> data;

        public formated_logs(String date, List<node_logs> data) {
            this.date = date;
            this.data = data;
        }
    }

    private List<formated_logs> consolidate(HashMap<String, List<node_logs>> data){
        List<formated_logs> consolidatedList = new ArrayList<>();

        for (String date : data.keySet())
            consolidatedList.add(new formated_logs(date, data.get(date) ));
        return consolidatedList;
    }

    private class GHViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final RecyclerView mRecyclerView;
        final View mView;

        GHViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_node_list, parent, false));
            mView = itemView;
            name = (TextView)itemView.findViewById(R.id.node_tittle);
            mRecyclerView = (RecyclerView)itemView.findViewById(R.id.node_device_list);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }
    }

    private class GroupAdapter extends RecyclerView.Adapter<MessagesFragment.GHViewHolder> {

        private final List<formated_logs> list;
        private LogsAdapter logsAdapter;
        private RecyclerView.RecycledViewPool recycledViewPool;

        GroupAdapter(List<formated_logs> data) {
            list = data;
            recycledViewPool = new RecyclerView.RecycledViewPool();
        }

        @NonNull
        @Override
        public GHViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new GHViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(GHViewHolder holder, int position) {
            holder.name.setText(list.get(position).date);
            logsAdapter = new LogsAdapter(list.get(position).data);
            holder.mRecyclerView.setAdapter(logsAdapter);
            holder.mRecyclerView.setRecycledViewPool(recycledViewPool);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    private class LogsViewHolder extends RecyclerView.ViewHolder {

        final TextView tittle;
        final TextView date;
        final View mView;
        final TextView details;
        node_logs item;

        LogsViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_logs_list, parent, false));
            mView = itemView;
            tittle = (TextView)itemView.findViewById(R.id.notify_device);
            date = (TextView)itemView.findViewById(R.id.notify_time);
            details = (TextView)itemView.findViewById(R.id.notify_log);
        }
    }

    private class LogsAdapter extends RecyclerView.Adapter<LogsViewHolder> {

        private final List<node_logs> list;

        LogsAdapter(List<node_logs> data) {
            list = data;
        }

        @NonNull
        @Override
        public LogsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new LogsViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(LogsViewHolder holder, int position) {
            holder.item = list.get(position);
            holder.tittle.setText(holder.item.node.node_type.tittle);
            holder.date.setText(format(holder.item.createdAt,"hh:mm:ss aa"));
            holder.details.setText(holder.item.data);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    void getlogs(){
        //update account id
        int account_id = 1;

        api.node_logs_get_list(account_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe( new Observer<response._node_logslist>(){

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(response._node_logslist response) {
                            if(response.success) {
                                model.set(response.data);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
    }

}
