package com.example.smarthome.ui.home.rooms;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smarthome.R;
import com.example.smarthome.api.ApiClient;
import com.example.smarthome.api.ApiInterface;
import com.example.smarthome.api.response;
import com.example.smarthome.databinding.FragmentRoomsAddBinding;
import com.example.smarthome.models.room_device;
import com.example.smarthome.ui.home.HomeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public class RoomsAddFragment extends Fragment{

    private FragmentRoomsAddBinding binding;
    private com.example.smarthome.models.room room = new com.example.smarthome.models.room();
    private List<room_device> list = new ArrayList<>();
    private HomeViewModel model;
    private static int mColumnCount = 2;
    private ApiInterface api;
    private Disposable disposable;
    public RoomsAddFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RoomsAddFragment newInstance() {
        return new RoomsAddFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
        api = ApiClient.getInstance(this.getContext()).create(ApiInterface.class);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getRoomSelected().observe(this, data ->{

            room = data;
            binding.txtRoomName.setText(room.name);
            if(!TextUtils.isEmpty(room.image)) {
                String[] image =  room.image.split("\\|");
                for (int i = 0; i < binding.roomIcon.getChildCount(); i++) {
                    Chip chip = (Chip) binding.roomIcon.getChildAt(i);
                    if (chip.getId() == Integer.parseInt(image[0])) {
                        chip.setChecked(true);
                        break;
                    }


                }
            }

                list.clear();
                if(data.room_devices != null)
                    list.addAll(data.room_devices);
                binding.deviceList.getAdapter().notifyDataSetChanged();

        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.fragment_rooms_add, container, false);
        binding = FragmentRoomsAddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        if (mColumnCount <= 1) {
            binding.deviceList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        } else {
            binding.deviceList.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        }
         binding.deviceList.setAdapter(new DeviceAdapter(list, this.getContext(), new OnItemInteractionListener(){

            @Override
            public void onRemoveClicked(room_device item) {

            }
        }));

        return  root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((BottomNavigationView)getActivity().findViewById(R.id.nav_view)).setVisibility(View.GONE);
    }



    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView status;
        final View mView;
        final ImageView icon;
        room_device item;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.device_list_item_view, parent, false));
            mView = itemView;
            name = (TextView)itemView.findViewById(R.id.device_name);
            status = (TextView)itemView.findViewById(R.id.device_status);
            icon = (ImageView)itemView.findViewById(R.id.device_icon);
        }
    }

    private class DeviceAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<room_device> list;
        private final Context context;
        private final OnItemInteractionListener listener;

        DeviceAdapter(List<room_device> data, Context cxt, OnItemInteractionListener listener1) {
            list = data;
            context = cxt;
            listener = listener1;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.item =  list.get(position);
            holder.name.setText(holder.item.name);
            holder.status.setText(holder.item.device.status);
//            int resID = context.getResources().getIdentifier("@drawable/"+ holder.item.device.image , "drawable", context.getPackageName());
//            holder.icon.setImageResource(resID);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != listener) {
                        listener.onRemoveClicked(holder.item);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    private interface OnItemInteractionListener {
        void onRemoveClicked(room_device item);
    }

    private void save(){
        String room_icon = "";
        if(TextUtils.isEmpty(binding.txtRoomName.getText().toString())){
            binding.txtRoomName.setError("*");
            return;
        }

        room_icon = String.valueOf(binding.roomIcon.getCheckedChipId());
        for(int i =0; i< binding.roomIcon.getChildCount();i++){
            Chip chip =  (Chip)binding.roomIcon.getChildAt(i);
            if(chip.isChecked()) {
                room_icon += "|" + chip.getText().toString();
                break;
            }
        }
        if(TextUtils.isEmpty(room_icon)){
            binding.txtRoomName.setError("Icon not set");
            return;
        }

        room.account_id = 1;
        room.name = binding.txtRoomName.getText().toString();
        room.image = room_icon;

        if(room.room_id == 0)
            create_room();
        else
            update_room();

        if(room.room_devices != null)
        if(room.room_devices.size()> 0)
        for(room_device device : room.room_devices) {
            if(device.room_device_id == 0)
                create_room_device(device);
            else
                update_room_device(device);
        }



    }

    void create_room(){
        api.room_post(room)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._room>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._room data) {
                        if(data.success == true)
                            room.room_id = data.data.room_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void update_room(){
        api.room_put(room.room_id, room)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( new Observer<response._room>(){

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(response._room data) {
                        if(data.success == true)
                            room.room_id = data.data.room_id;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void create_room_device(room_device device){

            api.room_device_post(device)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<response._room_device>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable = d;
                        }

                        @Override
                        public void onNext(response._room_device data) {
//                            if (data.success == true)
//                                room.room_id = data.data.room_id;
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
    }

    void update_room_device(room_device device){
            api.room_device_put(device.room_device_id, device)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<response._room_device>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable = d;
                        }

                        @Override
                        public void onNext(response._room_device data) {
//                            if (data.success == true)
//                                room.room_id = data.data.room_id;
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
    }
}
