package com.example.smarthome.ui.access.registration;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.smarthome.models.*;

public class RegistrationViewModel extends ViewModel {
    private final MutableLiveData<account> account = new MutableLiveData<account>();
    private final MutableLiveData<account_setting> settings = new MutableLiveData<account_setting>();

    public void setAccount(account item) {
        account.setValue(item);
    }
    public LiveData<account> getAccount() {
        return account;
    }

    public void setSetting(account_setting item) {
        settings.setValue(item);
    }
    public LiveData<account_setting> getSetting() {
        return settings;
    }
}
