package com.example.smarthome.helpers;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import com.example.smarthome.models.account;
import com.example.smarthome.models.account_setting;

import java.lang.reflect.Method;


public class Account {

    private Context _Context;
    private com.example.smarthome.models.account account;
    private PrefManager PrefManager;

    public com.example.smarthome.models.account getAccount() {
        return account;
    }

    public Account(Context cont) {
        _Context = cont;

        PrefManager = new PrefManager(this._Context);
        account = new com.example.smarthome.models.account();
        this.account.account_id = PrefManager.getInt(Config.account_id);

        try {
            String[] name = PrefManager.getString(Config.account_name).split(" ");
            String surname = "", firstname = "";
            if (name.length > 0)
                surname = name[0];
            if (name.length > 1)
                firstname = name[1];
            this.account.surname = surname;
            this.account.firstname = firstname;
        }catch (Exception e){}

        this.account.email = PrefManager.getString(Config.account_email);
        this.account.phone = PrefManager.getString(Config.account_phone);
        this.account.address = PrefManager.getString(Config.account_address);
        this.account.account_setting = new account_setting();
        this.account.account_setting.setting_id = PrefManager.getInt(Config.setting_id);
        this.account.account_setting.account_code = PrefManager.getString(Config.account_code);
        this.account.account_setting.root_address = PrefManager.getString(Config.root_address);
        this.account.account_setting.mesh_ssid = PrefManager.getString(Config.mesh_ssid);
        this.account.account_setting.mesh_password = PrefManager.getString(Config.mesh_password);
        this.account.account_setting.mesh_port = PrefManager.getInt(Config.mesh_port);
        this.account.account_setting.mesh_channel = PrefManager.getInt(Config.mesh_channel);
        this.account.account_setting.mesh_hidden = PrefManager.getInt(Config.mesh_hidden);
        this.account.account_setting.mesh_connections = PrefManager.getInt(Config.mesh_connections);
        this.account.account_setting.gateway_ssid = PrefManager.getString(Config.gateway_ssid);
        this.account.account_setting.gateway_password = PrefManager.getString(Config.gateway_password);
        this.account.account_setting.host_address = PrefManager.getString(Config.host_address);
        this.account.account_setting.host_port = PrefManager.getInt(Config.host_port);
    }


    private String Read() {

        String serialNumber = "";
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);

            serialNumber = (String) get.invoke(c, "gsm.sn1");

            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ril.serialnumber");

            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ro.serialno");

            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "sys.serialnumber");

            if (serialNumber.equals(""))
                serialNumber = Build.SERIAL;

            // If none of the methods above worked
            if (serialNumber.equals(""))
                serialNumber = Settings.Secure.getString(_Context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (serialNumber.equals(""))
                serialNumber = "FAILED";

//                if (ActivityCompat.checkSelfPermission(_Context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//
//                    this.terminal.DEVIMEI = ((TelephonyManager) _Context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
//                    this.terminal.PHONENO = ((TelephonyManager) _Context.getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();
//                    this.terminal.DEVICCID = ((TelephonyManager) _Context.getSystemService(Context.TELEPHONY_SERVICE)).getSubscriberId();
//                }else{
//                    this.terminal.PHONENO = this.terminal.DEVIMEI = this.terminal.DEVICCID = "DENIED";
//                }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return serialNumber;
    }

    public void Save( com.example.smarthome.models.account account){

        this.account = account;
        PrefManager.put(Config.account_id, this.account.account_id);
        PrefManager.put(Config.account_name, this.account.surname + " " + this.account.firstname);
        PrefManager.put(Config.account_email, this.account.email);
        PrefManager.put(Config.account_phone, this.account.phone);
        PrefManager.put(Config.account_address, this.account.address);
        PrefManager.put(Config.setting_id, this.account.account_setting.setting_id);
        PrefManager.put(Config.account_code, this.account.account_setting.account_code);
        PrefManager.put(Config.root_address, this.account.account_setting.root_address);
        PrefManager.put(Config.mesh_ssid, this.account.account_setting.mesh_ssid);
        PrefManager.put(Config.mesh_password, this.account.account_setting.mesh_password);
        PrefManager.put(Config.mesh_port, this.account.account_setting.mesh_port);
        PrefManager.put(Config.mesh_channel, this.account.account_setting.mesh_channel);
        PrefManager.put(Config.mesh_hidden, this.account.account_setting.mesh_hidden);
        PrefManager.put(Config.mesh_connections, this.account.account_setting.mesh_connections);
        PrefManager.put(Config.gateway_ssid, this.account.account_setting.gateway_ssid);
        PrefManager.put(Config.gateway_password, this.account.account_setting.gateway_password);
        PrefManager.put(Config.host_address, this.account.account_setting.host_address);
        PrefManager.put(Config.host_port, this.account.account_setting.host_port);
    }

}
