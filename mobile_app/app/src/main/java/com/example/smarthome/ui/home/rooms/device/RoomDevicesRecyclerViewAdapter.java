package com.example.smarthome.ui.home.rooms.device;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smarthome.R;
import com.example.smarthome.models.device;
import com.example.smarthome.models.room_device;

import java.util.List;


public class RoomDevicesRecyclerViewAdapter extends RecyclerView.Adapter<RoomDevicesRecyclerViewAdapter.ViewHolder> {

    private final List<room_device> mValues;
    private final OnRoomDeviceInteractionListener mListener;
    private  final Context context;
    private static View viewOld=null;

    public RoomDevicesRecyclerViewAdapter(List<room_device> items, Context cont, OnRoomDeviceInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = cont;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_room_device, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTitleView.setText(mValues.get(position).name);
//        int resID = context.getResources().getIdentifier("@drawable/"+ mValues.get(position).device.image , "drawable", context.getPackageName());
//        holder.mImageView.setImageResource(resID);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onRoomDeviceClicked(holder.mItem);
//                    holder.mView.setBackgroundColor(context.getResources().getColor(R.color.grey));
//                    if(viewOld!=null)
//                        viewOld.setBackgroundColor(context.getResources().getColor(R.color.transparent));
//                    viewOld=holder.mView;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final ImageView mImageView;
        public room_device mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.room_device_title);
            mImageView = (ImageView) view.findViewById(R.id.room_device_image);
        }

    }

    public interface OnRoomDeviceInteractionListener {
        // TODO: Update argument type and name
        void onRoomDeviceClicked(room_device item);
    }
}
