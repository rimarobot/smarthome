package com.example.smarthome.ui.home.rooms.device;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smarthome.R;
import com.example.smarthome.databinding.ViewWeatherSensorBinding;
import com.example.smarthome.ui.home.HomeViewModel;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WeatherSensorView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeatherSensorView extends Fragment {

    private HomeViewModel model;
    private ViewWeatherSensorBinding binding;
    private com.example.smarthome.models.device mDevice;
    private OnStatusReceiver onStatusReceiver = new OnStatusReceiver();
    public class OnStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent){
            /*
             {    room:room, T: node type,
                  N:node id, V:voltage, X:memory,
                  Y:rom, F:software version, H:hardware version,
                  P:processor, E:temperature, D:data
              }
             */
            String data = intent.getExtras().getString("data");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject object = new JSONObject(data);
//                        binding.nodeVoltage.setText(object.getString("V"));
//                        binding.nodeTemp.setText(object.getString("E"));
//                        binding.nodeRam.setText(object.getString("X"));
//                        binding.nodeStatus.setText("ON");
//                        binding.nodeId.setText(object.getString("N"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    public WeatherSensorView() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static WeatherSensorView newInstance( ) {
        return new WeatherSensorView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(this.getActivity()).get(HomeViewModel.class);
        model.getDeviceSelected().observe(this, data ->{
            mDevice = data;
            binding.deviceTitle.setText(mDevice.name);
            binding.deviceRoom.setText(((AppCompatActivity) getActivity()).getSupportActionBar().getTitle());
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.view_weather_sensor, container, false);
        binding = ViewWeatherSensorBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
         graph_init();
         graph_data();
        return root;
    }

    void graph_init(){
        binding.deviceChart.getDescription().setEnabled(false);
        // enable touch gestures
        binding.deviceChart.setTouchEnabled(false);

        binding.deviceChart.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        binding.deviceChart.setDragEnabled(false);
        binding.deviceChart.setScaleEnabled(false);
        binding.deviceChart.setDrawGridBackground(false);
        binding.deviceChart.setHighlightPerDragEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        binding.deviceChart.setPinchZoom(false);

        // set an alternative background color
//        binding.deviceChart.setBackgroundColor(Color.BLACK);

        binding.deviceChart.animateX(150);

        // get the legend (only possible after setting data)
        Legend l = binding.deviceChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(12f);
        l.setTextColor(this.getResources().getColor(R.color.colorPrimary));
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = binding.deviceChart.getXAxis();
        xAxis.setTextSize(12f);
        xAxis.setTextColor(this.getResources().getColor(R.color.colorPrimary));

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(-60);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setLabelCount(5, true);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(7f);
        xAxis.setValueFormatter(new ValueFormatter() {

            private final SimpleDateFormat mFormat = new SimpleDateFormat("dd-MMM HH:mm", Locale.ENGLISH);

            @Override
            public String getFormattedValue(float value) {

                long millis = TimeUnit.HOURS.toMillis((long) value);
                return mFormat.format(new Date(millis));
            }
        });


        YAxis leftAxis = binding.deviceChart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMaximum(100);
        leftAxis.setAxisMinimum(0);
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setGranularityEnabled(false);

        YAxis rightAxis = binding.deviceChart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMaximum(100);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawZeroLine(false);
        rightAxis.setGranularityEnabled(false);
    }

    void graph_data(){
        int count = 15; float range= 3f;
        ArrayList<Entry> values1 = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            float val = (float) (Math.random() * (range / 2f)) + 50;
            values1.add(new Entry(i, val));
        }

        ArrayList<Entry> values2 = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            float val = (float) (Math.random() * range) + 10;
            values2.add(new Entry(i, val));
        }


        LineDataSet set1, set2;

        if (binding.deviceChart.getData() != null &&
                binding.deviceChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) binding.deviceChart.getData().getDataSetByIndex(0);
            set2 = (LineDataSet) binding.deviceChart.getData().getDataSetByIndex(1);
            set1.setValues(values1);
            set2.setValues(values2);
            binding.deviceChart.getData().notifyDataChanged();
            binding.deviceChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values1, "Temp");

            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setLineWidth(1.8f);
            set1.setFillAlpha(65);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setDrawFilled(true);
            set1.setDrawCircles(false);
            set1.setDrawValues(false);

            set2 = new LineDataSet(values2, "Humidity");
            set2.setAxisDependency(YAxis.AxisDependency.RIGHT);
            set2.setColor(Color.RED);
            set2.setLineWidth(1.8f);
            set2.setFillColor(Color.RED);
            set2.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set2.setDrawFilled(true);
            set2.setDrawCircles(false);
            set2.setDrawValues(false);
            LineData data = new LineData(set1, set2);

            // set data
            binding.deviceChart.setData(data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(onStatusReceiver, new IntentFilter("onStatus"));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(onStatusReceiver);
    }
}
