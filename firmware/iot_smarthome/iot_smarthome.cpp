// Do not remove the include below
#include "iot_smarthome.h"
#include "libs/app.h"
#include "libs/nodes/node.h"

Scheduler scheduler; // to control your personal task
ESP8266WebServer server(80);

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
	SocketIOclient socketIO;
	app mesh(&server, &socketIO);
#else
	app mesh(&server);
#endif



void setup() {
#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER || ACTIVE_DEVICE == EIGHT_RELAY_CONTROLLER)
	Serial.begin(19200);
	Serial.println("Started...");
#endif

	mesh.boot(&scheduler);
	mesh.onPreInitializeHandler(&onPreInitialize);
	mesh.onPostInitializeHandler(&onPostInitialize);
	mesh.onSensorHandler(&onSensor);
}

// The loop function is called in an endless loop
void loop() {
	// The loop function is called in an endless loop
	mesh.run();
}


