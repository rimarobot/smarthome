/*
 * config.h
 *
 *  Created on: 23 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_CONFIG_H_
#define LIBS_CONFIG_H_

/*
 * NODE TYPE
 */
#define CENTRAL_HUB_CONTROLLER             (1)
#define LIGHT_RELAY_CONTROLLER             (2)
#define FAN_RELAY_CONTROLLER               (3)
#define LIGHT_SENSOR_CONTROLLER            (4)
#define TEMP_HUMIDITY_CONTROLLER           (5)
#define ONE_RELAY_CONTROLLER               (6)
#define TWO_RELAY_CONTROLLER               (7)
#define FOUR_RELAY_CONTROLLER              (8)
#define EIGHT_RELAY_CONTROLLER             (9)
#define WINDOW_SERVO_CONTROLLER            (10)
#define ACCESS_CONTROL_CONTROLLER          (11)
#define MOTION_SENSOR_CONTROLLER           (12)
#define MOTION_RELAY_CONTROLLER            (13)

#define ACTIVE_DEVICE   CENTRAL_HUB_CONTROLLER

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER) || (ACTIVE_DEVICE == EIGHT_RELAY_CONTROLLER) || (ACTIVE_DEVICE == MOTION_RELAY_CONTROLLER)|| (ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
#define ACTIVE_DEVICE_PROCESSOR "ESP8266"
#else
#define ACTIVE_DEVICE_PROCESSOR "ESP-01"
#endif

#define ACTIVE_HARDWARE_VERSION "V0.2"
#define ACTIVE_SOFTWARE_VERSION "V0.4"

/*
 * PROTOCOL MESSAGES
 */

#define NODE_TYPE                    ('T')
#define DATA                         ('D')
#define TEMPERATURE                  ('E')
#define HARDWARE_VERSION             ('H')
#define SOFTWARE_VERSION             ('F')
#define ROM_SPACE                    ('Y')
#define RAM_SPACE                    ('X')
#define VOLTAGE                      ('V')
#define NODE_ID                      ('N')
#define COMMAND_TYPE                 ('C')
#define DATA_DIRECTION               ('M')
#define USER_ID                       'I'
#define USER_STATUS                  ('S')
#define DEVICE_ADDRESS               ('K')
#define DEVICE_PROCESSOR             ('P')


#define UI_TIMEOUT     (1000l)
#define DOOR_TIMEOUT     (10)

enum COMMANDS {
	UNUSED,
	ROOT_BROADCAST,
	NODE_HEARTBIT,
	SWITCH_RELAY,
	DRIVE_MOTOR,
	REGISTER_FINGER_REQUEST,
	REGISTER_FINGER_RESPONSE,
	REGISTER_CARD_REQUEST,
	REGISTER_CARD_RESPONSE,
	VERIFY_FINGER_REQUEST,
	VERIFY_FINGER_RESPONSE,
	VERIFY_CARD_REQUEST,
	VERIFY_CARD_RESPONSE
};

typedef struct {
		String mesh_room;
		String mesh_ssid;
		String mesh_password;
		uint16_t mesh_port;
		uint8_t mesh_max_con;
		uint8_t mesh_channel;
		uint8_t mesh_hidden;
		String gateway_ssid;
		String gateway_password;
		String server_host;
		uint16_t server_port;
		String server_username;
		String server_password;
} config_t;

typedef struct {
		uint8_t pin;
		uint8_t status;
} relay_t, sensor_t;

typedef struct {
		uint8_t pin;
		uint16_t position;
} servo_t;

typedef std::function<void()> defaultCallback_t;
typedef std::function<void(uint8_t &index, uint8_t &status)> relayCallback_t;
typedef std::function<void(uint8_t &index, uint16_t &status)> motorCallback_t;
typedef std::function<void(uint32_t &from, uint8_t &command, String &data, uint8_t &status)> BiometricVerifyCallback_t;
typedef std::function<void(uint32_t &from, uint8_t &command, String &data, uint32_t &userid, uint16_t &fingerid)> BiometricRegisterCallback_t;

#endif /* LIBS_CONFIG_H_ */
