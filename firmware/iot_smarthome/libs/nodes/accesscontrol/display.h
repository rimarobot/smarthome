/*
 * display.h
 *
 *  Created on: 1 May 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_ACCESSCONTROL_DISPLAY_H_
#define LIBS_NODES_ACCESSCONTROL_DISPLAY_H_
#if(ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
extern TFT_eSPI tft;  // Invoke library, pins defined in User_Setup.h
void _display_window();

#define OFFSET_HEIGHT 40
#define OFFSET_WIDTH 0

inline void _display_init() {
	tft.init();
	tft.setRotation(1);
	tft.fillScreen(TFT_YELLOW);
	_display_window();
}

inline void _display_home() {

}

void _display_window() {
	tft.fillRect(OFFSET_WIDTH, OFFSET_HEIGHT, tft.width() - (OFFSET_WIDTH / 2), tft.height() - (OFFSET_HEIGHT / 2), TFT_WHITE);
}

void _display_tittle(String channel) {  //, String * tittle){

}

void _display_name(String name) {  //, String * tittle){

}

void _display_status(String status) {  //, String * tittle){

}

void _display_progress(char progress) {  //, String * tittle){

}

#endif
#endif /* LIBS_NODES_ACCESSCONTROL_DISPLAY_H_ */
