/*
 * rfid_reader.h
 *
 *  Created on: 1 May 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_ACCESSCONTROL_RFID_READER_H_
#define LIBS_NODES_ACCESSCONTROL_RFID_READER_H_

#if(ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)

extern MFRC522 rfid;
extern app mesh;
extern TFT_eSPI tft;
extern unsigned long nextTimeout;
extern Scheduler scheduler;



MFRC522::MIFARE_Key old_key_A = { keyByte: { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } };
MFRC522::MIFARE_Key old_key_B = { keyByte: { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } };

MFRC522::MIFARE_Key new_key_A = { keyByte: { 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5 } };
MFRC522::MIFARE_Key new_key_B = { keyByte: { 0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xB5 } };

String card = "";
uint32_t user_id = 0;
void vDoorLock();
void onVerifyCard();
bool _set_keys(MFRC522::MIFARE_Key* oldKeyA, MFRC522::MIFARE_Key* oldKeyB, MFRC522::MIFARE_Key* newKeyA, MFRC522::MIFARE_Key* newKeyB, int sector);
Task doorlockTask(TASK_SECOND * 1, TASK_FOREVER, &vDoorLock);

inline void _rfidreader_init() {
	rfid.PCD_Init();
	scheduler.addTask(doorlockTask);
	doorlockTask.disable();
}

bool _set_keys(MFRC522::MIFARE_Key* oldKeyA, MFRC522::MIFARE_Key* oldKeyB, MFRC522::MIFARE_Key* newKeyA, MFRC522::MIFARE_Key* newKeyB, int sector) {

	MFRC522::StatusCode status;
	byte trailerBlock = sector * 4 + 3;
	byte buffer[18];
	byte size = sizeof(buffer);

	//Authenticate using key A
	status = (MFRC522::StatusCode) rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, oldKeyA, &(rfid.uid));
	if (status != MFRC522::STATUS_OK)
		return false;

	status = (MFRC522::StatusCode) rfid.MIFARE_Read(trailerBlock, buffer, &size);
	if (status != MFRC522::STATUS_OK)
		return false;

	//Authenticate using key B
	status = (MFRC522::StatusCode) rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, oldKeyB, &(rfid.uid));
	if (status != MFRC522::STATUS_OK)
		return false;

	if (newKeyA != nullptr || newKeyB != nullptr) {
		for (byte i = 0; i < MFRC522::MF_KEY_SIZE; i++) {
			if (newKeyA != nullptr) {
				buffer[i] = newKeyA->keyByte[i];
			}
			if (newKeyB != nullptr) {
				buffer[i + 10] = newKeyB->keyByte[i];
			}
		}
	}

	status = (MFRC522::StatusCode) rfid.MIFARE_Write(trailerBlock, buffer, 16);
	if (status != MFRC522::STATUS_OK)
		return false;

//	status = (MFRC522::StatusCode) rfid.MIFARE_Read(trailerBlock, buffer, &size);
//	if (status != MFRC522::STATUS_OK)
//		return false;
	// Halt PICC
	rfid.PICC_HaltA();
	// Stop encryption on PCD
	rfid.PCD_StopCrypto1();
	return true;
}

bool card_detected() {
	if (!rfid.PICC_IsNewCardPresent())
		return false;
	if (!rfid.PICC_ReadCardSerial())
		return false;

	card = "";
	for (auto i = 0; i < 4; i++)
		card.concat(String(rfid.uid.uidByte[i], HEX));

	card.toUpperCase();
	return true;
}

void onVerifyCard() {

	card = "";

	if (card_detected() == true) {

		MFRC522::StatusCode status;
		byte buffer[18];
		byte size = sizeof(buffer);
		String _name = "", userId = "", error = "VERIFICATION FAILED";

		_display_tittle("CARD VERIFICATION");	//, &_name);

		nextTimeout = millis() + UI_TIMEOUT;

		//Authenticate using key A
		status = (MFRC522::StatusCode) rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, (15 * 4 + 3), &new_key_A, &(rfid.uid));
		if (status != MFRC522::STATUS_OK) {
			_display_tittle(error);
			return;
		}

		//read name from card
		status = (MFRC522::StatusCode) rfid.MIFARE_Read((15 * 4 + 2), buffer, &size);
		if (status != MFRC522::STATUS_OK) {
			_display_tittle(error);
			return;
		}
		_name = String((char *) buffer);
		memset(buffer, '\0', sizeof((char *) buffer));

		status = (MFRC522::StatusCode) rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, (15 * 4 + 3), &new_key_A, &(rfid.uid));
		if (status != MFRC522::STATUS_OK) {
			_display_tittle(error);
			return;
		}

		// read userid from card
		status = (MFRC522::StatusCode) rfid.MIFARE_Read((15 * 4 + 1), buffer, &size);
		if (status != MFRC522::STATUS_OK) {
			_display_tittle(error);
			return;
		}
		userId = String((char *) buffer);
		;
		user_id = userId.toInt();
		/*
		 * All Nodes should send activity/status data to the root node for server reporting
		 * Data Structure:
		 * T:''#C:''#D:''#
		 */

		char str[200];
		snprintf(str, 200, "%c:%d#%c:%d#%c:%s#", NODE_TYPE, ACTIVE_DEVICE, COMMAND_TYPE, VERIFY_CARD_REQUEST, DATA, card.c_str());
		mesh.sendSingle(mesh.getRootNodeId(), String(str));
		_display_tittle("VERIFICATION WAITING...");

		// Halt PICC
		rfid.PICC_HaltA();
		// Stop encryption on PCD
		rfid.PCD_StopCrypto1();
	}

}

void _register_card(String &data, uint32_t &userid) {
	char progress[] = "-|/\\n";
	uint8_t cnt = 0;
	nextTimeout = millis() + UI_TIMEOUT;
	bool success = false;
	card = "";

	do {

		if (card_detected()) {
			if (_set_keys(&old_key_A, &old_key_B, &new_key_A, &new_key_B, 15)) {
				MFRC522::StatusCode status;
				byte buffer[18];
				byte size;
				data.toCharArray((char *) buffer, 15);
				size = strlen((char *) buffer);
				for (byte i = size; i < 16; i++)
					buffer[i] = ' ';

				//Authenticate using key A
				status = (MFRC522::StatusCode) rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, (15 * 4 + 3), &new_key_A, &(rfid.uid));
				if (status != MFRC522::STATUS_OK) {
					break;
				}
				status = rfid.MIFARE_Write((15 * 4 + 2), buffer, 16);
				if (status != MFRC522::STATUS_OK) {
					break;
				}

				memset(buffer, '\0', sizeof((char *) buffer));
				String(userid).toCharArray((char *) buffer, 15);
				size = strlen((char *) buffer);
				for (byte i = size; i < 16; i++)
					buffer[i] = ' ';

				//Authenticate using key A
				status = (MFRC522::StatusCode) rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, (15 * 4 + 3), &new_key_A, &(rfid.uid));
				if (status != MFRC522::STATUS_OK) {
					break;
				}

				status = rfid.MIFARE_Write((15 * 4 + 1), buffer, 16);
				if (status != MFRC522::STATUS_OK) {
					break;
				}
				// Halt PICC
				rfid.PICC_HaltA();
				// Stop encryption on PCD
				rfid.PCD_StopCrypto1();
				success = true;
				break;
			}

		} else {
			_display_progress(progress[cnt++]);
			if (cnt > 4)
				cnt = 0;
		}

	} while (millis() < nextTimeout);

	char str[200];
	snprintf(str, 200, "%c:%d#%c:%d#%c:%s#%c:%s#%c:%d#", NODE_TYPE, ACTIVE_DEVICE, COMMAND_TYPE, REGISTER_CARD_RESPONSE, DATA, card.c_str(), USER_ID, String(userid).c_str(), USER_STATUS,
	        success ? 1 : 0);
	mesh.sendSingle(mesh.getRootNodeId(), String(str));

}

void vDoorLock() {

	for (auto i = 0; i < RELAY_ARRAY; i++)
		digitalWrite(relays[i].pin, LOW);
	doorlockTask.disable();
}
#endif
#endif /* LIBS_NODES_ACCESSCONTROL_RFID_READER_H_ */
