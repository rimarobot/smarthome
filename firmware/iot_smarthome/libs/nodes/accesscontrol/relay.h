/*
 * c
 *
 *  Created on: 1 May 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_ACCESSCONTROL_RELAY_H_
#define LIBS_NODES_ACCESSCONTROL_RELAY_H_
#if(ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
extern app mesh;

/*
 *
 * Door sensor
 * sensor pin (D2)
 *
 *
 * Relay
 * relay pin (D1)
 *
 */

#define RELAY_ARRAY (1)
relay_t relays[RELAY_ARRAY] = { { pin:D1, status:LOW } };

#define SENSOR_ARRAY (1)
sensor_t sensors[SENSOR_ARRAY] = { { pin:D2, status:0 } };

inline void _sensor_init() {

	for (auto i = 0; i < RELAY_ARRAY; i++) {
		pinMode(relays[i].pin, OUTPUT);
		digitalWrite(relays[i].pin, relays[i].status);
	}
	for (auto i = 0; i < SENSOR_ARRAY; i++)
		pinMode(sensors[i].pin, INPUT);

	mesh.onRelayEventListener([](uint8_t &index, uint8_t &status) {

		digitalWrite(relays[index].pin, status);
		relays[index].status = digitalRead(relays[index].pin);

		String data = "";

		data +="R";
		for (auto i = 0;i < RELAY_ARRAY; i++)
		data += digitalRead(relays[i].pin);
		data +="S";
		for (auto i = 0;i < SENSOR_ARRAY; i++)
		data += digitalRead(sensors[i].pin);
		mesh.setSensorData(data);
	});
}

inline void _sensor_onsensor() {
	String data = "";
	data += "R";
	for (auto i = 0; i < RELAY_ARRAY; i++)
		data += digitalRead(relays[i].pin);
	data += "S";
	for (auto i = 0; i < SENSOR_ARRAY; i++)
		data += digitalRead(sensors[i].pin);

	mesh.setSensorData(data);
	mesh.setSensorDelay(0);
}

inline void _sensor_relay() {
	for (auto i = 0; i < RELAY_ARRAY; i++) {
		digitalWrite(relays[i].pin, HIGH);
		relays[i].status = digitalRead(relays[i].pin);
	}
}

#endif
#endif /* LIBS_NODES_ACCESSCONTROL_RELAY_H_ */
