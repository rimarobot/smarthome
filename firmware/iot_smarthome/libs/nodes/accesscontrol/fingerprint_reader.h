/*
 * fingerprint_reader.h
 *
 *  Created on: 1 May 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_ACCESSCONTROL_FINGERPRINT_READER_H_
#define LIBS_NODES_ACCESSCONTROL_FINGERPRINT_READER_H_
#if(ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)

extern app mesh;
extern unsigned long nextTimeout;
extern Scheduler scheduler;
extern Adafruit_Fingerprint finger;

inline void _fingerprint_init() {
	finger.begin(57600);
	delay(5);
	finger.verifyPassword();
}

bool _fingerprint_detected() {
	return (finger.getImage() == FINGERPRINT_OK);
}

void onVerifyFinger() {
//	_fingerprint_init();
	if (_fingerprint_detected()) {
		String error = "VERIFICATION FAILED";
		_display_tittle("FINGER VERIFICATION");
		nextTimeout = millis() + UI_TIMEOUT;

		if (finger.image2Tz() != FINGERPRINT_OK) {
			_display_tittle(error);
			return;
		}

		if (finger.fingerFastSearch() != FINGERPRINT_OK) {
			_display_tittle(error);
			return;
		}

		char str[200];
		snprintf(str, 200, "%c:%d#%c:%d#%c:%d#", NODE_TYPE, ACTIVE_DEVICE, COMMAND_TYPE, VERIFY_FINGER_REQUEST, DATA, finger.fingerID);
		mesh.sendSingle(mesh.getRootNodeId(), String(str));
		_display_tittle("VERIFICATION WAITING...");

	}

}

void _register_fingerprint(String &data, uint32_t &userid, uint16_t &fingerid) {

	char progress[] = "-|/\\n";
	uint8_t cnt = 0;
	nextTimeout = millis() + UI_TIMEOUT;
	bool success = false;
	int p = 1;
	do {

		switch (p) {
			case 1: //stage one
			{
				if (_fingerprint_detected()) {
					if (finger.image2Tz(1) != FINGERPRINT_OK)
						break;
					_display_tittle("REMOVE FINGER");
					p = 2;
					nextTimeout = millis() + UI_TIMEOUT;
				}
				break;
			}
			case 2: {
				if (finger.getImage() == FINGERPRINT_NOFINGER) {
					_display_tittle("PLACE FINGER AGAIN");
					p = 3;
				}
				break;
			}
			case 3: {
				if (_fingerprint_detected()) {
					if (finger.image2Tz(2) != FINGERPRINT_OK)
						break;
					p = 4;
					nextTimeout = millis() + UI_TIMEOUT;
				}
				break;
			}
			case 4: {
				if (finger.createModel() == FINGERPRINT_OK) {
					if (finger.storeModel(fingerid) == FINGERPRINT_OK) {
						// finally we have reached the end
						success = true;
					}
				}
				break;
			}

		}

		if (success == false) {
			_display_progress(progress[cnt++]);
			if (cnt > 4)
				cnt = 0;
		} else {
			//finally we have reached the end lets exit and send status to server/source
			break;
		}

	} while (millis() < nextTimeout);

	char str[200];
	snprintf(str, 200, "%c:%d#%c:%d#%c:%s#%c:%s#%c:%d#", NODE_TYPE, ACTIVE_DEVICE, COMMAND_TYPE, REGISTER_FINGER_RESPONSE, DATA, String(fingerid).c_str(), USER_ID, String(userid).c_str(), USER_STATUS,
	        success ? 1 : 0);
	mesh.sendSingle(mesh.getRootNodeId(), String(str));

}

#endif
#endif /* LIBS_NODES_ACCESSCONTROL_FINGERPRINT_READER_H_ */
