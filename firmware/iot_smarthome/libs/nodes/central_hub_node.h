/*
 * central_hub_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_CENTRAL_HUB_NODE_H_
#define LIBS_NODES_CENTRAL_HUB_NODE_H_

#if(ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
extern app mesh;
/*
 *
 * Nodes:
 * 1. CENTRAL_HUB_CONTROLLER
 *
 * buzzer     pin (D3)
 *
 * DHT        pin (D1)
 *
 * TFT CS     pin (D8)
 * TFT DC     pin (D2)
 * TFT MOSI   pin (D7)
 * TFT SCK    pin (D5)
 * TFT MISO   pin (D6)
 *
 * program    pin (D0)
 *
*/
void onPreInitialize() {

}
void onPostInitialize() {

}

void onSensor() {

	mesh.setSensorDelay(0);
}

#endif
#endif /* LIBS_NODES_CENTRAL_HUB_NODE_H_ */
