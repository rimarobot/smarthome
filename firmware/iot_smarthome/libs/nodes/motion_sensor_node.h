/*
 * motion_sensor_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_MOTION_SENSOR_NODE_H_
#define LIBS_NODES_MOTION_SENSOR_NODE_H_

#if(ACTIVE_DEVICE == MOTION_SENSOR_CONTROLLER || ACTIVE_DEVICE == MOTION_RELAY_CONTROLLER)

extern app mesh;
extern Scheduler scheduler;

#if(ACTIVE_DEVICE == MOTION_SENSOR_CONTROLLER)
/*
 * Nodes:
 *
 * 1. MOTION_SENSOR_CONTROLLER
 * motion sensor pin (D2)
 * sound sensor pin  (D1)
 * light sensor pin  (D3)
 * program pin 		 (D0)
 *
 */
#define SENSOR_ARRAY (3)
sensor_t sensors[SENSOR_ARRAY] = { { pin:D2, status:0 }, { pin:D1, status:0 }, { pin:D3, status:0 } };
#endif

#if(ACTIVE_DEVICE == MOTION_RELAY_CONTROLLER)
/*
 * 2. MOTION_RELAY_CONTROLLER
 * motion sensor 1 pin (D1)
 * motion sensor 2 pin (D2)
 * motion sensor 3 pin (D3)
 * motion sensor 4 pin (D4)
 * relay pin           (D5)
 * program pin 		   (D0)
 */
#define SENSOR_ARRAY (4)
sensor_t sensors[SENSOR_ARRAY] = { { pin:D1, status:0 }, { pin:D2, status:0 }, { pin:D3, status:0 }, { pin:D4, status:0 } };
#define RELAY_ARRAY (1)
relay_t relays[RELAY_ARRAY] = { { pin:D5, status;LOW } };
void vSavePower();
Task powerSaveTask(TASK_SECOND * 1, TASK_FOREVER, &vSavePower);

void vSavePower() {

	for (auto i = 0; i < RELAY_ARRAY; i++)
		digitalWrite(relays[i].pin, LOW);
	powerSaveTask.disable();
}
#endif

void onPreInitialize() {
	for (auto i = 0; i < SENSOR_ARRAY; i++)
		pinMode(sensors[i].pin, INPUT);

#if(ACTIVE_DEVICE == MOTION_RELAY_CONTROLLER)

	for (auto i = 0; i < RELAY_ARRAY; i++) {
		pinMode(relays[i].pin, OUTPUT);
		digitalWrite(relays[i].pin, relays[i].status);
	}

	mesh.onRelayEventListener([](uint8_t &index, uint8_t &status) {

		digitalWrite(relays[index].pin, status);
		relays[index].status = digitalRead(relays[index].pin);

		String data = "";

		data +="R";
		for (auto i = 0;i < RELAY_ARRAY; i++)
		data += digitalRead(relays[i].pin);
		data +="S";
		for (auto i = 0;i < SENSOR_ARRAY; i++)
		data += digitalRead(sensors[i].pin);

		mesh.setSensorData(data);

		if(relays[index].status == HIGH)
		{
			powerSaveTask.setInterval(TASK_SECOND * 10);
			powerSaveTask.enable();
		}
	});

#endif
}
void onPostInitialize() {
#if(ACTIVE_DEVICE == MOTION_RELAY_CONTROLLER)
	scheduler.addTask(powerSaveTask);
	powerSaveTask.disable();
#endif
}

void onSensor() {
	String data = "";
#if(ACTIVE_DEVICE == MOTION_RELAY_CONTROLLER)
	data += "R";
	for (auto i = 0; i < RELAY_ARRAY; i++)
		data += digitalRead(relays[i].pin);
#endif
	data += "S";
	for (auto i = 0; i < SENSOR_ARRAY; i++)
		data += digitalRead(sensors[i].pin);

	mesh.setSensorData(data);
	mesh.setSensorDelay(0);
}

#endif

#endif /* LIBS_NODES_MOTION_SENSOR_NODE_H_ */
