/*
 * light_sensor_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_LIGHT_SENSOR_NODE_H_
#define LIBS_NODES_LIGHT_SENSOR_NODE_H_

#if(ACTIVE_DEVICE == LIGHT_SENSOR_CONTROLLER)
#include "Wire.h"
#include "Max44009.h"

extern app mesh;
Max44009 sensor(Max44009::Boolean::False);
/*
 * Nodes:
 * 1. LIGHT_SENSOR_CONTROLLER
 * SDA pin             (D1)
 * SCK pin             (D3)
 * program pin 		   (D0)
 */
#define SDA  (D1)
#define SCK  (D3)

void onPreInitialize() {
	Wire.begin(SDA, SCK);
	sensor.configure(MAX44009_DEFAULT_ADDRESS, &Wire);
}
void onPostInitialize() {

}

void onSensor() {
	if (sensor.getError() == MAX44009_OK) {
		String data = "";
		data += sensor.getLux();
		mesh.setSensorData(data);
	}
	mesh.setSensorDelay(0);
}

#endif
#endif /* LIBS_NODES_LIGHT_SENSOR_NODE_H_ */
