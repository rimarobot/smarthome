/*
 * window_servo_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_WINDOW_SERVO_NODE_H_
#define LIBS_NODES_WINDOW_SERVO_NODE_H_

#if(ACTIVE_DEVICE == WINDOW_SERVO_CONTROLLER)
#include <Servo.h>
extern app mesh;
/*
 * Nodes:
 * 1. WINDOW_SERVO_CONTROLLER
 * servo motor pin (D2)
 * program pin (D0)
 */

servo_t config[1] = { { pin:D2, status:0 } };

Servo servo;

void onPreInitialize()
{

	servo.attach(config[0].pin);
	for (auto pos = 0; pos <= config[0].position; pos++) {
		servo.write(pos);
		delay(15);
	}

	mesh.onMotorEventListener([](uint8_t &index, uint16_t &newPosition) {

		auto cnt = newPosition - config[index].position;
		if(cnt > 0)
		{
			for (auto pos = config[index].position; pos <= newPosition; pos++) {
				servo.write(pos);
				delay(15);
			}
		}
		else
		{
			for (auto pos = newPosition; pos <= config[index].position; pos--) {
				servo.write(pos);
				delay(15);
			}
		}
		config[index].position = newPosition;
		String data = "";
		data += config[index].position;
		mesh.setSensorData(data);
	});
}
void onPostInitialize()
{

}

void onSensor()
{
	String data = "";
	data += config[0].position;
	mesh.setSensorData(data);
	mesh.setSensorDelay(0);
}

#endif

#endif /* LIBS_NODES_WINDOW_SERVO_NODE_H_ */
