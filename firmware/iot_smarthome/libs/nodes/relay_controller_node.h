/*
 * relay_controller_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_RELAY_CONTROLLER_NODE_H_
#define LIBS_NODES_RELAY_CONTROLLER_NODE_H_

#if(ACTIVE_DEVICE == ONE_RELAY_CONTROLLER || ACTIVE_DEVICE == LIGHT_RELAY_CONTROLLER || ACTIVE_DEVICE == TWO_RELAY_CONTROLLER || ACTIVE_DEVICE == FOUR_RELAY_CONTROLLER || ACTIVE_DEVICE == EIGHT_RELAY_CONTROLLER)

extern app mesh;

#if(ACTIVE_DEVICE == ONE_RELAY_CONTROLLER || ACTIVE_DEVICE == LIGHT_RELAY_CONTROLLER)
/*
 * Nodes:
 * 1. LIGHT_RELAY_CONTROLLER
 * 2. ONE_RELAY_CONTROLLER
 *
 * relay pin (D2)
 * program pin (D0)
 */

#define RELAY_COUNT (1)
relay_t relay[RELAY_COUNT] = { { pin:D2, status:LOW } };
#endif

#if(ACTIVE_DEVICE == TWO_RELAY_CONTROLLER)
/*
 * Nodes:
 * 1. TWO_RELAY_CONTROLLER
 *
 * relay 1 pin (D3)
 * relay 2 pin (D1)
 * program pin (D0)
 */

#define RELAY_COUNT (2)
relay_t relay[RELAY_COUNT] = { { pin:D3, status:LOW }, { pin:D1, status:LOW } };
#endif

#if(ACTIVE_DEVICE == FOUR_RELAY_CONTROLLER)
/*
 * Nodes:
 * 1. FOUR_RELAY_CONTROLLER
 * 2. EIGHT_RELAY_CONTROLLER
 * relay 0 pin (D1)
 * relay 1 pin (D2)
 * relay 2 pin (D3)
 * relay 3 pin (D4)
 * program pin (D0)
 */
#define RELAY_COUNT (4)
relay_t relay[RELAY_COUNT] = { { pin:D1, status:LOW }, { pin:D2, status:LOW }, { pin:D3, status:LOW }, { pin:D4, status:LOW } };
#endif

#if(ACTIVE_DEVICE == EIGHT_RELAY_CONTROLLER)
/*
 * Nodes:
 * 1. FOUR_RELAY_CONTROLLER
 * 2. EIGHT_RELAY_CONTROLLER
 * relay 0 pin (D1)
 * relay 1 pin (D2)
 * relay 2 pin (D3)
 * relay 3 pin (D4)
 * relay 4 pin (D5)
 * relay 5 pin (D6)
 * relay 6 pin (D7)
 * relay 7 pin (D8)
 * program pin (D0)
 */
#define RELAY_COUNT (8)
relay_t relay[RELAY_COUNT] = { { pin:D1, status:LOW }, { pin:D2, status:LOW }, { pin:D3, status:LOW }, { pin:D4, status:LOW }, { pin:D5, status:LOW }, { pin:D6, status:LOW }, { pin:D7, status:LOW }, {
        pin:D8, status:LOW } };
#endif

void onPreInitialize() {
	for (auto i = 0; i < RELAY_COUNT; i++) {
		pinMode(relay[i].pin, OUTPUT);
		digitalWrite(relay[i].pin, relay[i].status);
	}

	mesh.onRelayEventListener([](uint8_t &index, uint8_t &status) {

		digitalWrite(relay[index].pin, status);
		relay[index].status = digitalRead(relay[index].pin);

		String data = "";
		for (auto i = 0;i < RELAY_COUNT; i++) {
			data += digitalRead(relay[index].pin);
		}

		mesh.setSensorData(data);
	});
}

void onPostInitialize() {

}

void onSensor() {

	String data = "";
	for (auto i = 0; i < RELAY_COUNT; i++) {
		relay[i].status = digitalRead(relay[i].pin);
		data += relay[i].status;
	}
	mesh.setSensorData(data);
	mesh.setSensorDelay(0);
}
#endif
#endif /* LIBS_NODES_RELAY_CONTROLLER_NODE_H_ */
