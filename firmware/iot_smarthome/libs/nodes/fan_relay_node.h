/*
 * fan_relay_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_FAN_RELAY_NODE_H_
#define LIBS_NODES_FAN_RELAY_NODE_H_

#if(ACTIVE_DEVICE == TEMP_HUMIDITY_CONTROLLER || ACTIVE_DEVICE == FAN_RELAY_CONTROLLER)
#include "DHT.h"
extern app mesh;
/*
 * Nodes:
 * 1. TEMP_HUMIDITY_CONTROLLER
 * 2. FAN_RELAY_CONTROLLER
 * relay 1 pin (D2)
 * DHT11   pin (D1)
 * program pin (D0)
 */

#define DHT_PIN (D1)

#if(ACTIVE_DEVICE == FAN_RELAY_CONTROLLER)
#define RELAY_COUNT (1)
relay_t relay[RELAY_COUNT] = { { pin:D2, status:LOW } };
#endif

DHT dht;

void onPreInitialize() {

	dht.setup(DHT_PIN);

#if(ACTIVE_DEVICE == FAN_RELAY_CONTROLLER)
	for (auto i = 0; i < RELAY_COUNT; i++) {
		pinMode(relay[i].pin, OUTPUT);
		digitalWrite(relay[i].pin, relay[i].status);
	}

	mesh.onRelayEventListener([](uint8_t &index, uint8_t &status) {

		digitalWrite(relay[index].pin, status);
		relay[index].status = digitalRead(relay[index].pin);

		String data = "";

		float temperature = dht.getTemperature();
		float humidity = dht.getHumidity();

		data += String(temperature) + "," + String(humidity);

		for (auto i = 0; i < RELAY_COUNT; i++) {
			relay[i].status = digitalRead(relay[i].pin);
			data += ",";
			data += relay[i].status;
		}
		mesh.setSensorData(data);
	});
#endif
}
void onPostInitialize() {

}

void onSensor() {
	String data = "";

	float temperature = dht.getTemperature();
	float humidity = dht.getHumidity();

	data += String(temperature) + "," + String(humidity);

#if(ACTIVE_DEVICE == FAN_RELAY_CONTROLLER)
	for (auto i = 0; i < RELAY_COUNT; i++) {
		relay[i].status = digitalRead(relay[i].pin);
		data += ",";
		data += relay[i].status;
	}
#endif

	mesh.setSensorData(data);

	mesh.setSensorDelay(dht.getMinimumSamplingPeriod() / 1000);
}

#endif

#endif /* LIBS_NODES_FAN_RELAY_NODE_H_ */
