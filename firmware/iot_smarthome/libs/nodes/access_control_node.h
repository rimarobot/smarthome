/*
 * access_control_node.h
 *
 *  Created on: 26 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_NODES_ACCESS_CONTROL_NODE_H_
#define LIBS_NODES_ACCESS_CONTROL_NODE_H_

#if(ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
#include "Adafruit_Fingerprint.h"
#include "SPI.h"
#include "MFRC522.h"
#include "TFT_eSPI.h"

extern app mesh;
unsigned long nextTimeout;

/*
 * MFRC522 Reader
 * TFT MISO    pin  (D6)
 * TFT MOSI    pin  (D7)
 * TFT SCK     pin  (D5)
 * TFT SDA(SS) pin  (D3)
 *
 */

#define RST_PIN         255
#define SS_PIN          D3 //  TFT SDA(SS) pin  (D3)

MFRC522 rfid(SS_PIN, RST_PIN);  // Create MFRC522 instance.

/*
 * Fingerprint
 * GND  pin
 * RXD  pin
 * TXD  pin
 * 3.3V pin
 */
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&Serial);

/*
 * TFT Display
 * TFT CS pin  (D8)
 * TFT A0 pin  (D4)
 * TFT SDA pin (D7)
 * TFT SCK pin (D5)
 */
TFT_eSPI tft = TFT_eSPI();

#include "accesscontrol/relay.h"
#include "accesscontrol/display.h"
#include "accesscontrol/rfid_reader.h"
#include "accesscontrol/fingerprint_reader.h"

/*
 * Nodes:
 * 1. ACCESS_CONTROL_CONTROLLER
 *
 * Fingerprint
 * GND  pin
 * RXD  pin
 * TXD  pin
 * 3.3V pin
 *
 * Door sensor
 * sensor pin (D2)
 *
 * Relay
 * relay pin (D1)
 *
 * TFT Display
 * TFT CS pin  (D8)
 * TFT A0 pin  (D4)
 * TFT SDA pin (D7)
 * TFT SCK pin (D5)
 *
 * MFRC522 Reader
 * TFT MISO    pin  (D6)
 * TFT MOSI    pin  (D7)
 * TFT SCK     pin  (D5)
 * TFT SDA(SS) pin  (D3)
 *
 *
 * program pin (D0)
 */

void onPreInitialize() {

	_sensor_init();
	_rfidreader_init();
	_fingerprint_init();
	mesh.onRFIDHandler(&onVerifyCard);
	mesh.onFingerPrintHandler(&onVerifyFinger);

	mesh.onAccessVerifyEventListener([](uint32_t &from, uint8_t &command, String &data, uint8_t &status) {
		if(from == mesh.getRootNodeId()) {
			if(command == VERIFY_FINGER_REQUEST || command == VERIFY_CARD_REQUEST) {
				_display_name(data);
				if (status == 1) {
					_sensor_relay();
					doorlockTask.setInterval(TASK_SECOND * DOOR_TIMEOUT);
					doorlockTask.enable();
					_display_status("GRANTED");

				} else
				{
					_display_status("DENIED");
				}
			}
		}
	});

	mesh.onAccessRegisterEventListener([](uint32_t &from, uint8_t &command, String &data, uint32_t &userid, uint16_t &fingerid) {
		if(from == mesh.getRootNodeId()) {
			if(command == REGISTER_FINGER_REQUEST) {
				_register_fingerprint(data, userid, fingerid);
			}
			else if(command == REGISTER_CARD_REQUEST) {
				_register_card(data, userid);
			}
		}
	});

}

void onPostInitialize() {

}

void onSensor() {
	_sensor_onsensor();
}

/*
 * process flow
 init
 -> relay
 -> door magnatic switch
 -> fingerprint sensor
 -> rfid card reader
 -> display
 * time
 * date
 * network status

 periodically check presence of
 -> rfid card
 -> fingerprint
 process incoming
 -> rfid card
 -> fingerprint
 */
#endif
#endif /* LIBS_NODES_ACCESS_CONTROL_NODE_H_ */
