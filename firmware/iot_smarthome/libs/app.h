/*
 * app.h
 *
 *  Created on: 20 Apr 2020
 *      Author: kalib
 */

#ifndef LIBS_APP_H_
#define LIBS_APP_H_

#ifdef ESP32
#include <WiFi.h>
#include <AsyncTCP.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESPAsyncTCP.h>
#endif

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
#include <WebSocketsClient.h>
#include <SocketIOclient.h>
#endif

#include <ESP8266WebServer.h>
#include <Hash.h>
#include <painlessMesh.h>
#include <FS.h>
#include <ArduinoJson.h>
#include "config.h"

using namespace painlessmesh;

class app: public painlessMesh {

	public:

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
		app(ESP8266WebServer * _server, SocketIOclient * _socket) {
			socket = _socket;
#else
			app(ESP8266WebServer * _server) {
#endif

			server = _server;

			/*
			 * Register standard callback function to itemize the
			 *
			 */

			auto cb = [this](uint32_t from, String &msg) {
#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER || ACTIVE_DEVICE == EIGHT_RELAY_CONTROLLER)
				    Serial.printf("node: %d  msg: %s\n",from, msg.c_str());
#endif
				    uint8_t channel = this->getValue(COMMAND_TYPE, msg).toInt();
				    switch(channel) {
					    case ROOT_BROADCAST:
					    {
						    this->rootNodeId = from;
						    heartBeatTask.enable();
						    sensorTask.enable();
						    heartBeatTask.forceNextIteration();
						    Serial.printf("this->rootNodeId: %d\n",this->rootNodeId);
						    break;
					    }
					    case NODE_HEARTBIT:				    // root node
					{

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
//					room: data.room, nodeid: data.N, voltage: data.V, ram: data.X, rom: data.Y, sversion: data.F,
//					hversion: data.H, processor: data.P, temperature: data.E, sdata: data.D, ntype: data.T
					DynamicJsonDocument doc(1024);
					JsonArray root = doc.to<JsonArray>();
					root.add("status");
					JsonObject payload = root.createNestedObject();
					payload["room"] = this->config.mesh_room;
					payload["N"] = from;
					payload["V"] = this->getValue(VOLTAGE, msg);
					payload["X"] = this->getValue(RAM_SPACE, msg);
					payload["Y"] = this->getValue(ROM_SPACE, msg);
					payload["F"] = this->getValue(SOFTWARE_VERSION, msg);
					payload["H"] = this->getValue(HARDWARE_VERSION, msg);
					payload["P"] = this->getValue(DEVICE_PROCESSOR, msg);
					payload["E"] = this->getValue(TEMPERATURE, msg);
					payload["D"] = this->getValue(DATA, msg);
					payload["T"] = this->getValue(NODE_TYPE, msg).toInt();
					String output;
					serializeJson(doc, output);
					//Serial.print(output);
					this->socket->sendEVENT(output);
#endif
					break;
				}
				case SWITCH_RELAY: //DONE
				{
					if(relayCallback) {
						uint8_t id = this->getValue(DEVICE_ADDRESS, msg).toInt();
						uint8_t value = this->getValue(DATA, msg).toInt();
						relayCallback( id, value );
					}

					break;
				}
				case DRIVE_MOTOR: //DONE
				{
					if(motorCallback) {
						uint8_t id = this->getValue(DEVICE_ADDRESS, msg).toInt();
						uint16_t value = this->getValue(DATA, msg).toInt();
						motorCallback( id, value );
					}

					break;
				}
				case REGISTER_FINGER_REQUEST: //DONE
				case REGISTER_CARD_REQUEST://DONE
				{
#if (ACTIVE_DEVICE != CENTRAL_HUB_CONTROLLER)
					//C:channel#I:user-id#K:finger-id#D:first name#
					if(BiometricRegisterCallback) {
						String data = this->getValue(DATA, msg);
						uint16_t fingerid = this->getValue(DEVICE_ADDRESS, msg).toInt();
						uint32_t userid = this->getValue(USER_ID, msg).toInt();
						BiometricRegisterCallback(from, channel, data ,userid, fingerid);
					}
#endif

					break;
				}
				case REGISTER_FINGER_RESPONSE: //DONE
				case REGISTER_CARD_RESPONSE://DONE
				{
#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
					//T:#C:[6,8]#D:[finger,card,pin]#I:user id#S:status#
					DynamicJsonDocument doc(1024);
					JsonArray root = doc.to<JsonArray>();
					root.add("register");
					JsonObject payload = root.createNestedObject();
					payload["scr"] = "hub";
					payload["room"] = this->config.mesh_room;
					payload["node"] = from;
					payload["type"] = this->getValue(NODE_TYPE, msg);
					payload["cmd"] = channel;
					payload["bioinfo"] = this->getValue(DATA, msg);
					payload["userid"] = this->getValue(USER_ID, msg);
					payload["status"] = this->getValue(USER_STATUS, msg);

					String output;
					serializeJson(doc, output);
//					Serial.println(output);
					this->socket->sendEVENT(output);
#endif
					break;
				}
				case VERIFY_FINGER_REQUEST: //DONE
				case VERIFY_CARD_REQUEST://DONE
				{

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
					//	T:ACTIVE_DEVICE#C:channel#D:[card info or pin info or finger id]#
					//{room:***,node:***,type:**,command:***, bio:[card,pin,finger id]}
					DynamicJsonDocument doc(1024);
					JsonArray root = doc.to<JsonArray>();
					root.add("verify");
					JsonObject payload = root.createNestedObject();
					payload["room"] = this->config.mesh_room;
					payload["node"] = from;
					payload["type"] = this->getValue(NODE_TYPE, msg);
					payload["cmd"] = channel;
					payload["bio"] = this->getValue(DATA, msg);
					String output;
					serializeJson(doc, output);
//					Serial.println(output);
					this->socket->sendEVENT(output);
#endif
					break;
				}
				case VERIFY_FINGER_RESPONSE: //DONE
				case VERIFY_CARD_RESPONSE://DONE
				{
#if (ACTIVE_DEVICE != CENTRAL_HUB_CONTROLLER)
					//T:#C:[10/12]#D:firstname#S:[0/1]#'
					if(BiometricVerifyCallback) {
						String data = this->getValue(DATA, msg);
						uint8_t status = this->getValue(USER_STATUS, msg).toInt();
						BiometricVerifyCallback(from, channel, data ,status);
					}
#endif

					break;
				}
			};

		}	;
			painlessMesh::onReceive(cb);

		}

		virtual ~app() {
		}

		void setSensorDelay(int seconds) {
			sensorDelay = seconds;
		}

		void setSensorData(String data) {
			SensorData = data;
		}

		void setHub(bool isHub = false) {
			this->isHub = isHub;

#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
			if (isHub) {
				subscribeTask.set(10 * TASK_SECOND, TASK_FOREVER, [this]() {

					char str[200];
					snprintf(str,200,
							"%c:%d#%c:%d#%c:%s#",
							NODE_TYPE, ACTIVE_DEVICE,
							COMMAND_TYPE, ROOT_BROADCAST,
							DATA, String(this->getNodeId()).c_str()
					);
					Serial.println(str);
					this->sendBroadcast(String(str));
					subscribeTask.setInterval( random( TASK_SECOND * 10, TASK_SECOND * 20 ));
				});

				networkTask.set(10 * TASK_SECOND, TASK_FOREVER, [this]() {

					DynamicJsonDocument doc(1024);
					JsonArray root = doc.to<JsonArray>();
					root.add("network");
					JsonObject payload = root.createNestedObject();
					payload["room"] = this->config.mesh_room;
					payload["network"] = this->subConnectionJson();
					String output;
					serializeJson(doc, output);
					this->socket->sendEVENT(output);
					Serial.printf("network msg: %s\n",output.c_str());
					networkTask.setInterval(random( TASK_SECOND * 20, TASK_SECOND * 40 ));
				});

				loginTask.set(10 * TASK_SECOND, TASK_FOREVER, [this]() {

					DynamicJsonDocument doc(1024);
					JsonArray root = doc.to<JsonArray>();
					root.add("login");
					JsonObject payload = root.createNestedObject();
					payload["room"] = this->config.mesh_room;
					payload["address"] = this->getNodeId();
					payload["username"] = String(ACTIVE_DEVICE_PROCESSOR);
					String output;
					serializeJson(doc, output);
					this->socket->sendEVENT(output);
					loginTask.setInterval( random( TASK_SECOND * 5, TASK_SECOND * 10 ));
				});

				// Add it
				mScheduler->addTask(subscribeTask);
				mScheduler->addTask(networkTask);
				mScheduler->addTask(loginTask);
				subscribeTask.disable();
				networkTask.disable();
				loginTask.disable();
			}
#endif
		}

		void onRelayEventListener(relayCallback_t callback) {
			relayCallback = callback;
		}

		void onMotorEventListener(motorCallback_t callback) {
			motorCallback = callback;
		}

		void onAccessVerifyEventListener(BiometricVerifyCallback_t callback) {
			BiometricVerifyCallback = callback;
		}

		void onAccessRegisterEventListener(BiometricRegisterCallback_t callback) {
			BiometricRegisterCallback = callback;
		}

		virtual void onPreInitializeHandler(defaultCallback_t callback) {
			preInitializeHandler = callback;
		}

		void onPostInitializeHandler(defaultCallback_t callback) {
			postInitializeHandler = callback;
		}

		void onPreRunHandler(defaultCallback_t callback) {
			preRunHandler = callback;
		}

		void onPostRunHandler(defaultCallback_t callback) {
			postRunHandler = callback;
		}

		virtual void onSensorHandler(defaultCallback_t callback) {
			sensorHandler = callback;
		}

#if (ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
		void onRFIDHandler(defaultCallback_t callback) {
			rfidHandler = callback;
		}

		void onFingerPrintHandler(defaultCallback_t callback) {
			fingerHandler = callback;
		}
#endif

		void boot(Scheduler* scheduler) {

			this->config_loaded = this->loadConfig();
			if (this->config_loaded == false) {

				if (preInitializeHandler) {
					preInitializeHandler();
				}

				this->setDebugMsgTypes(ERROR | CONNECTION | DEBUG);
				this->init(config.mesh_ssid, config.mesh_password, scheduler, (unsigned short int) config.mesh_port, WIFI_AP_STA, (unsigned char) config.mesh_channel,
				        (unsigned char) config.mesh_hidden, (unsigned char) config.mesh_max_con);
				this->setContainsRoot(true);
#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
				this->stationManual(config.gateway_ssid, config.gateway_password);
				this->setRoot(true);
				this->setHub(true);
				this->socket->begin(config.server_host, config.server_port);
				this->socket->onEvent([this](socketIOmessageType_t type, uint8_t * payload, size_t length) {
					Serial.printf("socket msg: %s\n",payload);

					switch(type)
					{
						case sIOtype_DISCONNECT:
						{
							heartBeatTask.disable();
							subscribeTask.disable();
							loginTask.disable();
							sensorTask.disable();
							break;
						}
						case sIOtype_CONNECT:
						{
							subscribeTask.enable();
							loginTask.enable();
							break;
						}
						case sIOtype_EVENT: //{"event-name":[{"address":"123456","userid":345,"name":"kalibatta"}]}
						{

							char * sptr = NULL;
							int id = strtol((char *)payload, &sptr, 10);
							if(id) payload = (uint8_t *)sptr;
							DynamicJsonDocument doc(1024);
							DeserializationError error = deserializeJson(doc, payload, length);
							if(error) return;

							String eventName = doc[0];
							if(eventName.equals("command") || eventName.equals("verify") || eventName.equals("register")) {
								JsonObject data = doc[1].as<JsonObject>();
								painlessMesh::sendSingle( strtol((char *)(data["node"].as<String>()).c_str(), &sptr, 10) , data["msg"].as<String>());
//
//								if(id) {
//									DynamicJsonDocument docOut(1024);
//									JsonArray array = docOut.to<JsonArray>();
//									JsonObject param1 = array.createNestedObject();
//									param1["node"] = data["node"].as<String>();
//									String output;
//									output += id;
//									serializeJson(docOut, output);
//									this->socket.send(sIOtype_ACK, output);
//								}
							}
							else if(eventName.equals("login")) {

								DeserializationError error = deserializeJson(doc, doc[1].as<String>());
								if(error) return;
								int status = doc["status"].as<int>();
								if(status == 1) {				    // login successful
									loginTask.disable();
									heartBeatTask.enable();
									networkTask.enable();
									sensorTask.enable();
								}
							}
//							else if(eventName.equals("routines")) {
//
//							}
//							else if(eventName.equals("weather")) {
//
//							}
//							else if(eventName.equals("logout")) {
//
//							}
							break;
						}
						default:
						{
							break;
						}
					};
				});
#endif
				/*
				 *Device heartbeat to report its status to the server
				 */
				heartBeatTask.set(10 * TASK_SECOND, TASK_FOREVER, [this]() {

#if (ACTIVE_DEVICE != CENTRAL_HUB_CONTROLLER)
					    /*
					     * All Nodes should send activity/status data to the root node for server reporting
					     * Data Structure:
					     * T:''#C:''#N:''#V:''#X:''#Y:''#F:''#H:''#P:''#E:''#D:''#
					     */

					    char str[200];
					    snprintf(str, 200,
							    "%c:%d#%c:%d#%c:%s#%c:%s#%c:%s#%c:%s#%c:%s#%c:%s#%c:%s#%c:%s#%c:%s#",
							    NODE_TYPE, ACTIVE_DEVICE,
							    COMMAND_TYPE, NODE_HEARTBIT,
							    NODE_ID, String(this->getNodeId()).c_str(),
							    VOLTAGE, String(String(ESP.getVcc())).c_str(),
							    TEMPERATURE, String(String(25.71)).c_str(),
							    RAM_SPACE, String(ESP.getFreeHeap()).c_str(),
							    ROM_SPACE, String(ESP.getSketchSize()).c_str(),
							    HARDWARE_VERSION, String(ACTIVE_HARDWARE_VERSION).c_str(),
							    SOFTWARE_VERSION, String(ACTIVE_SOFTWARE_VERSION).c_str(),
							    DEVICE_PROCESSOR, String(ACTIVE_DEVICE_PROCESSOR).c_str(),
							    DATA, SensorData.c_str()
					    );

					    Serial.println(str);
					    this->sendSingle(rootNodeId,String(str));

#else
					    /*
					     * Root node sends that directly to the server
					     * room: data.room, nodeid: data.N, voltage: data.V, ram: data.X, rom: data.Y, sversion: data.F,
					     * hversion: data.H, processor: data.P, temperature: data.E, sdata: data.D, ntype: data.T
					     *
					     */
					    DynamicJsonDocument doc(1024);
					    JsonArray root = doc.to<JsonArray>();
					    root.add("status");
					    JsonObject payload = root.createNestedObject();
					    payload["room"] = this->config.mesh_room;
					    payload["N"] = String(this->getNodeId());
					    payload["V"] = String(String(ESP.getVcc()));
					    payload["X"] = String(ESP.getFreeHeap());
					    payload["Y"] = String(ESP.getSketchSize());
					    payload["F"] = String(ACTIVE_SOFTWARE_VERSION);
					    payload["H"] = String(ACTIVE_HARDWARE_VERSION);
					    payload["P"] = String(ACTIVE_DEVICE_PROCESSOR);
					    payload["E"] = String(String(25.71));
					    payload["D"] = SensorData;
					    payload["T"] = ACTIVE_DEVICE;
					    String output;
					    serializeJson(doc, output);
					    Serial.println(output);
					    this->socket->sendEVENT(output);

#endif
					    heartBeatTask.setInterval(random( TASK_SECOND * 20, TASK_SECOND * 40 ));
				    });

				// Add it
				mScheduler->addTask(heartBeatTask);
				heartBeatTask.disable();

				/*
				 * Add hook to trigger  sensor that sampling
				 */
				sensorTask.set(10 * TASK_SECOND, TASK_FOREVER, [this]() {
					if(sensorHandler) {
						sensorHandler();

						if(sensorDelay > 0)
						sensorTask.setInterval(sensorDelay * TASK_SECOND);
						else
						sensorTask.setInterval( random( TASK_SECOND * 5, TASK_SECOND * 10 ));
					}
					else
					sensorTask.disable();

				});

				// Add it
				mScheduler->addTask(sensorTask);
				sensorTask.disable();

				if (postInitializeHandler) {
					postInitializeHandler();
				}

#if (ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
				rfidTask.set(60 * TASK_SECOND, TASK_FOREVER, [this]() {
							if(rfidHandler) {
								rfidHandler();
								rfidTask.setInterval(random( TASK_SECOND * 20, TASK_SECOND * 40 ));
							}
							else
							rfidTask.disable();

						});

				// Add it
				mScheduler->addTask(rfidTask);
				rfidTask.disable();

				fingerTask.set(60 * TASK_SECOND, TASK_FOREVER, [this]() {
							if(fingerHandler) {
								fingerHandler();
								fingerTask.setInterval(random( TASK_SECOND * 20, TASK_SECOND * 40 ));
							}
							else
							fingerTask.disable();

						});

				// Add it
				mScheduler->addTask(fingerTask);
				fingerTask.disable();
#endif

			} else {
				/* Put IP Address details */
				IPAddress local_ip(192, 168, 1, 1);
				IPAddress gateway(192, 168, 1, 1);
				IPAddress subnet(255, 255, 255, 0);
				WiFi.softAP(String("admin").c_str(), String("password").c_str());
				WiFi.softAPConfig(local_ip, gateway, subnet);

				this->server->on("/load", HTTP_GET, [this]() {

					DynamicJsonDocument doc(1024);
					JsonObject root = doc.createNestedObject();
					root["node"] = String(this->getEncodeNodeId());
					root["voltage"] = String(ESP.getVcc());
					root["ram"] = String(ESP.getFreeHeap());
					root["rom"] = String(ESP.getSketchSize());
					root["software"] = String(ACTIVE_SOFTWARE_VERSION);
					root["hardware"] = String(ACTIVE_HARDWARE_VERSION);
					root["processor"] = String(ACTIVE_DEVICE_PROCESSOR);
					root["temp"] = String(String(25.71));
					root["type"] = ACTIVE_DEVICE;
					String output;
					serializeJson(doc, output);

					this->server->send(200, "text/plain", output);
				});

				this->server->on("/save", HTTP_POST, [this]() {
					if(this->server->hasArg("params")) {
						if(saveConfig(this->server->arg("params"))) {
							this->server->send(200, "text/plain", "successful");
						}
						else {
							this->server->send(404, "text/plain", "failed");
						}
					}
					this->server->send(404, "text/plain", "parameters not found");
				});

				this->server->onNotFound([this]() {
					this->server->send(404, "text/plain", "Not found");
				});

				this->server->begin();
			}
		}

		void run() {
//
			if (preRunHandler)
				preRunHandler();

			if (this->config_loaded == false) {
				this->update();
#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
				this->socket->loop();
#endif
			} else {
				this->server->handleClient();
			}

			if (postRunHandler)
				postRunHandler();

		}

		uint32_t getRootNodeId() const {
			return rootNodeId;
		}

	private:
		Task heartBeatTask;
		Task sensorTask;
#if (ACTIVE_DEVICE == CENTRAL_HUB_CONTROLLER)
		Task subscribeTask;
		Task networkTask;
		Task loginTask;
		SocketIOclient *socket;
#endif

#if (ACTIVE_DEVICE == ACCESS_CONTROL_CONTROLLER)
		Task rfidTask;
		Task fingerTask;
#endif
		bool config_loaded = false, isHub = false;
		String SensorData;
		uint32_t rootNodeId = 0;
		int sensorDelay = 0;
		config_t config = { mesh_room:"0000001", mesh_ssid: "SHMab67", mesh_password:"2e80ab67", mesh_port:5555, mesh_max_con:4, mesh_channel:1, mesh_hidden:0, gateway_ssid:
		"KRB-AIR1", gateway_password:"benedict24", server_host:"192.168.1.100", server_port:3000, server_username:"username", server_password:"password" };
		ESP8266WebServer *server;
		relayCallback_t relayCallback;
		motorCallback_t motorCallback;
		BiometricVerifyCallback_t BiometricVerifyCallback;
		BiometricRegisterCallback_t BiometricRegisterCallback;
		defaultCallback_t preInitializeHandler;
		defaultCallback_t postInitializeHandler;
		defaultCallback_t preRunHandler;
		defaultCallback_t postRunHandler;
		defaultCallback_t sensorHandler;
		defaultCallback_t rfidHandler;
		defaultCallback_t fingerHandler;

		String getValue(char key, String &data) {
			char str[5];
			snprintf(str,5, "%c:", key);
			int index = data.indexOf(str);
			String temp = data.substring(index + 2);
			index = temp.indexOf('#');
			String value = temp.substring(0, index);
			//Serial.printf("key:%c value: %s\n",key,  value.c_str());
			return value;
		}

		uint32_t getEncodeNodeId() {
			uint8_t *hwaddr = WiFi.BSSID();
			uint32_t value = 0;
			value |= hwaddr[2] << 24;  // Big endian (aka "network order"):
			value |= hwaddr[3] << 16;
			value |= hwaddr[4] << 8;
			value |= hwaddr[5];
			return value;
		}

		bool loadConfig() {
			bool success = SPIFFS.begin();
			if (success) {
				File _file = SPIFFS.open("/config.txt", "r");
				if (!_file)
					return false;

				String data = _file.readString();
				DynamicJsonDocument doc(1024);
				DeserializationError error = deserializeJson(doc, data);
				if (error)
					return false;

				JsonObject obj = doc.as<JsonObject>();
				this->config.mesh_room = obj["room"].as<String>();
				this->config.mesh_ssid = obj["mhssid"].as<String>();
				this->config.mesh_password = obj["mhpass"].as<String>();
				this->config.mesh_port = obj["mhport"].as<uint16_t>();
				this->config.mesh_max_con = obj["mhconn"].as<uint8_t>();
				this->config.mesh_channel = obj["mhchnl"].as<uint8_t>();
				this->config.mesh_hidden = obj["mhhide"].as<uint8_t>();
				this->config.gateway_ssid = obj["gwssid"].as<String>();
				this->config.gateway_password = obj["gwpass"].as<String>();
				this->config.server_host = obj["svhost"].as<String>();
				this->config.server_port = obj["svport"].as<uint16_t>();
				this->config.server_username = obj["svuser"].as<String>();
				this->config.server_password = obj["svpass"].as<String>();
				_file.close();
			}
			SPIFFS.end();
			return success;
		}

		/*
		 * TODo requires web server implementation
		 */
		bool saveConfig(String data) {
			bool success = SPIFFS.begin();
			if (success) {
				File _file = SPIFFS.open("/config.txt", "w");
				if (!_file)
					return false;
				int bytesWritten = _file.print(data);
				if (bytesWritten <= 0)
					return false;
				_file.close();
			}
			SPIFFS.end();
			return success;
		}

};

#endif /* LIBS_APP_H_ */
